<?php
/**
 * Template Name: Home Template
 */
get_header();
?>
<div class="width100 luckiehome">

    <div class="row-fluid luckies_home">
        <div class="span12">
            <div class="banner">
                <div id="myCarousel" class="mycarousel1 slide">
                    <?php
                    if (have_posts()): while (have_posts()): the_post();
                            the_content();
                        endwhile;
                    endif;
                    ?>
                </div>
            </div>

            <?php $custom_theme_options = get_option('custom_theme_options'); ?>

            <?php
            $product_array_less15 = array();
            $category_array1 = array();
            $category_array2 = array();
            $category_array3 = array();
            $new_arrival_array = array();
            $category_crousel1 = $custom_theme_options['home_category_crousel1'];
            $number_of_product1 = $custom_theme_options['number_of_product1'];
            $category_crousel2 = $custom_theme_options['home_category_crousel2'];
            $number_of_product2 = $custom_theme_options['number_of_product2'];
            $category_crousel3 = $custom_theme_options['home_category_crousel3'];
            $number_of_product3 = $custom_theme_options['number_of_product3'];
            $new_arrival = $custom_theme_options['new_arrival'];

            $args = array('post_type' => 'product', 'posts_per_page' => -1);
            $custom_query = new WP_Query($args);

            $count1 = 0;
            $count2 = 0;
            $count3 = 0;
            if ($custom_query->have_posts()) {
                while ($custom_query->have_posts()) {
                    $custom_query->the_post();

                    $product_price = get_post_meta($post->ID, '_regular_price', true);
                    if ($product_price < 15 && !empty($product_price) && count($product_array_less15) < 5) {
                        $product_array_less15[] = get_the_ID();
                    }

                    $term_list = wp_get_post_terms($post->ID, 'product_cat', array("fields" => "all"));
                    foreach ($term_list as $single_term) {
                        $term_slug = $single_term->slug;
                        if (($term_slug == $category_crousel1) && ($count1 <= $number_of_product1)) {
                            $category_array1[] = get_the_ID();
                            $category_name1 = $single_term->name;
                            $category_view_all_link1 = get_term_link($single_term);
                            $count1++;
                        }
                        if (($new_arrival == 'on') && ($count2 <= $number_of_product2)) {
                            if (!in_array(get_the_ID(), $new_arrival_array))
                                $new_arrival_array[] = get_the_ID();
                            $category_name2 = $single_term->name;
                            $category_view_all_link2 = get_term_link($single_term);
                            $count2++;
                        } elseif (($term_slug == $category_crousel2) && ($count2 <= $number_of_product2)) {
                            $category_array2[] = get_the_ID();
                            $category_name2 = $single_term->name;
                            $category_view_all_link2 = get_term_link($single_term);
                            $count2++;
                        }
                        if (($term_slug == $category_crousel3) && ($count3 <= $number_of_product3)) {
                            $category_array3[] = get_the_ID();
                            $category_name3 = $single_term->name;
                            $category_view_all_link3 = get_term_link($single_term);
                            $count3++;
                        }
                    }
                }
            }
            wp_reset_postdata();
            wp_reset_query();

            global $product;
            ?>
            <div class="width100">
                <div class="span4 info_slider">
                    <div class="gift-slider">
                        <ul class="slides">
                            <?php
                            foreach ($product_array_less15 as $slider_product_id) {
                                $slider_product_object = get_post($slider_product_id);
                                ?>
                                <li class="sl-slide bg-1">
                                    <div class="sl-slide-inner">
                                        <div class="deco" data-icon="H"></div>
                                        <h3>Gifts for under</h3>
                                        <h1><?php
                                            $slider_product_price = get_post_meta($slider_product_id, '_regular_price', true);
                                            echo '&pound;' . $slider_product_price;
                                            ?></h1>
                                        <div class="image-seperator"></div>
                                        <blockquote><p><?php
                                                $slider_product_content = wp_trim_words($slider_product_object->post_content, 15);
                                                echo $slider_product_content;
                                                ?></p><a href="<?php echo $slider_product_object->guid; ?>">View All</a></blockquote>
                                    </div>
                                </li>
                            <?php } ?>

                        </ul>
                    </div><!-- /sl-slider -->



                </div><!-- end of span4 info_slider -->

                <!--getting all set theme options-->


                <div class="span8 fr image_banners">
                    <?php
                    $term_object1 = get_term_by('slug', $custom_theme_options['home_image_category1'], 'product_cat');
                    $term_thumbnail1 = wp_get_attachment_url(get_woocommerce_term_meta($term_object1->term_id, 'thumbnail_id', true));
                    $term_object2 = get_term_by('slug', $custom_theme_options['home_image_category2'], 'product_cat');
                    $term_thumbnail2 = wp_get_attachment_url(get_woocommerce_term_meta($term_object2->term_id, 'thumbnail_id', true));
                    $term_object3 = get_term_by('slug', $custom_theme_options['home_image_category3'], 'product_cat');
                    $term_thumbnail3 = wp_get_attachment_url(get_woocommerce_term_meta($term_object3->term_id, 'thumbnail_id', true));
                    ?>
                    <?php
                    if (isset($term_object1) && !empty($term_object1)) {
                        ?>
                        <div class="width100 adv">
                            <img src="<?php echo $term_thumbnail1; ?>" />
                            <div class="ad_content width100">
                                <h3 class="fl"><?php echo $term_object1->name; ?></h3>
                                <a href="<?php echo get_term_link($term_object1); ?>" class="fr view_all">View All</a>
                            </div>
                        </div>
                        <?php
                    }
                    ?><div class="clear marg_top_20"></div><?php
                    if (isset($term_object2) && !empty($term_object2)) {
                        ?>
                        <div class="span6 fl">
                            <div class="width100 adv">
                                <img src="<?php echo $term_thumbnail2; ?>" />
                                <div class="ad_content width100">
                                    <h3 class="fl"><?php echo $term_object2->name; ?></h3>
                                    <a href="<?php echo get_term_link($term_object2); ?>" class="fr view_all">View All</a>
                                </div>
                            </div>
                        </div>
                        <?php
                    }
                    if (isset($term_object3) && !empty($term_object3)) {
                        ?>
                        <div class="span6 fr">
                            <div class="width100 adv">
                                <img src="<?php echo $term_thumbnail3; ?>" />
                                <div class="ad_content width100">
                                    <h3 class="fl"><?php echo $term_object3->name; ?></h3>
                                    <a href="<?php echo get_term_link($term_object3); ?>" class="fr view_all">View All</a>
                                </div>
                            </div>
                        </div>
                    <?php } ?>
                </div>




                <div class="width100 additional-info-container">
                    <?php
                    $student_text = $custom_theme_options['student_discount'];
                    $discount_amount = $custom_theme_options['discount_amount'];
                    $call_text = $custom_theme_options['call_text'];
                    $contact_number = $custom_theme_options['contact_number'];
                    $shipping_text = $custom_theme_options['shipping_text'];
                    $shipping_amount = $custom_theme_options['shipping_amount'];
                    ?>
                    <?php
                    if (isset($student_text) && isset($discount_amount) && !empty($student_text) && !empty($discount_amount)) {
                        ?>
                        <div class="span4 additional-info"><span class="orange_text"><?php echo $discount_amount; ?></span> <?php echo $student_text; ?></div>
                        <?php
                    }
                    if (isset($call_text) && isset($contact_number) && !empty($call_text) && !empty($contact_number)) {
                        ?>
                        <div class="span4 additional-info"><?php echo $call_text; ?> <span class="orange_text">Call: <?php echo $contact_number; ?></span></div>
                        <?php
                    }
                    if (isset($shipping_text) && isset($shipping_amount) && !empty($shipping_text) && !empty($shipping_amount)) {
                        ?>
                        <div class="span4 additional-info"><span class="orange_text"><?php echo $shipping_text; ?></span> <?php echo $shipping_amount; ?></div>
                        <?php
                    }
                    ?>
                </div>

            </div>
        </div>
    </div>


    <div class="width100 gift_for_men">
        <div class="box_grey_left fl"></div>
        <div class="box_grey_left fr"></div>
        <div class="row-fluid all_carusl">
            <div class="headeing_border">
                <h2 class="category-name"><?php echo $category_name1; ?></h2>
                <div class="category-archive"><a href="<?php echo $category_view_all_link1; ?>">View All</a></div>
            </div>
            <div class="span12">
                <div id="category-crousel1" class="flexslider">
                    <ul class="slides">
                        <?php
                        foreach ($category_array1 as $product_id1) {
                            ?>
                            <li>
                                <div class="item">

                                    <?php
                                    if ('' != get_the_post_thumbnail($product_id1)) {
                                        echo get_the_post_thumbnail($product_id1, 'latest_featured_image');
                                    }
                                    ?>
                                    <div class="seperator"></div>
                                    <div class="width100 caro_detail">
                                        <div class="width100">
                                            <div class="span7 fl">

                                                <?php
                                                $product_object1 = get_post($product_id1);
                                                $product_price1 = get_post_meta($product_id1, '_regular_price', true);
                                                ?>
                                                <a href="<?php echo $product_object1->guid; ?>"><h5><?php echo $product_object1->post_title; ?></h5></a>
                                                <h3>
                                                    <?php
                                                    if (isset($product_price1) && !empty($product_price1)) {
                                                        echo $product_price1;
                                                    }
                                                    ?>
                                                </h3>
                                            </div>
                                            <?php
                                            $product = new WC_Product_Simple($product_id1);
                                            ?>
                                            <?php woocommerce_template_loop_add_to_cart($product_object1, $product); ?>
                                        </div>
                                    </div>
                                </div>
                            </li>
                            <?php
                        }
                        ?>
                        <!-- items mirrored twice, total of 12 -->
                    </ul>
                </div>
            </div><!--end of span12-->
        </div><!--end of row-fluid-->
    </div><!-- end of width100 gift_for_men-->




    <div class="width100 new_arrivals">
        <div class="box_grey_left fl"></div>
        <div class="box_grey_left fr"></div>
        <div class="row-fluid all_carusl">
            <div class="headeing_border">
                <h2 class="category-name"><?php echo $category_name2; ?></h2>
                <div class="category-archive"><a href="<?php echo $category_view_all_link2; ?>">View All</a></div>
            </div>
            <div class="span12">
                <div id="category-crousel2" class="flexslider">
                    <ul class="slides">
                        <?php
                        foreach ($category_array2 as $product_id2) {
                            ?>
                            <li>
                                <div class="item">

                                    <?php
                                    if ('' != get_the_post_thumbnail($product_id2)) {
                                        echo get_the_post_thumbnail($product_id2, 'latest_featured_image');
                                    }
                                    ?>
                                    <div class="seperator"></div>
                                    <div class="width100 caro_detail">
                                        <div class="width100">
                                            <div class="span7 fl">

                                                <?php
                                                $product_object2 = get_post($product_id2);
                                                $product_price2 = get_post_meta($product_id2, '_regular_price', true);
                                                ?>
                                                <a href="<?php echo $product_object2->guid; ?>"><h5><?php echo $product_object2->post_title; ?></h5></a>
                                                <h3>
                                                    <?php
                                                    if (isset($product_price2) && !empty($product_price2)) {
                                                        echo $product_price2;
                                                    }
                                                    ?>
                                                </h3>
                                            </div>
                                            <?php
                                            $product = new WC_Product_Simple($product_id2);
                                            ?>
                                            <?php woocommerce_template_loop_add_to_cart($product_object2, $product); ?>
                                        </div>
                                    </div>
                                </div>
                            </li>
                            <?php
                        }
                        ?>
                        <!-- items mirrored twice, total of 12 -->
                    </ul>
                </div>
            </div><!--end of span12-->
        </div><!--end of row-fluid-->
    </div><!-- end of width100 gift_for_men-->

    <div class="width100 gift_for_men">
        <div class="box_grey_left fl"></div>
        <div class="box_grey_left fr"></div>
        <div class="row-fluid all_carusl">
            <div class="headeing_border">
                <h2 class="category-name"><?php echo $category_name3; ?></h2>
                <div class="category-archive"><a href="<?php echo $category_view_all_link3; ?>">View All</a></div>
            </div>
            <div class="span12">
                <div id="category-crousel3" class="flexslider">
                    <ul class="slides">
                        <?php
                        foreach ($category_array3 as $product_id3) {
                            ?>
                            <li>
                                <div class="item">

                                    <?php
                                    if ('' != get_the_post_thumbnail($product_id3)) {
                                        echo get_the_post_thumbnail($product_id3, 'latest_featured_image');
                                    }
                                    ?>
                                    <div class="seperator"></div>
                                    <div class="width100 caro_detail">
                                        <div class="width100">
                                            <div class="span7 fl">

                                                <?php
                                                $product_object3 = get_post($product_id3);
                                                $product_price3 = get_post_meta($product_id3, '_regular_price', true);
                                                ?>
                                                <a href="<?php echo $product_object3->guid; ?>"><h5><?php echo $product_object3->post_title; ?></h5></a>
                                                <h3>
                                                    <?php
                                                    if (isset($product_price3) && !empty($product_price3)) {
                                                        echo $product_price3;
                                                    }
                                                    ?>
                                                </h3>
                                            </div>
                                            <?php
                                            $product = new WC_Product_Simple($product_id3);
                                            ?>
                                            <?php woocommerce_template_loop_add_to_cart($product_object3, $product); ?>
                                        </div>
                                    </div>
                                </div>
                            </li>
                            <?php
                        }
                        ?>
                        <!-- items mirrored twice, total of 13 -->
                    </ul>
                </div>
            </div><!--end of span13-->
        </div><!--end of row-fluid-->
    </div><!-- end of width100 gift_for_men-->


    <div class="width100 blog-section">
        <div class="row-fluid all_carusl">
            <div class="headeing_border">
                <h2 class="category-name">Latest Blog Posts</h2>
                <div class="category-archive"><a href="<?php echo $category_view_all_link1; ?>">View All</a></div>
            </div>
            <div class="span4 latest-post-sidebar">
                <?php dynamic_sidebar('home-page-post-tabs'); ?>
            </div>

            <div class="span8 latest-post-main">
                <?php
                $post_args = array('post_type' => 'post', 'posts_per_page' => 1);
                $custom_post_query = new WP_Query($post_args);

                if ($custom_post_query->have_posts()) {
                    while ($custom_post_query->have_posts()) {
                        $custom_post_query->the_post();
                        ?>
                        <div class="span8">
                            <?php
                        if(has_post_thumbnail()){
                            echo the_post_thumbnail('full');
                        }
                        ?>
                        </div>
                <div class="span4">
                    <h3 class="post-title"><a href="<?php the_permalink(); ?>"><span class="orange_text"><?php the_title(); ?></span></a></h3>
                    <div class="post-content"><?php $post_content = wp_trim_words(get_the_content(),15); echo $post_content; ?></div>
                    <!--<p class="small_fonts">Posted by:<span class="bold orange_text"><?php the_author(); ?></span> on <?php the_date('d-m-Y', '<span class="orange_text">', '</span>'); ?></p>-->
                </div>
                            
                            <?php
                    }
                }
                wp_reset_postdata();
                wp_reset_query();
                ?>

            </div>
        </div><!--end of row-fluid-->
    </div>



</div>
<?php get_footer(); ?>