$(document).ready(function() {
    //Initialising Accordion
    $(".accordion").tabs(".pane", {
        tabs: '> h2',
        effect: 'slide',
        initialIndex: null
    });

    //The click to hide function
    $(".accordion > h2").click(function() {
        if ($(this).hasClass("current") && $(this).next().queue().length === 0) {
            $(this).next().slideUp();
            $(this).removeClass("current");
        } else if (!$(this).hasClass("current") && $(this).next().queue().length === 0) {
            $(this).next().slideDown();
            $(this).addClass("current");
        }
    });
    
    $('.faq-catergory').click(function(){
    $('.faq-catergory').css("background","none");
    $('.faq-category-name').css("background","none");
    $('.faq-category-name').css("color","#000");
    $(this).css("background","white");
    $(this).children('h2').css("background","white");
    $(this).children('h2').css("color","#fb8b0f");
    $(this).find('.faq-question-title').css("color","#000");
    $(this).find('.current').css("color","#fb8b0f");
   
  });
});