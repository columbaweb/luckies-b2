/**
 * Mobile Navigation Script
 *
 * @package luckiesDesign
 */

jQuery( document ).ready(function() {
    var nav_btn = '<button class="luckiesdesign-nav-btn" type="button"><span class="luckiesdesign-icon-bar"></span><span class="luckiesdesign-icon-bar"></span><span class="luckiesdesign-icon-bar"></span></button>';
    /* prepend menu icon */
    jQuery('.luckiesdesign-mobile-nav').prepend(nav_btn);

    /* toggle nav */
    jQuery('.luckiesdesign-nav-btn').on('click', function(){
        jQuery('#luckiesdesign-nav-menu').slideToggle();
        jQuery(this).toggleClass('active');
    });
});