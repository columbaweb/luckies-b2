<?php
/**
 * The template for displaying 404 pages (Not Found)
 *
 * @package luckiesDesign
 * 
 * @since luckiesDesign 2.0
 */
get_header(); ?>

    <section id="content" role="main" class="luckiesdesign-grid-8">

        <?php luckiesdesign_hook_begin_content(); ?>

            <h1 class="post-title luckiesdesign-main-title"><?php _e( 'Not Found', 'luckiesDesign' ); ?></h1>

            <?php luckiesdesign_hook_begin_post(); ?>

            <div class="post-content clearfix luckiesdesign-not-found">
                <p><?php _e( 'Apologies, but no results were found for the requested archive. Perhaps searching will help find a related post.', 'luckiesDesign' ); ?></p>
                <?php get_search_form(); ?>
            </div>

            <?php luckiesdesign_hook_end_post();?>

        <?php luckiesdesign_hook_end_content(); ?>

    </section><!-- #content -->

    <?php luckiesdesign_hook_sidebar(); ?>

<?php get_footer(); ?>