<?php
/**
 * Template Name: Blog
 */
get_header();
?>


<div class="row-fluid about_us_page">


<!--breadcrumb-->
<?php get_breadcrumb(); ?>
    <div class="span12 about_grey_box">

        <div class="headeing_border">
            <h2><?php the_title(); ?></h2>
        </div>
        <p class="text_center">
            <?php
            if (have_posts()): while (have_posts()): the_post();
                    the_content();
                endwhile;
            endif;
            ?>
        </p>

    </div><!--end of span12-->
</div>



<div id="entries-blog" class="row-fluid luckies_blog">
    <div class="span12">

        <div class="blog_header width100">
            <select class="icon_archive span4" name="archive-menu" onChange="document.location.href = this.options[this.selectedIndex].value;">
                <option value="">Select month</option>
                <?php wp_get_archives('type=monthly&format=option'); ?>
            </select>

            <select class="icon_categories span4" name="event-dropdown" onchange='document.location.href = this.options[this.selectedIndex].value;'> 
                <option value=""><?php echo esc_attr(__('Categories')); ?></option> 
                <?php
                $categories = get_categories();
                foreach ($categories as $category) {
                    $option = '<option value="' . home_url('/') . '/category/' . $category->category_nicename . '">';
                    $option .= $category->cat_name;
                    $option .= '</option>';
                    echo $option;
                }
                ?>
            </select>

            <div class="span4 search_blog">
                <?php get_search_form(); ?>
            </div><!-- end of span4 search_blog -->
        </div><!--end of blog_header width100-->

        <div class="clear"></div>


        <?php
        global $paged;
        $curpage = $paged ? $paged : 1;
        $count_posts = wp_count_posts();
        $published_posts = $count_posts->publish;
        $post_array = array();
        $post_args = array('post_type' => 'post', 'paged' => $paged);
        $post_query = new WP_Query($post_args);

        if ($post_query->have_posts()) {
            while ($post_query->have_posts()) {
                $post_query->the_post();
                $post_array[] = get_the_ID();
            }
        }
        wp_reset_postdata();
        wp_reset_query();
        ?>
        <div class="width100 blog_container">
            <?php
            $count = 0;
            $div_once = 'true';
            foreach ($post_array as $single_post_id) {
                if ($count < 1) {
                    ?>
                    <div class="blog_left span6">
                        <span class="span12 large_blog">
                            <?php
                            $post_object = get_post($single_post_id);
                            $post_image = get_the_post_thumbnail($single_post_id, 'full');
                            if (isset($post_image) && !empty($post_image)) {
                                ?>
                                <a href="<?php echo get_the_permalink($single_post_id); ?>"><?php echo $post_image; ?></a>
                                <?php
                            }
                            ?>
                            <a href="<?php echo get_the_permalink($single_post_id); ?>"><h4 class="seperator"><?php echo $post_object->post_title; ?></h4></a>
                            <h5 class="suggesstion">Suggesstion</h5>
                            <p>
                                <?php
                                $post_content = wp_trim_words(($post_object->post_content), 15);
                                echo $post_content;
                                ?>
                            </p>
                            <?php
                            $post_author = get_user_by('id', $post_object->post_author);
                            $author_posts = get_author_posts_url($post_object->post_author);
                            $author_display_name = $post_author->display_name;
                            ?>
                            <p class="small_fonts">Posted by: <a href="<?php echo $author_posts ?>"><span class="bold orange_text"><?php echo $author_display_name; ?></span></a> on <?php the_date('d-m-Y', '<span class="orange_text">', '</span>'); ?></p>
                        </span>
                    </div>
                    <?php
                    array_shift($post_array);
                } elseif ($count < 3) {
                    if ($div_once == 'true') {
                        ?>
                        <div class="blog_right span6">
                        <?php } ?>
                        <span class="span6 small_blog">
                            <?php
                            $post_object = get_post($single_post_id);
                            $post_image = get_the_post_thumbnail($single_post_id, 'full');
                            if (isset($post_image) && !empty($post_image)) {
                                ?>
                                <a href="<?php echo get_the_permalink($single_post_id); ?>"><?php echo $post_image; ?></a>
                                <?php
                            }
                            ?>
                            <a href="<?php echo get_the_permalink($single_post_id); ?>"><h4 class="seperator"><?php echo $post_object->post_title; ?></h4></a>
                            <h5 class="suggesstion">Suggesstion</h5>
                            <p>
                                <?php
                                $post_content = wp_trim_words(($post_object->post_content), 15);
                                echo $post_content;
                                ?>
                            </p>
                            <?php
                            $post_author = get_user_by('id', $post_object->post_author);
                            $author_posts = get_author_posts_url($post_object->post_author);
                            $author_display_name = $post_author->display_name;
                            ?>
                            <p class="small_fonts">Posted by: <a href="<?php echo $author_posts ?>"><span class="bold orange_text"><?php echo $author_display_name; ?></span></a> on <?php the_date('d-m-Y', '<span class="orange_text">', '</span>'); ?></p>
                        </span><!--end of span6 small_blog-->
                        <?php
                        $div_once = 'false';
                        array_shift($post_array);
                    }
                    $count++;
                }
                ?>
            </div>
        </div>

        <div class="width100 blog_container">
            <?php
            $count = 0;
            $div_close = 'true';
            $div_once = 'true';
            foreach ($post_array as $single_post_id) {
                if ($count < 2) {
                    if ($div_once == 'true') {
                        ?>
                        <div class="blog_left span6">
                        <?php } ?>
                        <span class="span6 small_blog">
                            <?php
                            $post_object = get_post($single_post_id);
                            $post_image = get_the_post_thumbnail($single_post_id, 'full');
                            if (isset($post_image) && !empty($post_image)) {
                                ?>
                                <a href="<?php echo get_the_permalink($single_post_id); ?>"><?php echo $post_image; ?></a>
                                <?php
                            }
                            ?>
                            <a href="<?php echo get_the_permalink($single_post_id); ?>"><h4 class="seperator"><?php echo $post_object->post_title; ?></h4></a>
                            <h5 class="suggesstion">Suggesstion</h5>
                            <p>
                                <?php
                                $post_content = wp_trim_words(($post_object->post_content), 15);
                                echo $post_content;
                                ?>
                            </p>
                            <?php
                            $post_author = get_user_by('id', $post_object->post_author);
                            $author_posts = get_author_posts_url($post_object->post_author);
                            $author_display_name = $post_author->display_name;
                            ?>
                            <p class="small_fonts">Posted by: <a href="<?php echo $author_posts ?>"><span class="bold orange_text"><?php echo $author_display_name; ?></span></a> on <?php the_date('d-m-Y', '<span class="orange_text">', '</span>'); ?></p>
                        </span>
                        <?php
                        $div_once = 'false';
                        array_shift($post_array);
                    } elseif ($count < 3) {
                        if ($div_close == 'true') {
                            ?>
                        </div>
                    <?php } ?>
                    <div class="blog_right span6">
                        <span class="span12 small_blog">
                            <?php
                            $post_object = get_post($single_post_id);
                            $post_image = get_the_post_thumbnail($single_post_id, 'full');
                            if (isset($post_image) && !empty($post_image)) {
                                ?>
                                <a href="<?php echo get_the_permalink($single_post_id); ?>"><?php echo $post_image; ?></a>
                                <?php
                            }
                            ?>
                            <a href="<?php echo get_the_permalink($single_post_id); ?>"><h4 class="seperator"><?php echo $post_object->post_title; ?></h4></a>
                            <h5 class="suggesstion">Suggesstion</h5>
                            <p>
                                <?php
                                $post_content = wp_trim_words(($post_object->post_content), 15);
                                echo $post_content;
                                ?>
                            </p>
                            <?php
                            $post_author = get_user_by('id', $post_object->post_author);
                            $author_posts = get_author_posts_url($post_object->post_author);
                            $author_display_name = $post_author->display_name;
                            ?>
                            <p class="small_fonts">Posted by: <a href="<?php echo $author_posts ?>"><span class="bold orange_text"><?php echo $author_display_name; ?></span></a> on <?php the_date('d-m-Y', '<span class="orange_text">', '</span>'); ?></p>
                        </span><!--end of span6 small_blog-->
                    </div>
                    <?php
                    array_shift($post_array);
                    $div_close = 'false';
                }
                $count++;
            }
            ?>
        </div>


        <div class="width100 blog_container">
            <?php
            $count = 0;
            $div_once = 'true';
            foreach ($post_array as $single_post_id) {
                if ($count < 1) {
                    ?>
                    <div class="blog_left span6">
                        <span class="span12 large_blog">
                            <?php
                            $post_object = get_post($single_post_id);
                            $post_image = get_the_post_thumbnail($single_post_id, 'full');
                            if (isset($post_image) && !empty($post_image)) {
                                ?>
                                <a href="<?php echo get_the_permalink($single_post_id); ?>"><?php echo $post_image; ?></a>
                                <?php
                            }
                            ?>
                            <a href="<?php echo get_the_permalink($single_post_id); ?>"><h4 class="seperator"><?php echo $post_object->post_title; ?></h4></a>
                            <h5 class="suggesstion">Suggesstion</h5>
                            <p>
                                <?php
                                $post_content = wp_trim_words(($post_object->post_content), 15);
                                echo $post_content;
                                ?>
                            </p>
                            <?php
                            $post_author = get_user_by('id', $post_object->post_author);
                            $author_posts = get_author_posts_url($post_object->post_author);
                            $author_display_name = $post_author->display_name;
                            ?>
                            <p class="small_fonts">Posted by: <a href="<?php echo $author_posts ?>"><span class="bold orange_text"><?php echo $author_display_name; ?></span></a> on <?php the_date('d-m-Y', '<span class="orange_text">', '</span>'); ?></p>
                        </span>
                    </div>
                    <?php
                    array_shift($post_array);
                } elseif ($count < 3) {
                    if ($div_once == 'true') {
                        ?>
                        <div class="blog_right span6">
                        <?php } ?>
                        <span class="span6 small_blog">
                            <?php
                            $post_object = get_post($single_post_id);
                            $post_image = get_the_post_thumbnail($single_post_id, 'full');
                            if (isset($post_image) && !empty($post_image)) {
                                ?>
                                <a href="<?php echo get_the_permalink($single_post_id); ?>"><?php echo $post_image; ?></a>
                                <?php
                            }
                            ?>
                            <a href="<?php echo get_the_permalink($single_post_id); ?>"><h4 class="seperator"><?php echo $post_object->post_title; ?></h4></a>
                            <h5 class="suggesstion">Suggesstion</h5>
                            <p>
                                <?php
                                $post_content = wp_trim_words(($post_object->post_content), 15);
                                echo $post_content;
                                ?>
                            </p>
                            <?php
                            $post_author = get_user_by('id', $post_object->post_author);
                            $author_posts = get_author_posts_url($post_object->post_author);
                            $author_display_name = $post_author->display_name;
                            ?>
                            <p class="small_fonts">Posted by: <a href="<?php echo $author_posts ?>"><span class="bold orange_text"><?php echo $author_display_name; ?></span></a> on <?php the_date('d-m-Y', '<span class="orange_text">', '</span>'); ?></p>
                        </span><!--end of span6 small_blog-->
                        <?php
                        $div_once = 'false';
                        array_shift($post_array);
                    }
                    $count++;
                }
                ?>
            </div>
        </div>

        <?php luckiesdesign_hook_blog_pagination(); ?>
<div id="test-div1"></div>
        
    </div>
</div><!--end of luckies_blog-->

<?php get_footer(); ?>