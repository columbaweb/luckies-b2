<?php
/**
 * The loop that displays the post according to the query
 */
global $luckiesdesign_post_comments;
?>
<div class="row-fluid">
    <?php get_breadcrumb(); ?>
    <div class="span12 about_grey_box">
        <div class="headeing_border">
            <?php
            /* Archive Page Titles */
            if (is_search()) {
                ?>
                <h2 class="post-title luckiesdesign-main-title"><?php printf(__('Search Results for: %s', 'luckiesDesign'), '<span>' . get_search_query() . '</span>'); ?></h2><?php } elseif (is_tag()) {
                ?>
                <h2 class="post-title luckiesdesign-main-title"><?php printf(__('Tags: %s', 'luckiesDesign'), '<span>' . single_tag_title('', false) . '</span>'); ?></h2><?php } elseif (is_category()) {
                ?>
                <h2 class="post-title luckiesdesign-main-title"><?php printf(__('Category: %s', 'luckiesDesign'), '<span>' . single_cat_title('', false) . '</span>'); ?></h2><p class="center_text"><?php echo category_description(); ?></p><?php } elseif (is_day()) {
                ?>
                <h2 class="post-title luckiesdesign-main-title"><?php printf(__('Archive for %s', 'luckiesDesign'), '<span>' . get_the_time('F jS, Y') . '</span>'); ?></h2><?php } elseif (is_month()) {
                ?>
                <h2 class="post-title luckiesdesign-main-title"><?php printf(__('Archive for  %s', 'luckiesDesign'), '<span>' . get_the_time('F, Y') . '</span>'); ?></h2><?php } elseif (is_year()) {
                ?>
                <h2 class="post-title luckiesdesign-main-title"><?php printf(__('Archive for  %s', 'luckiesDesign'), '<span>' . get_the_time('Y') . '</span>'); ?></h2><?php
            } elseif (is_author()) {
                $curauth = ( isset($_GET['author_name']) ) ? get_user_by('slug', $author_name) : get_userdata(intval($author));
                ?>
                <h2 class="post-title luckiesdesign-main-title"><?php printf(__('Author: %s', 'luckiesDesign'), '<span>' . $curauth->display_name . '</span>'); ?></h2><?php
            }
            ?>
        </div>
    </div>
    <?php
    $post_array = array();
    /* The Loop */
    if (have_posts()) {
        while (have_posts()) {
            the_post();
            $post_array[] = get_the_ID();
        }
    }
    ?>
    <div id="entries-blog" class="row-fluid luckies_blog">
        <div class="width100 blog_container">
            <?php
            $count = 0;
            $div_once = 'true';
            foreach ($post_array as $single_post_id) {
                if ($count < 1) {
                    ?>
                    <div class="blog_left span6">
                        <span class="span12 large_blog">
                            <?php
                            $post_object = get_post($single_post_id);
                            $post_image = get_the_post_thumbnail($single_post_id, 'full');
                            if (isset($post_image) && !empty($post_image)) {
                                ?>
                                <a href="<?php echo get_the_permalink($single_post_id); ?>"><?php echo $post_image; ?></a>
                                <?php
                            }
                            ?>
                            <a href="<?php echo get_the_permalink($single_post_id); ?>"><h4 class="seperator"><?php echo $post_object->post_title; ?></h4></a>
                            <h5 class="suggesstion">Suggesstion</h5>
                            <p>
                                <?php
                                $post_content = wp_trim_words(($post_object->post_content), 15);
                                echo $post_content;
                                ?>
                            </p>
                            <?php
                            $post_author = get_user_by('id', $post_object->post_author);
                            $author_posts = get_author_posts_url($post_object->post_author);
                            $author_display_name = $post_author->display_name;
                            ?>
                            <p class="small_fonts">Posted by: <a href="<?php echo $author_posts ?>"><span class="bold orange_text"><?php echo $author_display_name; ?></span></a> on <?php the_date('d-m-Y', '<span class="orange_text">', '</span>'); ?></p>
                        </span>
                    </div>
                    <?php
                    array_shift($post_array);
                } elseif ($count < 3) {
                    if ($div_once == 'true') {
                        ?>
                        <div class="blog_right span6">
                        <?php } ?>
                        <span class="span6 small_blog">
                            <?php
                            $post_object = get_post($single_post_id);
                            $post_image = get_the_post_thumbnail($single_post_id, 'full');
                            if (isset($post_image) && !empty($post_image)) {
                                ?>
                                <a href="<?php echo get_the_permalink($single_post_id); ?>"><?php echo $post_image; ?></a>
                                <?php
                            }
                            ?>
                            <a href="<?php echo get_the_permalink($single_post_id); ?>"><h4 class="seperator"><?php echo $post_object->post_title; ?></h4></a>
                            <h5 class="suggesstion">Suggesstion</h5>
                            <p>
                                <?php
                                $post_content = wp_trim_words(($post_object->post_content), 15);
                                echo $post_content;
                                ?>
                            </p>
                            <?php
                            $post_author = get_user_by('id', $post_object->post_author);
                            $author_posts = get_author_posts_url($post_object->post_author);
                            $author_display_name = $post_author->display_name;
                            ?>
                            <p class="small_fonts">Posted by: <a href="<?php echo $author_posts ?>"><span class="bold orange_text"><?php echo $author_display_name; ?></span></a> on <?php the_date('d-m-Y', '<span class="orange_text">', '</span>'); ?></p>
                        </span><!--end of span6 small_blog-->
                        <?php
                        $div_once = 'false';
                        array_shift($post_array);
                    }
                    $count++;
                }
                ?>
            </div>
        </div>

        <div class="width100 blog_container">
            <?php
            $count = 0;
            $div_close = 'true';
            $div_once = 'true';
            foreach ($post_array as $single_post_id) {
                if ($count < 2) {
                    if ($div_once == 'true') {
                        ?>
                        <div class="blog_left span6">
                        <?php } ?>
                        <span class="span6 small_blog">
                            <?php
                            $post_object = get_post($single_post_id);
                            $post_image = get_the_post_thumbnail($single_post_id, 'full');
                            if (isset($post_image) && !empty($post_image)) {
                                ?>
                                <a href="<?php echo get_the_permalink($single_post_id); ?>"><?php echo $post_image; ?></a>
                                <?php
                            }
                            ?>
                            <a href="<?php echo get_the_permalink($single_post_id); ?>"><h4 class="seperator"><?php echo $post_object->post_title; ?></h4></a>
                            <h5 class="suggesstion">Suggesstion</h5>
                            <p>
                                <?php
                                $post_content = wp_trim_words(($post_object->post_content), 15);
                                echo $post_content;
                                ?>
                            </p>
                            <?php
                            $post_author = get_user_by('id', $post_object->post_author);
                            $author_posts = get_author_posts_url($post_object->post_author);
                            $author_display_name = $post_author->display_name;
                            ?>
                            <p class="small_fonts">Posted by: <a href="<?php echo $author_posts ?>"><span class="bold orange_text"><?php echo $author_display_name; ?></span></a> on <?php the_date('d-m-Y', '<span class="orange_text">', '</span>'); ?></p>
                        </span>
                        <?php
                        $div_once = 'false';
                        array_shift($post_array);
                    } elseif ($count < 3) {
                        if ($div_close == 'true') {
                            ?>
                        </div>
                    <?php } ?>
                    <div class="blog_right span6">
                        <span class="span12 small_blog">
                            <?php
                            $post_object = get_post($single_post_id);
                            $post_image = get_the_post_thumbnail($single_post_id, 'full');
                            if (isset($post_image) && !empty($post_image)) {
                                ?>
                                <a href="<?php echo get_the_permalink($single_post_id); ?>"><?php echo $post_image; ?></a>
                                <?php
                            }
                            ?>
                            <a href="<?php echo get_the_permalink($single_post_id); ?>"><h4 class="seperator"><?php echo $post_object->post_title; ?></h4></a>
                            <h5 class="suggesstion">Suggesstion</h5>
                            <p>
                                <?php
                                $post_content = wp_trim_words(($post_object->post_content), 15);
                                echo $post_content;
                                ?>
                            </p>
                            <?php
                            $post_author = get_user_by('id', $post_object->post_author);
                            $author_posts = get_author_posts_url($post_object->post_author);
                            $author_display_name = $post_author->display_name;
                            ?>
                            <p class="small_fonts">Posted by: <a href="<?php echo $author_posts ?>"><span class="bold orange_text"><?php echo $author_display_name; ?></span></a> on <?php the_date('d-m-Y', '<span class="orange_text">', '</span>'); ?></p>
                        </span><!--end of span6 small_blog-->
                    </div>
                    <?php
                    array_shift($post_array);
                    $div_close = 'false';
                }
                $count++;
            }
            ?>
        </div>


        <div class="width100 blog_container">
            <?php
            $count = 0;
            $div_once = 'true';
            foreach ($post_array as $single_post_id) {
                if ($count < 1) {
                    ?>
                    <div class="blog_left span6">
                        <span class="span12 large_blog">
                            <?php
                            $post_object = get_post($single_post_id);
                            $post_image = get_the_post_thumbnail($single_post_id, 'full');
                            if (isset($post_image) && !empty($post_image)) {
                                ?>
                                <a href="<?php echo get_the_permalink($single_post_id); ?>"><?php echo $post_image; ?></a>
                                <?php
                            }
                            ?>
                            <a href="<?php echo get_the_permalink($single_post_id); ?>"><h4 class="seperator"><?php echo $post_object->post_title; ?></h4></a>
                            <h5 class="suggesstion">Suggesstion</h5>
                            <p>
                                <?php
                                $post_content = wp_trim_words(($post_object->post_content), 15);
                                echo $post_content;
                                ?>
                            </p>
                            <?php
                            $post_author = get_user_by('id', $post_object->post_author);
                            $author_posts = get_author_posts_url($post_object->post_author);
                            $author_display_name = $post_author->display_name;
                            ?>
                            <p class="small_fonts">Posted by: <a href="<?php echo $author_posts ?>"><span class="bold orange_text"><?php echo $author_display_name; ?></span></a> on <?php the_date('d-m-Y', '<span class="orange_text">', '</span>'); ?></p>
                        </span>
                    </div>
                    <?php
                    array_shift($post_array);
                } elseif ($count < 3) {
                    if ($div_once == 'true') {
                        ?>
                        <div class="blog_right span6">
                        <?php } ?>
                        <span class="span6 small_blog">
                            <?php
                            $post_object = get_post($single_post_id);
                            $post_image = get_the_post_thumbnail($single_post_id, 'full');
                            if (isset($post_image) && !empty($post_image)) {
                                ?>
                                <a href="<?php echo get_the_permalink($single_post_id); ?>"><?php echo $post_image; ?></a>
                                <?php
                            }
                            ?>
                            <a href="<?php echo get_the_permalink($single_post_id); ?>"><h4 class="seperator"><?php echo $post_object->post_title; ?></h4></a>
                            <h5 class="suggesstion">Suggesstion</h5>
                            <p>
                                <?php
                                $post_content = wp_trim_words(($post_object->post_content), 15);
                                echo $post_content;
                                ?>
                            </p>
                            <?php
                            $post_author = get_user_by('id', $post_object->post_author);
                            $author_posts = get_author_posts_url($post_object->post_author);
                            $author_display_name = $post_author->display_name;
                            ?>
                            <p class="small_fonts">Posted by: <a href="<?php echo $author_posts ?>"><span class="bold orange_text"><?php echo $author_display_name; ?></span></a> on <?php the_date('d-m-Y', '<span class="orange_text">', '</span>'); ?></p>
                        </span><!--end of span6 small_blog-->
                        <?php
                        $div_once = 'false';
                        array_shift($post_array);
                    }
                    $count++;
                }
                ?>
            </div>
        </div>

        <?php luckiesdesign_hook_archive_pagination(); ?>
        <div id="test-div1"></div>

    </div>
</div>