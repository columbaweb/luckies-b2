<?php
/**
 * The Header for luckiesDesign
 *
 * Displays all of the <head> section and everything up till <div id="content-wrapper">
 */
global $luckiesdesign_general;
?><!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" <?php language_attributes(); ?>>
    <head>
        <meta http-equiv="Content-Type" content="<?php bloginfo('html_type'); ?>; charset=<?php bloginfo('charset'); ?>" />
        <title><?php wp_title(''); ?></title>

        <!-- Mobile Viewport Fix ( j.mp/mobileviewport & davidbcalhoun.com/2010/viewport-metatag ) -->
        <meta name="viewport" content="<?php echo apply_filters('luckiesdesign_viewport', 'width=device-width, initial-scale=1.0, maximum-scale=1.0'); ?>" />

        <link rel="profile" href="http://gmpg.org/xfn/11" />
        <?php if ('disable' != $luckiesdesign_general['favicon_use']) { ?><link rel="shortcut icon" type="image/x-icon" href="<?php echo $luckiesdesign_general['favicon_upload']; ?>" /><?php } ?>

        <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />

        <link rel="stylesheet" href="<?php bloginfo('stylesheet_url'); ?>" type="text/css" media="all" />

        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
        <script src="http://cdn.jquerytools.org/1.2.7/full/jquery.tools.min.js"></script>
        

        <!--[if lt IE 9]>
            <script src="<?php echo get_template_directory_uri(); ?>/js/html5.js"></script>
        <![endif]-->

        <?php wp_head(); ?>

        <?php luckiesdesign_head(); ?>
        <script type="text/javascript">

                var ajaxurl = '<?php echo admin_url('admin-ajax.php'); ?>';

                var homeurl = '<?php echo home_url('/'); ?>';

                var styleurl = '<?php echo get_stylesheet_directory_uri(); ?>';

            </script>

        <script type="text/javascript">
            jQuery(document).ready(function() {
                // ajax pagination
                jQuery('.wp-pagenavi a').live('click', function(event) { // if not using wp_pagination, change this to correct ID
                    event.preventDefault();
                    var nextLink = jQuery(this).attr('href');
                    // #main is the ID of the outer div wrapping your posts
                    jQuery('#test-div1').html('<div><h2>Loading...</h2></div>');
                    // #entries is the ID of the inner div wrapping your posts
                    jQuery('#test-div1').load(nextLink + ' #entries-blog')
                });
            }); // end ready function
        </script>
    </head>
    <body <?php body_class(); ?>><!-- ends in footer.php -->

        <?php luckiesdesign_hook_begin_body(); ?>

        <div id="main-wrapper" class="luckiesdesign-container-12 clearfix"><!-- ends in footer.php -->

            <?php luckiesdesign_hook_begin_main_wrapper(); ?>

            <?php $header_class = get_header_image() ? ' luckiesdesign-header-wrapper-image' : ''; ?>
            <header class="luckiesdesign-container clearfix<?php echo $header_class; ?>">

                <?php luckiesdesign_hook_before_header(); ?>

                <div id="header">
                    <div class="width100 top_hdr">
                        <div class="row-fluid">
                            <div class="span12">
                                <?php luckiesdesign_hook_before_logo(); ?>
                                <?php dynamic_sidebar('header_widgetized_area'); ?>
                            </div>
                        </div>
                    </div>

                    <div class="row-fluid">
                        <div class="span6">
                <!--Logo-->
                            <a role="link" class="logo" href="<?php echo home_url('/'); ?>" title="<?php bloginfo('name'); ?>"><?php echo ( 'image' == $luckiesdesign_general['logo_use'] ) ? '<img role="img" alt="' . get_bloginfo('name') . '" ' . luckiesdesign_get_image_dimensions($luckiesdesign_general['logo_upload']) . ' src="' . $luckiesdesign_general['logo_upload'] . '" />' : get_bloginfo('name'); ?></a>
                            <?php luckiesdesign_hook_after_logo(); ?>
                        </div> <!-- span6 end -->    

                        <div class="span6 col2">
                            <div id="header-search" class="span8">
                                <?php get_search_form(); ?>  
                            </div> <!-- header search end -->
                                
                            <div id="header-basket" class="span4">
                                <?php global $woocommerce; ?>
                                <div class="basket-wrap">
                                    <h4>Basket <a class="cart-contents" href="<?php echo $woocommerce->cart->get_cart_url(); ?>" title="<?php _e('View your shopping cart', 'woothemes'); ?>"><?php echo $woocommerce->cart->get_cart_total(); ?></a></h4>
                                    <div class="cart-items">
                                        <a class="cart-contents" href="<?php echo $woocommerce->cart->get_cart_url(); ?>" title="<?php _e('View your shopping cart', 'woothemes'); ?>"><?php echo sprintf(_n('%d', '%d', $woocommerce->cart->cart_contents_count, 'woothemes'), $woocommerce->cart->cart_contents_count); ?></a>
                                    </div>
                                </div>    
                            </div> <!-- header basket end -->    
                        </div> <!-- span6 end --> 

                        <nav class="main_menu">
                            <?php
                            $terms = get_terms('product_cat');

                            if (count($terms) > 0) {
                                ?><ul><?php
                                    foreach ($terms as $term) {
                                        $term_link = get_term_link($term);
                                        ?><li class="<?php echo $term->slug; ?>">
                                            <a href="<?php echo $term_link; ?>" title="<?php echo $term->name; ?>"><?php echo $term->name ?></a>
                                        </li><?php
                                    }
                                    ?>
<!-- temp :) -->
                                        <li class="filter new"><a href="#">New</a></li>
                                        <li class="filter show-all"><a href="#">Show All</a></li>

                                    </ul>

                                    <?php
                            }
                            ?>
                        </nav>

                    </div> <!-- row end -->
                </div><!-- #header -->

                <?php luckiesdesign_hook_after_header(); ?>

            </header><!-- #header-wrapper -->

            <div id="content-wrapper"<?php echo ( is_search() && $luckiesdesign_general['search_code'] && $luckiesdesign_general['search_layout'] ) ? ' class="search-layout-wrapper clearfix"' : ' class="luckiesdesign-container-12 clearfix"'; ?>><!-- ends in footer.php -->
                <?php luckiesdesign_hook_begin_content_wrapper(); ?>