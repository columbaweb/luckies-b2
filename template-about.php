<?php
/**
 * Template Name: About Template
 */
get_header();
?>
<div class="row-fluid about_us_page">
    <!--breadcrumb-->
    <?php get_breadcrumb(); ?>
    <div class="span12 about_grey_box">
        <div class="headeing_border">
            <h2><?php the_title(); ?></h2>
        </div>
        <p class="text_center">
            <?php
            if (have_posts()): while (have_posts()): the_post();
                    the_content();
                endwhile;
            endif;
            ?>
        </p>

    </div><!--end of span12-->
</div>


<!--user gravtars and detaisl-->
<?php
// The Query
$user_query = new WP_User_Query(array('blog_id' => 1, 'who' => 'authors'));
?>
<div class="row-fluid luckies_team">
    <div class="span12">
        <div class="headeing_border">
            <h2>The Luckies Team</h2>
        </div>

        <?php
        // User Loop
        $user_count = 0;
        if (!empty($user_query->results)) {
		
			
            ?>
            
                <?php
				$check_count_rs=count($user_query->results);
                foreach ($user_query->results as $single_user) {
					if($user_count%4 == 0)
					{
						echo '<div class="span12 no_pad team_container">';
					}
                    ?>
                    <div class="span3 no_pad team">
                        <?php
                        echo get_avatar($single_user->ID, 216);
                        if (!empty($single_user->user_firstname)) {
                            ?><h5><?php echo $single_user->user_firstname . ' ' . $single_user->user_lastname; ?></h5><?php
                        } else {
                            ?><h5><?php echo $single_user->display_name; ?></h5><?php
                        }
                        $user_metadata = get_userdata($single_user->ID);
                        ?><h5 class="orange_text"><?php echo implode(', ', $user_metadata->roles); ?></h5><?php
                        ?><p><?php echo $user_metadata->description; ?></p><?php
                        ?><a href="<?php echo get_author_posts_url($single_user->ID); ?>">View Blog Posts</a>
                    </div>
                    <?php
                    $user_count++;
                    array_shift($user_query->results);
					if($user_count%4 == 0)
					{
						echo "</div>";
					}
					if(($check_count_rs == ($user_count)))
					{
						echo "</div>";
					}
               
				}
                ?><?php
				
				
        }
		else
		{
			$user_count = 1;
		}

     
        
        ?>

        <div class="width100 blog-section">
            <div class="row-fluid all_carusl">
                <div class="headeing_border">
                    <h2 class="category-name">Latest Blog Posts</h2>
                    <div class="category-archive"><a href="<?php echo $category_view_all_link1; ?>">View All</a></div>
                </div>
                <div class="span4 latest-post-sidebar">
                    <?php dynamic_sidebar('home-page-post-tabs'); ?>
                </div>

                <div class="span8 latest-post-main">
                    <?php
                    $post_args = array('post_type' => 'post', 'posts_per_page' => 1);
                    $custom_post_query = new WP_Query($post_args);

                    if ($custom_post_query->have_posts()) {
                        while ($custom_post_query->have_posts()) {
                            $custom_post_query->the_post();
                            ?>
                            <div class="span8">
                                <?php
                                if (has_post_thumbnail()) {
                                    echo the_post_thumbnail('full');
                                }
                                ?>
                            </div>
                            <div class="span4">
                                <h3 class="post-title"><a href="<?php the_permalink(); ?>"><span class="orange_text"><?php the_title(); ?></span></a></h3>
                                <div class="post-content"><?php
                                    $post_content = wp_trim_words(get_the_content(), 15);
                                    echo $post_content;
                                    ?></div>
                                <!--<p class="small_fonts">Posted by:<span class="bold orange_text"><?php the_author(); ?></span> on <?php the_date('d-m-Y', '<span class="orange_text">', '</span>'); ?></p>-->
                            </div>

                            <?php
                        }
                    }
                    wp_reset_postdata();
                    wp_reset_query();
                    ?>

                </div>
            </div><!--end of row-fluid-->
        </div>
    </div>
</div><!--end of luckies_team-->
<?php get_footer(); ?>




