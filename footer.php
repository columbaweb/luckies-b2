<?php
/**
 * The template for displaying the footer
 *
 * @package luckiesDesign
 *
 * @since luckiesDesign 2.0
 */
global $luckiesdesign_general;
?>
<div class="width100 brand-section">
    <div class="row-fluid">
        <div class="span12">
            <div class="product-brands">
                <div id="brand-slider" class="flexslider">
                    <ul class="slides">
                        <?php
                        $brands = get_terms('product_brand');
                        foreach ($brands as $single_brand) {
                            ?><li><?php
                                $brand_image = wp_get_attachment_url(get_woocommerce_term_meta($single_brand->term_id, 'thumbnail_id', true));
                                ?><div class="brand-item"><img src="<?php echo $brand_image; ?>" /></div>
                            </li><?php
                        }
                        ?>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<?php luckiesdesign_hook_end_content_wrapper(); ?>
</div><!-- #content-wrapper -->

<footer id="footer-wrapper" role="contentinfo" class="clearfix luckiesdesign-container-12 luckiesdesign-section-separator">

<!-- twitter feed -->
    <section id="twitter-feed" class="light_grey_bg">
        <div class="container-fluid">
            <div class="row-fluid">
                <div class="span12">
                    <?php echo do_shortcode("[rotatingtweets screen_name='fathomcomment' speed='3000' rotation_type='scrollLeft' show_meta_prev_next='0' show_meta_via='0' middot='']" ) ?>
                </div> <!-- span12 end -->
            </div> <!-- row end -->
        </div> <!-- container end --> 
    </section> <!-- twitter feed end -->


<!-- feat footer -->
    <div class="dark_grey_bg">
        <div class="container-fluid">
            <div class="row-fluid">

                <div class="span8">
                    <div class="l-row">
                        <div class="span3">
                            <?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar('Footer 1') ) : ?><?php endif; ?>
                        </div>
                        <div class="span9 categories">
                            <?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar('Footer 2') ) : ?><?php endif; ?>
                        </div> 
                    </div>  

<!-- row 2 -->
                    <div class="l-row">
                        <div class="span3">
                            <?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar('Footer 3') ) : ?><?php endif; ?>
                        </div>
                        <div class="span9">
                            <?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar('Footer 4') ) : ?><?php endif; ?>
                        </div>  
                    </div>                   
                </div> <!-- span8 -->


                <div class="span4">
                    <h3>Get Involved</h3>
                    <p class="sub">Newsletter</p>
                    <form>
                        <input type="text" placeholder="Name" />
                        <input type="email" placeholder="Email Address" />
                        <input type="submit" value="SUBSCRIBE" />
                    </form>

                    <?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar('Footer 5') ) : ?><?php endif; ?>
                </div> <!-- span4 end -->

            </div> <!-- row end -->
        </div> <!-- container end --
    </div><!-- feat footer end -->

</div> <!-- dark grey end -->






    <?php luckiesdesign_hook_before_footer(); ?>
    <?php
    $footer_options = get_option('custom_theme_options');
    $site_map_link = $footer_options['site_map_link'];
    $site_map_text = $footer_options['site_map_text'];
    $cookie_policy_link = $footer_options['cookie_policy_link'];
    $cookie_policy_text = $footer_options['cookie_policy_text'];
    $terms_condition_link = $footer_options['terms_condition_link'];
    $terms_condition_text = $footer_options['terms_condition_text'];
    ?>
    <div class="row-fluid">
        <div class="span12">
            <p class="orange_text footer_text">&copy; <?php
                //echo ' - ';
                bloginfo('name');
                echo date('Y');
                ?> All rights reserverd - <a href="<?php echo $terms_condition_link; ?>"><?php echo $terms_condition_text; ?></a> - <a href="<?php echo $site_map_link; ?>"><?php echo $site_map_text; ?></a> - <a href="<?php echo $cookie_policy_link; ?>"><?php echo $cookie_policy_text; ?></a></p>
        </div>
    </div>      
    <?php luckiesdesign_hook_after_footer(); ?>

</footer><!-- #footer-wrapper-->

<?php luckiesdesign_hook_end_main_wrapper(); ?>

</div><!-- #main-wrapper -->

<?php wp_footer(); ?>

<?php luckiesdesign_hook_end_body(); ?>
<script>
    jQuery(document).ready(function() {
//        var myArray = [];
var pathname = window.location.href;
            jQuery('.brands_filter').on('change', '.brands-checkbox', function() {
      var brand_id = jQuery(this).val();
      
//      jQuery(this).prop('checked');
//      jQuery('.brands-checkbox').each(function (){
//     if (jQuery('input.brands-checkbox').is(':checked')) 
//{
//    alert('l');
//    myArray.push().jQuery(this).val();
//}    
//      });
window.location = pathname+"?filter_product_brand="+brand_id;
      });
      
      
      
      
//      console.log(myArray);
      });
</script>

<script type="text/javascript">
// Can also be used with $(document).ready()
jQuery(window).load(function() {
  jQuery('.gift-slider').flexslider({
    animation: "slide"
  });
  
  jQuery('#category-crousel1').flexslider({
    animation: "slide",
    useCSS : false,
    controlNav: false,
    animationLoop: false,
    slideshow: false,
    itemWidth: 210,
    itemMargin: 5,
    
  });
  jQuery('#category-crousel2').flexslider({
    animation: "slide",
    controlNav: false,
    animationLoop: false,
    slideshow: false,
    itemWidth: 210,
    itemMargin: 5,
    
  });
  jQuery('#category-crousel3').flexslider({
    animation: "slide",
    controlNav: false,
    animationLoop: false,
    slideshow: false,
    itemWidth: 210,
    itemMargin: 5,
    
  });
  jQuery('#brand-slider').flexslider({
    animation: "slide",
    useCSS: false,
    controlNav: false,
    animationLoop: false,
    slideshow: false,
    itemWidth: 210,
    itemMargin: 5,
    
  });
  
  
  jQuery('#product-carousel').flexslider({
    animation: "slide",
    controlNav: false,
    animationLoop: false,
    slideshow: false,
    useCSS: false,
    itemWidth: 84,
    itemMargin: 5,
    asNavFor: '#slider'
  });
 
  jQuery('#product-slider').flexslider({
    animation: "slide",
    controlNav: false,
    animationLoop: false,
    slideshow: false,
    sync: "#carousel"
  });
  
  
  jQuery('.wooslider-prev').addClass('carousel-control');
  jQuery('.wooslider-next').addClass('carousel-control');
  
  jQuery('h2.current').parent().css('background','white');
  
  
//  jQuery(function(){
//  var toggles = jQuery('.toggle a'),
//      codes = jQuery('.code');
//  
//  toggles.on("click", function(event){
//    event.preventDefault();
//    var jQuerythis = jQuery(this);
//    
//    if (!jQuerythis.hasClass("active")) {
//      toggles.removeClass("active");
//      jQuerythis.addClass("active");
//      codes.hide().filter(this.hash).show();
//    }
//  });
//  toggles.first().click();
//});
  jQuery('.cloese-button').click(function(){
    jQuery('.woocommerce-message').fadeOut( "slow" );
  });

});
</script>
</body>
</html>