<?php
/**
 * The template for displaying product content within loops.
 *
 * Override this template by copying it to yourtheme/woocommerce/content-product.php
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     1.6.4
 */
if (!defined('ABSPATH'))
    exit; // Exit if accessed directly

global $product, $woocommerce_loop;

// Store loop count we're currently on
if (empty($woocommerce_loop['loop']))
    $woocommerce_loop['loop'] = 0;

// Store column count for displaying the grid
if (empty($woocommerce_loop['columns']))
    $woocommerce_loop['columns'] = apply_filters('loop_shop_columns', 4);

// Ensure visibility
if (!$product || !$product->is_visible())
    return;

// Increase loop count
$woocommerce_loop['loop'] ++;

// Extra post classes
$classes = array();
if (0 == ( $woocommerce_loop['loop'] - 1 ) % $woocommerce_loop['columns'] || 1 == $woocommerce_loop['columns'])
    $classes[] = 'first';
if (0 == $woocommerce_loop['loop'] % $woocommerce_loop['columns'])
    $classes[] = 'last';
?>
<div class="span4">
    <div class="item">
    <div <?php post_class($classes); ?>>

        <?php do_action('woocommerce_before_shop_loop_item'); ?>

        <a href="<?php the_permalink(); ?>">

            <?php
            /**
             * woocommerce_before_shop_loop_item_title hook
             *
             * @hooked woocommerce_show_product_loop_sale_flash - 10
             * @hooked woocommerce_template_loop_product_thumbnail - 10
             */
            do_action('woocommerce_before_shop_loop_item_title');
            ?>
            <div class="seperator"></div>
            <div class="width100 caro_detail">
                <div class="width100">
                    <div class="span7 fl">
                        <h5><?php the_title(); ?></h5>
                        <h3><?php
                            /**
                             * woocommerce_after_shop_loop_item_title hook
                             *
                             * @hooked woocommerce_template_loop_rating - 5
                             * @hooked woocommerce_template_loop_price - 10
                             */
                            do_action('woocommerce_after_shop_loop_item_title');
                            ?></h3>
                    </div>

                    <div class="span4 fr">
                        <?php do_action('woocommerce_after_shop_loop_item'); ?>
                    </div>
                </div>

                <div class="product_info">
                    <p><?php echo apply_filters('woocommerce_short_description', $post->post_excerpt) ?></p>

                    <a href="<?php the_permalink(); ?>">View Product</a>
                </div>
            </div>



        </a>


    </div>
    </div>
</div>