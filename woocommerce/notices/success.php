<?php
/**
 * Show messages
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     1.6.4
 */
global $post, $product;

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

if ( ! $messages ) return;
?>

<?php foreach ( $messages as $message ) : ?>
<?php if(is_product()){ ?>
	<div class="woocommerce-message">
            <div class="cloese-button">x</div>
            <div class="succes-message orange_text">Product Successfully Added To Your Shopping Cart</div>
            <div class="product-description">
            <div class="product-image-popup">
                <?php
                        if (has_post_thumbnail()) {

                            $image_title = esc_attr(get_the_title(get_post_thumbnail_id()));
                            $image_link = wp_get_attachment_url(get_post_thumbnail_id());
                            $image = get_the_post_thumbnail($post->ID, apply_filters('single_product_large_thumbnail_size', 'shop_single'), array(
                                'title' => $image_title
                            ));
                                                 echo $image;
                        }
                        
                        ?>
            </div>
                <h5><?php the_title(); ?></h5>
                <div class="selected-quantity">Quantity x <?php echo $_POST['quantity']; ?></div>
                <?php
                //$product_price = get_post_meta($post->ID, '_regular_price',TRUE);
                $price = $product->get_price();
                $toatl_price = ($_POST['quantity'])*($price);
                ?>
                <div class="orange_text price-to-pay">Total: <?php echo $toatl_price; ?></div>
            </div>
            <?php
            $shop_page_url = get_permalink( woocommerce_get_page_id( 'shop' ) ); 
            $cart_page_url = get_permalink( woocommerce_get_page_id( 'cart' ) ); 
            
            ?>
            <a href="<?php echo $shop_page_url; ?>"><div class="continue-shopping">Continue Shopping</div></a>
            <a href="<?php echo $cart_page_url; ?>"><div class="continue-shopping cart-page-link">Checkout</div></a>
        </div>
<?php }  else {
    ?><div class="row-fluid"><div class="woocommerce-message"><?php echo wp_kses_post( $message ); ?></div></div><?php    
        } ?>
<?php endforeach; ?>
