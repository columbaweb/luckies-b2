<?php
/**
 * Login Form
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.1.0
 */

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly
?>

<?php wc_print_notices(); ?>

<?php do_action( 'woocommerce_before_customer_login_form' ); ?>

<?php if ( get_option( 'woocommerce_enable_myaccount_registration' ) === 'yes' ) : ?>

<div class="row-fluid luckies_contact luckies_login">
	<div class="span12 grey_box">
            <div class="headeing_border">
                <h2><?php the_title(); ?></h2>
		</div>

<?php endif; ?>
<div class="span6 contact_left">
    <div class="headeing_border">
		<h2><?php _e( 'Login', 'woocommerce' ); ?></h2>
    </div>
		<form method="post" class="login width100 chk_right">

			<?php do_action( 'woocommerce_login_form_start' ); ?>

			<div class="contact_field width100">
					<div class="span12">
				<label for="username"><?php _e( 'Username or email address', 'woocommerce' ); ?> <span class="required">*</span></label>
				<input type="text" class="input-text txt_checkout" name="username" id="username" />
                                        </div>
                        </div>
			<div class="contact_field width100">
					<div class="span12">
				<label for="password"><?php _e( 'Password', 'woocommerce' ); ?> <span class="required">*</span></label>
				<input class="input-text txt_checkout" type="password" name="password" id="password" />
                                        </div>
                        </div>

			<?php do_action( 'woocommerce_login_form' ); ?>

			<div class="contact_field width100">
					<div class="span6">
				<?php wp_nonce_field( 'woocommerce-login' ); ?>
				<input type="submit" class="button btn_orange" name="login" value="<?php _e( 'Login', 'woocommerce' ); ?>" /> 
				<label for="rememberme" class="inline">
                                    <input name="rememberme" type="checkbox" id="rememberme" value="forever" /> <?php _e( 'Remember me', 'woocommerce' ); ?>
				</label>
                                        </div>
                        </div>
                    
			<p class="lost_password">
				<a href="<?php echo esc_url( wc_lostpassword_url() ); ?>"><?php _e( 'Lost your password?', 'woocommerce' ); ?></a>
			</p>

			<?php do_action( 'woocommerce_login_form_end' ); ?>

		</form>

<?php if ( get_option( 'woocommerce_enable_myaccount_registration' ) === 'yes' ) : ?>

	</div>

	<div class="span6 fr contact_right">
<div class="headeing_border">
		<h2><?php _e( 'Register', 'woocommerce' ); ?></h2>
</div>
		<form method="post" class="register width100 chk_right">

			<?php do_action( 'woocommerce_register_form_start' ); ?>

			<?php if ( 'no' === get_option( 'woocommerce_registration_generate_username' ) ) : ?>

				<div class="contact_field width100">
					<div class="span12">
					<label for="reg_username"><?php _e( 'Username', 'woocommerce' ); ?> <span class="required">*</span></label>
					<input type="text" class="input-text" name="username" id="reg_username" value="<?php if ( ! empty( $_POST['username'] ) ) echo esc_attr( $_POST['username'] ); ?>" />
                                        </div>
                                </div>

			<?php endif; ?>

			<div class="contact_field width100">
					<div class="span12">
				<label for="reg_email"><?php _e( 'Email address', 'woocommerce' ); ?> <span class="required">*</span></label>
				<input type="email" class="input-text txt_checkout" name="email" id="reg_email" value="<?php if ( ! empty( $_POST['email'] ) ) echo esc_attr( $_POST['email'] ); ?>" />
                                        </div>
                        </div>

			<?php if ( 'no' === get_option( 'woocommerce_registration_generate_password' ) ) : ?>
	
				<div class="contact_field width100">
					<div class="span12">
					<label for="reg_password"><?php _e( 'Password', 'woocommerce' ); ?> <span class="required">*</span></label>
					<input type="password" class="input-text txt_checkout" name="password" id="reg_password" value="<?php if ( ! empty( $_POST['password'] ) ) echo esc_attr( $_POST['password'] ); ?>" />
                                        </div>
                                </div>

			<?php endif; ?>

			<!-- Spam Trap -->
			<div style="left:-999em; position:absolute;"><label for="trap"><?php _e( 'Anti-spam', 'woocommerce' ); ?></label><input type="text" name="email_2" id="trap" tabindex="-1" /></div>

			<?php do_action( 'woocommerce_register_form' ); ?>
			<?php do_action( 'register_form' ); ?>

			<div class="contact_field width100">
					<div class="span6">
				<?php wp_nonce_field( 'woocommerce-register', 'register' ); ?>
				<input type="submit" class="button btn_orange" name="register" value="<?php _e( 'Register', 'woocommerce' ); ?>" />
                                        </div>
                        </div>

			<?php do_action( 'woocommerce_register_form_end' ); ?>

		</form>

	</div>

</div>
<?php endif; ?>

<?php do_action( 'woocommerce_after_customer_login_form' ); ?>