<?php
/**
 * Description tab
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.0.0
 */

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

global $woocommerce, $post;

$heading = esc_html( apply_filters( 'woocommerce_product_description_heading', __( 'Product Description', 'woocommerce' ) ) );
?>

<h2><?php echo $heading; ?></h2>

<?php the_content(); ?>

<?php

//  Features List
$features_list = array();

//  Check Material
if(($materialInfo = get_field('product_material')) != '')
    $features_list[] = 'Material: ' . $materialInfo;

//  Get Features
if(($featuresInfo = trim(get_field('product_features'))) != '')
    $features_list = array_merge($features_list, explode("\n", $featuresInfo));

?>

<div class="product-extra-info">
    <?php if(sizeof($features_list) > 0) { ?>
    <div class="product-extra-info-part">
        <h4>Features</h4>
        <ul>
            <li><?php echo implode('</li><li>', $features_list); ?></li>
        </ul>
    </div>
    <?php } ?>

    <?php if(($dimensionInfo = get_field('product_dimension')) != '') { ?>
    <div class="product-extra-info-part">
        <h4>Dimensions</h4>
        <p><?php echo $dimensionInfo; ?></p>
    </div>
    <?php } ?>
    <div class="clear"></div>
</div>