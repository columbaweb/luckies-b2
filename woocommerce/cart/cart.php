<?php
/**
 * Cart Page
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.1.0
 */
if (!defined('ABSPATH'))
    exit; // Exit if accessed directly

global $woocommerce;

wc_print_notices();

do_action('woocommerce_before_cart');
?>




<form action="<?php echo esc_url(WC()->cart->get_cart_url()); ?>" method="post">

    <div class="row-fluid basket_page">
        <div class="span12 grey_box cart-page">

            <div class="span12 basket_header">
                <div class="span9">

                    <h3><?php the_title(); ?></h3>
                    <p>Please review the contents of your basket and click "Checkout" when you are happy to proceed.</p>
                    <a class="btn_grey button wc-backward" href="<?php echo get_permalink(wc_get_page_id('shop')); ?>"><?php _e('Continue Shopping', 'woocommerce') ?></a> 
                </div><!--end of span9-->

                <div class="span3">
                    <input type="submit" class="btn_orange checkout-button button alt wc-forward" name="proceed" value="<?php _e('Checkout', 'woocommerce'); ?>" />
                </div>
            </div><!--end of span12 basket_header-->

            <?php do_action('woocommerce_before_cart_table'); ?>
            <div class="span12 basket_body">
                <div class="span12 product_table">


                    <div class="width100 product_table_hdr">
                        <div class="span6 product">
                            <h3><?php _e('Product', 'woocommerce'); ?></h3>
                        </div>
                        <!--                            <div class="span2 remove">
                                                        <h3><?php _e('Total', 'woocommerce'); ?></h3>
                                                    </div>-->
                        <div class="span2 remove">
                            <h3><?php _e('Remove', 'woocommerce'); ?></h3>
                        </div>
                        <div class="span2 quantity">
                            <h3><?php _e('Price', 'woocommerce'); ?></h3>
                        </div>
                        <div class="span2 sub_total hidden_tab">
                            <h3><?php _e('Quantity', 'woocommerce'); ?></h3>
                        </div>
                    </div>
                    <?php do_action('woocommerce_before_cart_contents'); ?>

                    <?php
                    foreach (WC()->cart->get_cart() as $cart_item_key => $cart_item) {
                        $_product = apply_filters('woocommerce_cart_item_product', $cart_item['data'], $cart_item, $cart_item_key);
                        $product_id = apply_filters('woocommerce_cart_item_product_id', $cart_item['product_id'], $cart_item, $cart_item_key);

                        if ($_product && $_product->exists() && $cart_item['quantity'] > 0 && apply_filters('woocommerce_cart_item_visible', true, $cart_item, $cart_item_key)) {
                            ?>
                            <div class="width100 product_table_body <?php echo esc_attr(apply_filters('woocommerce_cart_item_class', 'cart_item', $cart_item, $cart_item_key)); ?>">
                                <div class="span6 product">
                                    <div class="span2">
                                        <?php
                                        $thumbnail = apply_filters('woocommerce_cart_item_thumbnail', $_product->get_image(), $cart_item, $cart_item_key);

                                        if (!$_product->is_visible())
                                            echo $thumbnail;
                                        else
                                            printf('<a href="%s">%s</a>', $_product->get_permalink(), $thumbnail);
                                        ?>
                                    </div>
                                    <div class="span9 fr">
                                        <p class="bold">
                                            <?php
                                            if (!$_product->is_visible())
                                                echo apply_filters('woocommerce_cart_item_name', $_product->get_title(), $cart_item, $cart_item_key);
                                            else
                                                echo apply_filters('woocommerce_cart_item_name', sprintf('<a href="%s">%s</a>', $_product->get_permalink(), $_product->get_title()), $cart_item, $cart_item_key);

                                            // Meta data
                                            echo WC()->cart->get_item_data($cart_item);

                                            // Backorder notification
                                            if ($_product->backorders_require_notification() && $_product->is_on_backorder($cart_item['quantity']))
                                                echo '<p class="backorder_notification">' . __('Available on backorder', 'woocommerce') . '</p>';
                                            ?></p>
                                    </div>

                                </div><!-- end of span6 product -->

                                                <!--                                        <td class="product-subtotal">
                                <?php
                                echo apply_filters('woocommerce_cart_item_subtotal', WC()->cart->get_product_subtotal($_product, $cart_item['quantity']), $cart_item, $cart_item_key);
                                ?>
                                                                                </td>-->

                                <div class="span2 remove">
                                    <?php
                                    echo apply_filters('woocommerce_cart_item_remove_link', sprintf('<a href="%s" class="remove" title="%s">&times;</a>', esc_url(WC()->cart->get_remove_url($cart_item_key)), __('Remove this item', 'woocommerce')), $cart_item_key);
                                    ?>
                                </div><!-- end of span2 remove -->
                                <div class="span2 sub_total hidden_tab">
                                    <span class="price">
                                        <?php
                                        echo apply_filters('woocommerce_cart_item_price', WC()->cart->get_product_price($_product), $cart_item, $cart_item_key);
                                        ?>
                                    </span>
                                </div>
                                <div class="span2 quantity">
                                    <?php
                                    if ($_product->is_sold_individually()) {
                                        $product_quantity = sprintf('1 <input type="hidden" name="cart[%s][qty]" value="1" />', $cart_item_key);
                                    } else {
                                        $product_quantity = woocommerce_quantity_input(array(
                                            'input_name' => "cart[{$cart_item_key}][qty]",
                                            'input_value' => $cart_item['quantity'],
                                            'max_value' => $_product->backorders_allowed() ? '' : $_product->get_stock_quantity(),
                                                ), $_product, false);
                                    }

                                    echo apply_filters('woocommerce_cart_item_quantity', $product_quantity, $cart_item_key);
                                    ?>
                                </div><!-- end of span2 quantity -->
                            </div>
                            <?php
                        }
                    }

                    do_action('woocommerce_cart_contents');
                    ?>

                </div>


            </div>
        </div>


        <?php do_action('woocommerce_after_cart_contents'); ?>

        <div class="span12 other_basket_details">
            <div class="width100 visible_tablet hidden_desktop">
                <input type="radio">
                <p>Add Discount</p>
            </div>
            <div class="span7 white_box promotion_code">
                <h3>Promotion Code(S) and Student Cards</h3>
                <p>If you have a Promotion code or Student card please enter it here</p>

                <h5>Promotion Code</h5>

                <div class="width100">
                    <?php if (WC()->cart->coupons_enabled()) { ?>


                        <input type="text" name="coupon_code" class="input-text span6" id="coupon_code" value="" placeholder="<?php _e('Coupon code', 'woocommerce'); ?>" /> 
                        <input type="submit" class="button span5 btn_grey" name="apply_coupon" value="<?php _e('Apply Coupon', 'woocommerce'); ?>" />

                        <?php do_action('woocommerce_cart_coupon'); ?>


                    <?php } ?>

                        <!--<input type="submit" class="button span5 btn_grey" name="update_cart" value="<?php _e('Update Cart', 'woocommerce'); ?>" />-->



                    <?php do_action('woocommerce_proceed_to_checkout'); ?>

                    <?php wp_nonce_field('woocommerce-cart'); ?>

                </div>
                <a href="#" class="span6 marg_left_10">Help?</a>
                <a href="#" class="span6">Add Another</a>

            </div><!--end of span6 white_box-->

            <div class="span5 white_box fr delivery_billing">
                <?php do_action('woocommerce_cart_collaterals'); ?>

                <?php woocommerce_cart_totals(); ?>

                <?php //woocommerce_shipping_calculator(); ?>

            </div>
        </div><!--end of span12 other_basket_details-->



        <?php do_action('woocommerce_after_cart_table'); ?>
    </div>
</form>

<?php do_action('woocommerce_after_cart'); ?>