<?php
/**
 * The template for displaying product content in the single-product.php template
 *
 * Override this template by copying it to yourtheme/woocommerce/content-single-product.php
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     1.6.4
 */

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly
?>

<?php
	/**
	 * woocommerce_before_single_product hook
	 *
	 * @hooked wc_print_notices - 10
	 */
	 do_action( 'woocommerce_before_single_product' );

	 if ( post_password_required() ) {
	 	echo get_the_password_form();
	 	return;
	 }
?>

<div itemscope itemtype="<?php echo woocommerce_get_product_schema(); ?>" id="product-<?php the_ID(); ?>" <?php post_class(); ?>>

	<?php
		/**
		 * woocommerce_before_single_product_summary hook
		 *
		 * @hooked woocommerce_show_product_sale_flash - 10
		 * @hooked woocommerce_show_product_images - 20
		 */
		do_action( 'woocommerce_before_single_product_summary' );
	?>

	<span class="span4 fr prod_right">

		<?php
			/**
			 * woocommerce_single_product_summary hook
			 *
			 * @hooked woocommerce_template_single_title - 5
			 * @hooked woocommerce_template_single_rating - 10
			 * @hooked woocommerce_template_single_price - 10
			 * @hooked woocommerce_template_single_excerpt - 20
			 * @hooked woocommerce_template_single_add_to_cart - 30
			 * @hooked woocommerce_template_single_meta - 40
			 * @hooked woocommerce_template_single_sharing - 50
			 */
			do_action( 'woocommerce_single_product_summary' );
		?>
            <div class="email-a-friend">
                <div class="tell-a-friend">Email a Friend</div>
            <div class="email-a-friend-form">
                <label>Recipient's Name<span class="orange_text">*</span></label>
                <input type="text" required class="email-a-friend-input" id="friend-name" name="friendname"/>
                <label>Recipient's Email Address<span class="orange_text">*</span></label>
                <input type="email" required class="email-a-friend-input" id="friend-email" name="friendemail"/>
                <label>Your Name<span class="orange_text">*</span></label>
                <input type="text" required class="email-a-friend-input" id="your-name" name="yourname"/>
                <label>Your Email Address Name<span class="orange_text">*</span></label>
                <input type="email" required class="email-a-friend-input" id="your-email" name="youremail"/>
                <label>Your Message<span class="orange_text">*</span></label>
                <textarea required placeholder="Your Message Here..." class="email-a-friend-input" id="your-message" name="yourmessage"></textarea>
                <input type="submit" value="Email A Friend" class="email-a-friend-btn" />
            </div>
                <div class="response-div"></div>
            </div>
            

	</span><!--end of span4 right-->

	

	<meta itemprop="url" content="<?php the_permalink(); ?>" />

</div><!-- #product-<?php the_ID(); ?> -->

<?php do_action( 'woocommerce_after_single_product' ); ?>
