<?php
/**
 * Template Name: Contact Template
 */
get_header();
?>

<!--breadcrumb-->
<?php get_breadcrumb(); ?>


<div class="row-fluid about_us_page">
    <div class="span12 about_grey_box">

        <div class="headeing_border">
            <h2><?php the_title(); ?></h2>
        </div>
        <p class="text_center">
            <?php
            if (have_posts()): while (have_posts()): the_post();
                    the_content();
                endwhile;
            endif;
            ?>
        </p>

    </div><!--end of span12-->
</div>

<div class="row-fluid luckies_contact">
    <div class="span12">
        
    </div>
</div>