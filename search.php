<?php
/**
 * The template for displaying Google Custom Search or WordPress Default Search
 */
get_header(); ?>

<?php global $luckiesdesign_general; ?>

    <section id="content" role="main" class="luckiesdesign-multiple-post<?php echo ( $luckiesdesign_general['search_code'] && $luckiesdesign_general['search_layout'] ) ? ' luckiesdesign-grid-12' : ' luckiesdesign-grid-8'; ?>"><!-- content begins --><?php

        luckiesdesign_hook_begin_content();

        if ( $luckiesdesign_general['search_code'] ) {
            $version = NULL;
            /* Check which version of Google Search Element Code is being used */
            if ( preg_match( '/customSearchControl.draw\(\'cse\'(.*)\)\;/i', $luckiesdesign_general['search_code'], $split_code ) ) {
                $version = 1; // Google Search Element V1 code
            } elseif ( preg_match('/\<gcse:(searchresults-only|searchresults|search).*\>\<\/gcse:(searchresults-only|searchresults|search)\>/i', $luckiesdesign_general['search_code'] ) ) {
                $version = 2; // Google Search Element V2 code
            } ?>
                <h1 class="post-title luckiesdesign-main-title"><?php printf( __( 'Search Results for: %s', 'luckiesDesign' ), '<span>' . get_search_query() . '</span>' ); ?></h1><?php
                if ( 1 == $version ) {
                    $search_code = preg_split('/customSearchControl.draw\(\'cse\'(.*)\)\;/i', $luckiesdesign_general['search_code']);
                    echo $search_code[0];
                    echo $split_code[0];
                    echo "customSearchControl.execute('" . get_search_query() . "');";
                    echo $search_code[1];
                } elseif ( 2 == $version ) {
                    echo preg_replace('/\<gcse:(searchresults-only|searchresults|search)(.*)\>\<\/gcse:(searchresults-only|searchresults|search)\>/i', '<gcse:$1 queryParameterName="s"$2></gcse:$3>', $luckiesdesign_general['search_code'] );
                }
        } else {
            get_template_part( 'loop', 'common' );
        } ?>

        <?php luckiesdesign_hook_end_content(); ?>

    </section><!-- #content -->

    <?php if ( !$luckiesdesign_general['search_code'] || !$luckiesdesign_general['search_layout'] ) luckiesdesign_hook_sidebar(); ?>

<?php get_footer(); ?>