<?php
/**
 * Template Name: FAQ Template
 */
get_header();
?>
<div class="row-fluid faq">
    <!--breadcrumb-->
    <?php get_breadcrumb(); ?>

    <div class="span12 about_grey_box">
        <div class="headeing_border">
            <h2><?php the_title(); ?></h2>
        </div>
        <p class="text_center">
            <?php
            if (have_posts()): while (have_posts()): the_post();
                    the_content();
                endwhile;
            endif;
            ?>
        </p>

        <?php
        $question_id_array = array();
        $faq_args = array('post_type' => 'faqs', 'posts_per_page' => -1);
        $faq_query = new WP_Query($faq_args);

        if ($faq_query->have_posts()) {
            while ($faq_query->have_posts()) {
                $faq_query->the_post();
                $faq_cat_list = wp_get_post_terms($post->ID, 'faq-category', array("fields" => "all"));
                foreach ($faq_cat_list as $single_term) {
                    $single_category = $single_term->name;
                    $question_id_array[$single_category][] = get_the_ID();
                }
            }
        }
        wp_reset_postdata();
        wp_reset_query();

        $num_cats = wp_count_terms('faq-category');
        if ($num_cats % 2 == 0) {
            $half_cat = $num_cats / 2;
        } else {
            $half_cat = ($num_cats + 1) / 2;
        }
        ?>


        <div class="span6 faq_left">
            <?php
            $cat_count = 0;
            foreach ($question_id_array as $category_name => $category_array) {
                if ($cat_count < $half_cat) {
                    ?>
                    <div class="accordion faq-catergory">
                        <h2 class="faq-category-name"><?php echo $category_name; ?></h2>
                        <div class="pane faq-question">
                            <?php
                            foreach ($category_array as $faq_id) {
                                $faq_object = get_post($faq_id);
                                ?>
                                <div class="accordion faq-post">
                                    <h2 class="faq-question-title"><?php echo $faq_object->post_title; ?></h2>
                                    <div class="pane faq-content">
                                        <h2 class="faq-answer"><?php echo $faq_object->post_content; ?></h2>
                                    </div>
                                </div>
                                <?php
                            }
                            array_shift($question_id_array);
                            $cat_count++;
                            ?>
                        </div>  
                    </div><!--end of accordian group sub 1-->
                    <?php
                }
            }
            ?>
        </div>


        <div class="span6 fr faq_right">
            <div class="accordion" id="interventions1">
                <div class="accordion-group">
                    <?php
                    $cat_count = 0;
                    foreach ($question_id_array as $category_name => $category_array) {
                        ?>
                        <div class="accordion faq-catergory">
                            <h2 class="faq-category-name"><?php echo $category_name; ?></h2>
                            <div class="pane faq-question">
                                <?php
                                foreach ($category_array as $faq_id) {
                                    $faq_object = get_post($faq_id);
                                    ?>
                                    <div class="accordion faq-post">
                                        <h2 class="faq-question-title"><?php echo $faq_object->post_title; ?></h2>
                                        <div class="pane faq-content">
                                            <h2 class="faq-answer"><?php echo $faq_object->post_content; ?></h2>
                                        </div>
                                    </div>
                                    <?php
                                }
                                array_shift($question_id_array);
                                $cat_count++;
                                ?>
                            </div>  
                        </div><!--end of accordian group sub 1-->
                        <?php
                    }
                    ?>
                </div>
            </div><!--end of accordian_intervention-->
        </div><!--end of span6 faq_right-->

    </div>
</div>

<?php get_footer(); ?>