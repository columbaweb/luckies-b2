<?php
/* 
 * Generic Single Post Page
 */
get_header(); ?>

<?php
if ( have_posts() ) {
    while( have_posts() ) {
        the_post(); ?> 
    <div class="row-fluid luckies_blog_detail">
	<div class="span12">
	
		<div class="headeing_border">
                    <h2><?php the_title(); ?></h2>
                    <a href="<?php echo home_url('/'); ?>">Back to Category Blog</a>
		</div>
		
		<div class="width100 blog_detail_content">
			<div class="span8 blog_info">
			<?php
                        if(has_post_thumbnail()){
                            echo the_post_thumbnail();
                        }                        
                        ?>
                        <p class="small_fonts">Posted by:<span class="bold orange_text"><?php the_author(); ?></span> on <?php the_date('d-m-Y', '<span class="orange_text">', '</span>'); ?></p>
			
			
                        <?php the_content(); ?>
                        
			</div><!-- end of span7 blog_info-->
			
			<div class="span4 fr blog_info_right">
				<?php dynamic_sidebar('single-post-sidebar'); ?>
			</div><!-- end of span4 fr blog_info_right-->
		</div><!-- end of width100 blog_detail_content-->
		
	</div>
	</div><!--end of luckies_blog-->

	<script type="text/javascript">
		jQuery(document).ready(function() {
		document.getElementById("archive_win").style.display = "none";
		jQuery(".icon_arrow_up_down").addClass("arrow_down");
		jQuery("#archive").click(function(e) {
			jQuery("#archive_win").toggle("slow");
			
			if (document.getElementById("archive_win").style.display == "block") {
				jQuery(".icon_arrow_up_down").addClass("arrow_up");
				jQuery(".icon_arrow_up_down").removeClass("arrow_down");
				
			}
			if (document.getElementById("archive_win").style.display == "none") {
				jQuery(".icon_arrow_up_down").removeClass("arrow_up");
				jQuery(".icon_arrow_up_down").addClass("arrow_down");
		
				
			}
			
		});
		
		
	});
	</script>
	
	
	<script type="text/javascript">
		jQuery(document).ready(function() {
		document.getElementById("category_win").style.display = "none";
		jQuery(".icon_arrow_up_down").addClass("arrow_down");
		jQuery("#category").click(function(e) {
			jQuery("#category_win").toggle("slow");
			
			if (document.getElementById("category_win").style.display == "block") {
				jQuery(".icon_arrow_up_down").addClass("arrow_up");
				jQuery(".icon_arrow_up_down").removeClass("arrow_down");
				
			}
			if (document.getElementById("category_win").style.display == "none") {
				jQuery(".icon_arrow_up_down").removeClass("arrow_up");
				jQuery(".icon_arrow_up_down").addClass("arrow_down");
		
				
			}
			
		});
		
		
	});
	</script>
<?php
        }
}
?>
        <?php get_footer(); ?>