<?php
/**
 * The generic template file
 */
get_header();
?>

<?php luckiesdesign_hook_begin_content(); ?>

        <?php get_template_part( 'loop', 'common' ); ?>

        <?php luckiesdesign_hook_end_content(); ?>

<?php get_footer(); ?>