<?php
/**
 * Template Name: Full-Width Template
 */
get_header();
$content_width = $max_content_width; ?>

    <section id="content" class="luckiesdesign-grid-12 luckiesdesign-full-width">
        <?php luckiesdesign_hook_begin_content(); ?>

        <?php get_template_part( 'loop', 'common' ); ?>

        <?php luckiesdesign_hook_end_content(); ?>
    </section><!-- #content -->

<?php get_footer(); ?>