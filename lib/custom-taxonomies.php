<?php

/**
 * Custom Taxonomies
 *
 */

/**
 * Registers Taxonomies
 * 
 */
function create_faq_category_taxonomies() {

    /* Post Custom Taxonomy */
    register_taxonomy('faq-category', 'faqs', array(
        'hierarchical' => true,
        'update_count_callback' => '',
        'rewrite' => true,
        'query_var' => 'faq-category',
        'public' => true,
        'show_ui' => null,
        'show_tagcloud' => null,
        '_builtin' => false,
        'labels' => array(
            'name' => _x('Questions Category', 'taxonomy general name', 'luckiesDesign'),
            'singular_name' => _x('Question Category', 'taxonomy singular name', 'luckiesDesign'),
            'search_items' => __('Search Questions Category', 'luckiesDesign'),
            'all_items' => __('All Questions Category', 'luckiesDesign'),
            'parent_item' => array(null, __('Parent Questions Category', 'luckiesDesign')),
            'parent_item_colon' => array(null, __('Parent Questions Category:', 'luckiesDesign')),
            'edit_item' => __('Edit Questions Category', 'luckiesDesign'),
            'view_item' => __('View Questions Category', 'luckiesDesign'),
            'update_item' => __('Update Questions Category', 'luckiesDesign'),
            'add_new_item' => __('Add New Questions Category', 'luckiesDesign'),
            'new_item_name' => __('New Custom Questions Category', 'luckiesDesign')),
        'capabilities' => array(),
        'show_in_nav_menus' => null,
        'label' => __('Questions Category', 'luckiesDesign'),
        'sort' => true,
        'args' => array('orderby' => 'term_order'))
    );
}

add_action('init', 'create_faq_category_taxonomies');
