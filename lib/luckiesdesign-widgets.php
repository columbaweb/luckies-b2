<?php
/**
 * luckiesDesign Custom Widgets
 *
 * A small 'icing on cake' ;)
 *
 * @package luckiesDesign
 */

/**
 * Custom Widget for FeedBurner RSS Subscription and Social Share
 */
class luckiesdesign_subscribe_widget extends WP_Widget {

    /**
     * Constructor
     *
     * @return void
     * */
    function luckiesdesign_subscribe_widget() {
        $widget_ops = array('classname' => 'luckiesdesign-subscribe-widget-container', 'description' => __('Widget Social Icons such as Facebook, Twitter, etc.', 'luckiesDesign'));
        $this->WP_Widget('rt-subscribe-widget', __('luckiesDesign: Subscribe Widget', 'luckiesDesign'), $widget_ops);
    }

    /**
     * Outputs the HTML
     *
     * @param array An array of standard parameters for widgets in this theme
     * @param array An array of settings for this widget instance
     * @return void Echoes it's output
     * */
    function widget($args, $instance) {
        extract($args, EXTR_SKIP);
        $title = empty($instance['title']) ? '' : apply_filters('widget_title', $instance['title']);
        $facebook_link = empty($instance['facebook_link']) ? '' : $instance['facebook_link'];
        $twitter_link = empty($instance['twitter_link']) ? '' : $instance['twitter_link'];
        $google_link = empty($instance['google_link']) ? '' : $instance['google_link'];
        $rss_link = empty($instance['rss_link']) ? '' : $instance['rss_link'];
        $pintrest_link = empty($instance['pintrest_link']) ? '' : $instance['pintrest_link'];
        $tumbler_link = empty($instance['tumbler_link']) ? '' : $instance['tumbler_link'];
        $vimeo_link = empty($instance['vimeo_link']) ? '' : $instance['vimeo_link'];
        $youtube_link = empty($instance['youtube_link']) ? '' : $instance['youtube_link'];
        $instagram_link = empty($instance['instagram_link']) ? '' : $instance['instagram_link'];
        $luckiesdesign_link_target = isset($instance['luckiesdesign_link_target']) ? $instance['luckiesdesign_link_target'] : true;
        $luckiesdesign_facebook_show = isset($instance['luckiesdesign_show_facebook']) ? $instance['luckiesdesign_show_facebook'] : true;
        $luckiesdesign_google_show = isset($instance['luckiesdesign_show_google']) ? $instance['luckiesdesign_show_google'] : true;
        $luckiesdesign_twitter_show = isset($instance['luckiesdesign_show_twitter']) ? $instance['luckiesdesign_show_twitter'] : true;
        $luckiesdesign_rss_show = isset($instance['luckiesdesign_show_rss']) ? $instance['luckiesdesign_show_rss'] : true;
        $luckiesdesign_youtube_show = isset($instance['luckiesdesign_show_youtube']) ? $instance['luckiesdesign_show_youtube'] : true;
        $luckiesdesign_instagram_show = isset($instance['luckiesdesign_show_instagram']) ? $instance['luckiesdesign_show_instagram'] : true;
        $luckiesdesign_vimeo_show = isset($instance['luckiesdesign_show_vimeo']) ? $instance['luckiesdesign_show_vimeo'] : true;
        $luckiesdesign_tumbler_show = isset($instance['luckiesdesign_show_tumbler']) ? $instance['luckiesdesign_show_tumbler'] : true;
        $luckiesdesign_pintrest_show = isset($instance['luckiesdesign_show_pintrest']) ? $instance['luckiesdesign_show_pintrest'] : true;
        $no_options = 0;

        echo $before_widget;
        if ($title)
            echo $before_title . $title . $after_title;
        ?>

        <div class="social_media_header">
            <?php
            $target = ( $luckiesdesign_link_target ) ? ' target="_blank"' : '';

            if (( $luckiesdesign_facebook_show && $facebook_link ) || ( $luckiesdesign_pintrest_show && $pintrest_link ) || ( $luckiesdesign_tumbler_show && $tumbler_link ) || ( $luckiesdesign_vimeo_show && $vimeo_link ) || ( $luckiesdesign_instagram_show && $instagram_link ) || ( $luckiesdesign_youtube_show && $youtube_link ) || ( $luckiesdesign_twitter_show && $twitter_link ) || ( $luckiesdesign_google_show && $google_link ) || ( $luckiesdesign_rss_show && $rss_link ) || ( $luckiesdesign_linkedin_show && $linkedin_link )) {
                $no_options++;

                echo ( $luckiesdesign_facebook_show && $facebook_link ) ? '<a role="link" rel="nofollow"' . $target . ' class="facebook-bw" href="' . $facebook_link . '" title="' . __('Like Us on Facebook', 'luckiesDesign') . '">Facebook</a>' : '';
                echo ( $luckiesdesign_twitter_show && $twitter_link ) ? '<a role="link" rel="nofollow"' . $target . ' class="twitter-bw" href="' . $twitter_link . '" title="' . __('Follow Us on Twitter', 'luckiesDesign') . '">Twitter</a>' : '';
                echo ( $luckiesdesign_google_show && $google_link ) ? '<a role="link" rel="nofollow"' . $target . ' class="google-bw" href="' . $google_link . '" title="' . __('Add to Circle', 'luckiesDesign') . '">Google</a>' : '';
                echo ( $luckiesdesign_pintrest_show && $pintrest_link ) ? '<a role="link" rel="nofollow"' . $target . ' class="pintrest-bw" href="' . $pintrest_link . '" title="' . __('Pintrest', 'luckiesDesign') . '">Pintrest</a>' : '';
                echo ( $luckiesdesign_tumbler_show && $tumbler_link ) ? '<a role="link" rel="nofollow"' . $target . ' class="tumbler-bw" href="' . $tumbler_link . '" title="' . __('Tumbler', 'luckiesDesign') . '">Tumbler</a>' : '';
                echo ( $luckiesdesign_vimeo_show && $vimeo_link ) ? '<a role="link" rel="nofollow"' . $target . ' class="vimeo-bw" href="' . $vimeo_link . '" title="' . __('Vimeo', 'luckiesDesign') . '">Vimeo</a>' : '';
                echo ( $luckiesdesign_youtube_show && $youtube_link ) ? '<a role="link" rel="nofollow"' . $target . ' class="youtube-bw" href="' . $youtube_link . '" title="' . __('Youtube', 'luckiesDesign') . '">Youtube</a>' : '';
                echo ( $luckiesdesign_instagram_show && $instagram_link ) ? '<a role="link" rel="nofollow"' . $target . ' class="instagram-bw" href="' . $instagram_link . '" title="' . __('Instagram', 'luckiesDesign') . '">Instagram</a>' : '';
                echo ( $luckiesdesign_rss_show && $rss_link ) ? '<a role="link" rel="nofollow"' . $target . ' class="rss-bw" href="' . $rss_link . '" title="' . __('Subscribe via RSS', 'luckiesDesign') . '">RSS</a>' : '';
            }

            if (!$no_options) {
                ?>
                <p><?php printf(__('Please configure this widget <a href="%s" target="_blank" title="Configure Subscribe Widget">here</a>.', 'luckiesDesign'), admin_url('/widgets.php')); ?></p><?php }
            ?>
        </div> <!-- end email-subscription-container -->
        <?php
        echo $after_widget;
    }

    /**
     * Deals with the settings when they are saved by the admin
     * */
    function update($new_instance, $old_instance) {
        $instance = $old_instance;
        $instance['title'] = strip_tags($new_instance['title']);
        $instance['rss_link'] = esc_url_raw($new_instance['rss_link']);
        $instance['vimeo_link'] = esc_url_raw($new_instance['vimeo_link']);
        $instance['tumbler_link'] = esc_url_raw($new_instance['tumbler_link']);
        $instance['youtube_link'] = esc_url_raw($new_instance['youtube_link']);
        $instance['instagram_link'] = esc_url_raw($new_instance['instagram_link']);
        $instance['twitter_link'] = esc_url_raw($new_instance['twitter_link']);
        $instance['pintrest_link'] = esc_url_raw($new_instance['pintrest_link']);
        $instance['facebook_link'] = esc_url_raw($new_instance['facebook_link']);
        $instance['google_link'] = esc_url_raw($new_instance['google_link']);
        $instance['luckiesdesign_link_target'] = !empty($new_instance['luckiesdesign_link_target']) ? 1 : 0;
        $instance['luckiesdesign_show_rss'] = !empty($new_instance['luckiesdesign_show_rss']) ? 1 : 0;
        $instance['luckiesdesign_show_facebook'] = !empty($new_instance['luckiesdesign_show_facebook']) ? 1 : 0;
        $instance['luckiesdesign_show_pintrest'] = !empty($new_instance['luckiesdesign_show_pintrest']) ? 1 : 0;
        $instance['luckiesdesign_show_youtube'] = !empty($new_instance['luckiesdesign_show_youtube']) ? 1 : 0;
        $instance['luckiesdesign_show_vimeo'] = !empty($new_instance['luckiesdesign_show_vimeo']) ? 1 : 0;
        $instance['luckiesdesign_show_instagram'] = !empty($new_instance['luckiesdesign_show_instagram']) ? 1 : 0;
        $instance['luckiesdesign_show_pintrest'] = !empty($new_instance['luckiesdesign_show_pintrest']) ? 1 : 0;
        $instance['luckiesdesign_show_tumbler'] = !empty($new_instance['luckiesdesign_show_tumbler']) ? 1 : 0;
        $instance['luckiesdesign_show_twitter'] = !empty($new_instance['luckiesdesign_show_twitter']) ? 1 : 0;
        $instance['luckiesdesign_show_google'] = !empty($new_instance['luckiesdesign_show_google']) ? 1 : 0;
        return $instance;
    }

    /**
     * Displays the form on the Widgets page of the WP Admin area
     * */
    function form($instance) {
        $defaults = array('luckiesdesign_show_rss' => '0', 'luckiesdesign_show_tumbler' => '0', 'luckiesdesign_show_youtube' => '0', 'luckiesdesign_show_instagram' => '0', 'luckiesdesign_show_vimeo' => '0', 'luckiesdesign_show_pintrest' => '0', 'luckiesdesign_show_facebook' => '0', 'luckiesdesign_show_twitter' => '0', 'luckiesdesign_show_google' => '0', 'luckiesdesign_link_target' => '1');
        // update instance's default options
        $instance = wp_parse_args((array) $instance, $defaults);

        $title = isset($instance['title']) ? esc_attr(( $instance['title'])) : '';
        $rss_link = isset($instance['rss_link']) ? esc_url($instance['rss_link']) : '';
        $twitter_link = isset($instance['twitter_link']) ? esc_url($instance['twitter_link']) : '';
        $facebook_link = isset($instance['facebook_link']) ? esc_url($instance['facebook_link']) : '';
        $google_link = isset($instance['google_link']) ? esc_url($instance['google_link']) : '';
        $tumbler_link = isset($instance['pintrest_link']) ? esc_url($instance['pintrest_link']) : '';
        $youtube_link = isset($instance['tumbler_link']) ? esc_url($instance['tumbler_link']) : '';
        $pintrest_link = isset($instance['youtube_link']) ? esc_url($instance['youtube_link']) : '';
        $instagram_link = isset($instance['instagram_link']) ? esc_url($instance['instagram_link']) : '';
        $vimeo_link = isset($instance['vimeo_link']) ? esc_url($instance['vimeo_link']) : '';

        $luckiesdesign_show_rss = isset($instance['luckiesdesign_show_rss']) ? (bool) $instance['luckiesdesign_show_rss'] : false;
        $luckiesdesign_show_facebook = isset($instance['luckiesdesign_show_facebook']) ? (bool) $instance['luckiesdesign_show_facebook'] : false;
        $luckiesdesign_show_twitter = isset($instance['luckiesdesign_show_twitter']) ? (bool) $instance['luckiesdesign_show_twitter'] : false;
        $luckiesdesign_show_google = isset($instance['luckiesdesign_show_google']) ? (bool) $instance['luckiesdesign_show_google'] : false;
        $luckiesdesign_show_youtube = isset($instance['luckiesdesign_show_youtube']) ? (bool) $instance['luckiesdesign_show_youtube'] : false;
        $luckiesdesign_show_instagram = isset($instance['luckiesdesign_show_instagram']) ? (bool) $instance['luckiesdesign_show_instagram'] : false;
        $luckiesdesign_show_pintrest = isset($instance['luckiesdesign_show_pintrest']) ? (bool) $instance['luckiesdesign_show_pintrest'] : false;
        $luckiesdesign_show_tumbler = isset($instance['luckiesdesign_show_tumbler']) ? (bool) $instance['luckiesdesign_show_tumbler'] : false;
        $luckiesdesign_show_vimeo = isset($instance['luckiesdesign_show_vimeo']) ? (bool) $instance['luckiesdesign_show_vimeo'] : false;
        $luckiesdesign_link_target = isset($instance['luckiesdesign_link_target']) ? (bool) $instance['luckiesdesign_link_target'] : false;
        ?>

        <p><label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Title', 'luckiesDesign'); ?>: </label><input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" role="textbox" value="<?php echo esc_attr($title); ?>" /></p>
        <p><strong><?php _e('Social Share', 'luckiesDesign'); ?>:</strong></p>
        <p>
            <input role="checkbox" type="checkbox" name="<?php echo $this->get_field_name('luckiesdesign_show_facebook'); ?>" id="<?php echo $this->get_field_id('luckiesdesign_show_facebook'); ?>" <?php checked($luckiesdesign_show_facebook); ?> />
            <label for="<?php echo $this->get_field_id('luckiesdesign_show_facebook'); ?>"><?php _e('Facebook Link', 'luckiesDesign'); ?>: </label>
            <input class="widefat" id="<?php echo $this->get_field_id('facebook_link'); ?>" name="<?php echo $this->get_field_name('facebook_link'); ?>" type="text" role="textbox" value="<?php echo esc_attr($facebook_link); ?>" />
        </p>
        <p>
            <input role="checkbox" type="checkbox" name="<?php echo $this->get_field_name('luckiesdesign_show_twitter'); ?>" id="<?php echo $this->get_field_id('luckiesdesign_show_twitter'); ?>" <?php checked($luckiesdesign_show_twitter); ?> />
            <label for="<?php echo $this->get_field_id('luckiesdesign_show_twitter'); ?>"><?php _e('Twitter Link', 'luckiesDesign'); ?>: </label>
            <input class="widefat" id="<?php echo $this->get_field_id('twitter_link'); ?>" name="<?php echo $this->get_field_name('twitter_link'); ?>" type="text" role="textbox" value="<?php echo esc_attr($twitter_link); ?>" />
        </p>
        <p>
            <input role="checkbox" type="checkbox" name="<?php echo $this->get_field_name('luckiesdesign_show_google'); ?>" id="<?php echo $this->get_field_id('luckiesdesign_show_google'); ?>" <?php checked($luckiesdesign_show_google); ?> />
            <label for="<?php echo $this->get_field_id('luckiesdesign_show_google'); ?>"><?php _e('Google+ Link', 'luckiesDesign'); ?>: </label>
            <input class="widefat" id="<?php echo $this->get_field_id('google_link'); ?>" name="<?php echo $this->get_field_name('google_link'); ?>" type="text" role="textbox" value="<?php echo esc_attr($google_link); ?>" />
        </p>
        <p>
            <input role="checkbox" type="checkbox" name="<?php echo $this->get_field_name('luckiesdesign_show_pintrest'); ?>" id="<?php echo $this->get_field_id('luckiesdesign_show_pintrest'); ?>" <?php checked($luckiesdesign_show_pintrest); ?> />
            <label for="<?php echo $this->get_field_id('luckiesdesign_show_pintrest'); ?>"><?php _e('Pintrest Link', 'luckiesDesign'); ?>: </label>
            <input class="widefat" id="<?php echo $this->get_field_id('pintrest_link'); ?>" name="<?php echo $this->get_field_name('pintrest_link'); ?>" type="text" role="textbox" value="<?php echo esc_attr($pintrest_link); ?>" />
        </p>
        <p>
            <input role="checkbox" type="checkbox" name="<?php echo $this->get_field_name('luckiesdesign_show_tumbler'); ?>" id="<?php echo $this->get_field_id('luckiesdesign_show_tumbler'); ?>" <?php checked($luckiesdesign_show_tumbler); ?> />
            <label for="<?php echo $this->get_field_id('luckiesdesign_show_tumbler'); ?>"><?php _e('Tumbler Link', 'luckiesDesign'); ?>: </label>
            <input class="widefat" id="<?php echo $this->get_field_id('tumbler_link'); ?>" name="<?php echo $this->get_field_name('tumbler_link'); ?>" type="text" role="textbox" value="<?php echo esc_attr($tumbler_link); ?>" />
        </p>
        <p>
            <input role="checkbox" type="checkbox" name="<?php echo $this->get_field_name('luckiesdesign_show_vimeo'); ?>" id="<?php echo $this->get_field_id('luckiesdesign_show_vimeo'); ?>" <?php checked($luckiesdesign_show_vimeo); ?> />
            <label for="<?php echo $this->get_field_id('luckiesdesign_show_vimeo'); ?>"><?php _e('Vimeo Link', 'luckiesDesign'); ?>: </label>
            <input class="widefat" id="<?php echo $this->get_field_id('vimeo_link'); ?>" name="<?php echo $this->get_field_name('vimeo_link'); ?>" type="text" role="textbox" value="<?php echo esc_attr($vimeo_link); ?>" />
        </p>
        <p>
            <input role="checkbox" type="checkbox" name="<?php echo $this->get_field_name('luckiesdesign_show_youtube'); ?>" id="<?php echo $this->get_field_id('luckiesdesign_show_youtube'); ?>" <?php checked($luckiesdesign_show_youtube); ?> />
            <label for="<?php echo $this->get_field_id('luckiesdesign_show_youtube'); ?>"><?php _e('Youtube Link', 'luckiesDesign'); ?>: </label>
            <input class="widefat" id="<?php echo $this->get_field_id('youtube_link'); ?>" name="<?php echo $this->get_field_name('youtube_link'); ?>" type="text" role="textbox" value="<?php echo esc_attr($youtube_link); ?>" />
        </p>
        <p>
            <input role="checkbox" type="checkbox" name="<?php echo $this->get_field_name('luckiesdesign_show_instagram'); ?>" id="<?php echo $this->get_field_id('luckiesdesign_show_instagram'); ?>" <?php checked($luckiesdesign_show_instagram); ?> />
            <label for="<?php echo $this->get_field_id('luckiesdesign_show_instagram'); ?>"><?php _e('Instagram Link', 'luckiesDesign'); ?>: </label>
            <input class="widefat" id="<?php echo $this->get_field_id('instagram_link'); ?>" name="<?php echo $this->get_field_name('instagram_link'); ?>" type="text" role="textbox" value="<?php echo esc_attr($instagram_link); ?>" />
        </p>
        <p>
            <input role="checkbox" type="checkbox" name="<?php echo $this->get_field_name('luckiesdesign_show_rss'); ?>" id="<?php echo $this->get_field_id('luckiesdesign_show_rss'); ?>" <?php checked($luckiesdesign_show_rss); ?> />
            <label for="<?php echo $this->get_field_id('luckiesdesign_show_rss'); ?>"><?php _e('RSS Feed Link', 'luckiesDesign'); ?>: </label>
            <input class="widefat" id="<?php echo $this->get_field_id('rss_link'); ?>" name="<?php echo $this->get_field_name('rss_link'); ?>" type="text" role="textbox" value="<?php echo esc_attr($rss_link); ?>" />
        </p>
        <p>
            <input class="link_target" id="<?php echo $this->get_field_id('luckiesdesign_link_target'); ?>" name="<?php echo $this->get_field_name('luckiesdesign_link_target'); ?>" role="checkbox" role="checkbox" role="checkbox" role="checkbox" type="checkbox" <?php checked($luckiesdesign_link_target); ?> />
            <label for="<?php echo $this->get_field_id('luckiesdesign_link_target'); ?>"><?php _e('Open Social Links in New Tab/Window', 'luckiesDesign'); ?></label>
        </p><?php
    }

}

/**
 * Custom Widget for FeedBurner RSS Subscription and Social Share
 */
class luckiesdesign_subscribe_widget_footer extends WP_Widget {

    /**
     * Constructor
     *
     * @return void
     * */
    function luckiesdesign_subscribe_widget_footer() {
        $widget_ops = array('classname' => 'luckiesdesign-subscribe-widget-container', 'description' => __('Widget Social Icons such as Facebook, Twitter, etc.', 'luckiesDesign'));
        $this->WP_Widget('rt-subscribe-widget-footer', __('luckiesDesign: Footer Subscribe Widget ', 'luckiesDesign'), $widget_ops);
    }

    /**
     * Outputs the HTML
     *
     * @param array An array of standard parameters for widgets in this theme
     * @param array An array of settings for this widget instance
     * @return void Echoes it's output
     * */
    function widget($args, $instance) {
        extract($args, EXTR_SKIP);
        $title = empty($instance['title']) ? '' : apply_filters('widget_title', $instance['title']);
        $facebook_link = empty($instance['facebook_link']) ? '' : $instance['facebook_link'];
        $twitter_link = empty($instance['twitter_link']) ? '' : $instance['twitter_link'];
        $google_link = empty($instance['google_link']) ? '' : $instance['google_link'];
        $rss_link = empty($instance['rss_link']) ? '' : $instance['rss_link'];
        $pintrest_link = empty($instance['pintrest_link']) ? '' : $instance['pintrest_link'];
        $tumbler_link = empty($instance['tumbler_link']) ? '' : $instance['tumbler_link'];
        $vimeo_link = empty($instance['vimeo_link']) ? '' : $instance['vimeo_link'];
        $youtube_link = empty($instance['youtube_link']) ? '' : $instance['youtube_link'];
        $instagram_link = empty($instance['instagram_link']) ? '' : $instance['instagram_link'];
        $luckiesdesign_link_target = isset($instance['luckiesdesign_link_target']) ? $instance['luckiesdesign_link_target'] : true;
        $luckiesdesign_facebook_show = isset($instance['luckiesdesign_show_facebook']) ? $instance['luckiesdesign_show_facebook'] : true;
        $luckiesdesign_google_show = isset($instance['luckiesdesign_show_google']) ? $instance['luckiesdesign_show_google'] : true;
        $luckiesdesign_twitter_show = isset($instance['luckiesdesign_show_twitter']) ? $instance['luckiesdesign_show_twitter'] : true;
        $luckiesdesign_rss_show = isset($instance['luckiesdesign_show_rss']) ? $instance['luckiesdesign_show_rss'] : true;
        $luckiesdesign_youtube_show = isset($instance['luckiesdesign_show_youtube']) ? $instance['luckiesdesign_show_youtube'] : true;
        $luckiesdesign_instagram_show = isset($instance['luckiesdesign_show_instagram']) ? $instance['luckiesdesign_show_instagram'] : true;
        $luckiesdesign_vimeo_show = isset($instance['luckiesdesign_show_vimeo']) ? $instance['luckiesdesign_show_vimeo'] : true;
        $luckiesdesign_tumbler_show = isset($instance['luckiesdesign_show_tumbler']) ? $instance['luckiesdesign_show_tumbler'] : true;
        $luckiesdesign_pintrest_show = isset($instance['luckiesdesign_show_pintrest']) ? $instance['luckiesdesign_show_pintrest'] : true;
        $no_options = 0;

        echo $before_widget;
        if ($title)
            
            ?>

        <h4 class="text_upper"><?php echo $title; ?></h4>
        <div class="width100 social_media">
            <?php
            $target = ( $luckiesdesign_link_target ) ? ' target="_blank"' : '';

            if (( $luckiesdesign_facebook_show && $facebook_link ) || ( $luckiesdesign_pintrest_show && $pintrest_link ) || ( $luckiesdesign_tumbler_show && $tumbler_link ) || ( $luckiesdesign_vimeo_show && $vimeo_link ) || ( $luckiesdesign_instagram_show && $instagram_link ) || ( $luckiesdesign_youtube_show && $youtube_link ) || ( $luckiesdesign_twitter_show && $twitter_link ) || ( $luckiesdesign_google_show && $google_link ) || ( $luckiesdesign_rss_show && $rss_link ) || ( $luckiesdesign_linkedin_show && $linkedin_link )) {
                $no_options++;

                echo ( $luckiesdesign_facebook_show && $facebook_link ) ? '<a role="link" rel="nofollow"' . $target . ' class="facebook-bw" href="' . $facebook_link . '" title="' . __('Like Us on Facebook', 'luckiesDesign') . '"><span class="icon_social facebook"></span></a>' : '';
                echo ( $luckiesdesign_twitter_show && $twitter_link ) ? '<a role="link" rel="nofollow"' . $target . ' class="twitter-bw" href="' . $twitter_link . '" title="' . __('Follow Us on Twitter', 'luckiesDesign') . '"><span class="icon_social twiiter"></span></a>' : '';
                echo ( $luckiesdesign_google_show && $google_link ) ? '<a role="link" rel="nofollow"' . $target . ' class="google-bw" href="' . $google_link . '" title="' . __('Add to Circle', 'luckiesDesign') . '"><span class="icon_social google_plus"></span></a>' : '';
                echo ( $luckiesdesign_pintrest_show && $pintrest_link ) ? '<a role="link" rel="nofollow"' . $target . ' class="pintrest-bw" href="' . $pintrest_link . '" title="' . __('Pintrest', 'luckiesDesign') . '"><span class="icon_social pintrest"></span></a>' : '';
                echo ( $luckiesdesign_tumbler_show && $tumbler_link ) ? '<a role="link" rel="nofollow"' . $target . ' class="tumbler-bw" href="' . $tumbler_link . '" title="' . __('Tumbler', 'luckiesDesign') . '"><span class="icon_social t"></span></a>' : '';
                echo ( $luckiesdesign_vimeo_show && $vimeo_link ) ? '<a role="link" rel="nofollow"' . $target . ' class="vimeo-bw" href="' . $vimeo_link . '" title="' . __('Vimeo', 'luckiesDesign') . '"><span class="icon_social vimeo"></span></a>' : '';
                echo ( $luckiesdesign_youtube_show && $youtube_link ) ? '<a role="link" rel="nofollow"' . $target . ' class="youtube-bw" href="' . $youtube_link . '" title="' . __('Youtube', 'luckiesDesign') . '"><span class="icon_social you_tube"></span></a>' : '';
                echo ( $luckiesdesign_instagram_show && $instagram_link ) ? '<a role="link" rel="nofollow"' . $target . ' class="instagram-bw" href="' . $instagram_link . '" title="' . __('Instagram', 'luckiesDesign') . '"><span class="icon_social insta"></span></a>' : '';
                echo ( $luckiesdesign_rss_show && $rss_link ) ? '<a role="link" rel="nofollow"' . $target . ' class="rss-bw" href="' . $rss_link . '" title="' . __('Subscribe via RSS', 'luckiesDesign') . '"><span class="icon_social rss_feed"></span></a>' : '';
            }

            if (!$no_options) {
                ?>
                <p><?php printf(__('Please configure this widget <a href="%s" target="_blank" title="Configure Subscribe Widget">here</a>.', 'luckiesDesign'), admin_url('/widgets.php')); ?></p><?php }
            ?>
        </div> <!-- end email-subscription-container -->
        <?php
        echo $after_widget;
    }

    /**
     * Deals with the settings when they are saved by the admin
     * */
    function update($new_instance, $old_instance) {
        $instance = $old_instance;
        $instance['title'] = strip_tags($new_instance['title']);
        $instance['rss_link'] = esc_url_raw($new_instance['rss_link']);
        $instance['vimeo_link'] = esc_url_raw($new_instance['vimeo_link']);
        $instance['tumbler_link'] = esc_url_raw($new_instance['tumbler_link']);
        $instance['youtube_link'] = esc_url_raw($new_instance['youtube_link']);
        $instance['instagram_link'] = esc_url_raw($new_instance['instagram_link']);
        $instance['twitter_link'] = esc_url_raw($new_instance['twitter_link']);
        $instance['pintrest_link'] = esc_url_raw($new_instance['pintrest_link']);
        $instance['facebook_link'] = esc_url_raw($new_instance['facebook_link']);
        $instance['google_link'] = esc_url_raw($new_instance['google_link']);
        $instance['luckiesdesign_link_target'] = !empty($new_instance['luckiesdesign_link_target']) ? 1 : 0;
        $instance['luckiesdesign_show_rss'] = !empty($new_instance['luckiesdesign_show_rss']) ? 1 : 0;
        $instance['luckiesdesign_show_facebook'] = !empty($new_instance['luckiesdesign_show_facebook']) ? 1 : 0;
        $instance['luckiesdesign_show_pintrest'] = !empty($new_instance['luckiesdesign_show_pintrest']) ? 1 : 0;
        $instance['luckiesdesign_show_youtube'] = !empty($new_instance['luckiesdesign_show_youtube']) ? 1 : 0;
        $instance['luckiesdesign_show_vimeo'] = !empty($new_instance['luckiesdesign_show_vimeo']) ? 1 : 0;
        $instance['luckiesdesign_show_instagram'] = !empty($new_instance['luckiesdesign_show_instagram']) ? 1 : 0;
        $instance['luckiesdesign_show_pintrest'] = !empty($new_instance['luckiesdesign_show_pintrest']) ? 1 : 0;
        $instance['luckiesdesign_show_tumbler'] = !empty($new_instance['luckiesdesign_show_tumbler']) ? 1 : 0;
        $instance['luckiesdesign_show_twitter'] = !empty($new_instance['luckiesdesign_show_twitter']) ? 1 : 0;
        $instance['luckiesdesign_show_google'] = !empty($new_instance['luckiesdesign_show_google']) ? 1 : 0;
        return $instance;
    }

    /**
     * Displays the form on the Widgets page of the WP Admin area
     * */
    function form($instance) {
        $defaults = array('luckiesdesign_show_rss' => '0', 'luckiesdesign_show_tumbler' => '0', 'luckiesdesign_show_youtube' => '0', 'luckiesdesign_show_instagram' => '0', 'luckiesdesign_show_vimeo' => '0', 'luckiesdesign_show_pintrest' => '0', 'luckiesdesign_show_facebook' => '0', 'luckiesdesign_show_twitter' => '0', 'luckiesdesign_show_google' => '0', 'luckiesdesign_link_target' => '1');
        // update instance's default options
        $instance = wp_parse_args((array) $instance, $defaults);

        $title = isset($instance['title']) ? esc_attr(( $instance['title'])) : '';
        $rss_link = isset($instance['rss_link']) ? esc_url($instance['rss_link']) : '';
        $twitter_link = isset($instance['twitter_link']) ? esc_url($instance['twitter_link']) : '';
        $facebook_link = isset($instance['facebook_link']) ? esc_url($instance['facebook_link']) : '';
        $google_link = isset($instance['google_link']) ? esc_url($instance['google_link']) : '';
        $tumbler_link = isset($instance['pintrest_link']) ? esc_url($instance['pintrest_link']) : '';
        $youtube_link = isset($instance['tumbler_link']) ? esc_url($instance['tumbler_link']) : '';
        $pintrest_link = isset($instance['youtube_link']) ? esc_url($instance['youtube_link']) : '';
        $instagram_link = isset($instance['instagram_link']) ? esc_url($instance['instagram_link']) : '';
        $vimeo_link = isset($instance['vimeo_link']) ? esc_url($instance['vimeo_link']) : '';

        $luckiesdesign_show_rss = isset($instance['luckiesdesign_show_rss']) ? (bool) $instance['luckiesdesign_show_rss'] : false;
        $luckiesdesign_show_facebook = isset($instance['luckiesdesign_show_facebook']) ? (bool) $instance['luckiesdesign_show_facebook'] : false;
        $luckiesdesign_show_twitter = isset($instance['luckiesdesign_show_twitter']) ? (bool) $instance['luckiesdesign_show_twitter'] : false;
        $luckiesdesign_show_google = isset($instance['luckiesdesign_show_google']) ? (bool) $instance['luckiesdesign_show_google'] : false;
        $luckiesdesign_show_youtube = isset($instance['luckiesdesign_show_youtube']) ? (bool) $instance['luckiesdesign_show_youtube'] : false;
        $luckiesdesign_show_instagram = isset($instance['luckiesdesign_show_instagram']) ? (bool) $instance['luckiesdesign_show_instagram'] : false;
        $luckiesdesign_show_pintrest = isset($instance['luckiesdesign_show_pintrest']) ? (bool) $instance['luckiesdesign_show_pintrest'] : false;
        $luckiesdesign_show_tumbler = isset($instance['luckiesdesign_show_tumbler']) ? (bool) $instance['luckiesdesign_show_tumbler'] : false;
        $luckiesdesign_show_vimeo = isset($instance['luckiesdesign_show_vimeo']) ? (bool) $instance['luckiesdesign_show_vimeo'] : false;
        $luckiesdesign_link_target = isset($instance['luckiesdesign_link_target']) ? (bool) $instance['luckiesdesign_link_target'] : false;
        ?>

        <p><label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Title', 'luckiesDesign'); ?>: </label><input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" role="textbox" value="<?php echo esc_attr($title); ?>" /></p>
        <p><strong><?php _e('Social Share', 'luckiesDesign'); ?>:</strong></p>
        <p>
            <input role="checkbox" type="checkbox" name="<?php echo $this->get_field_name('luckiesdesign_show_facebook'); ?>" id="<?php echo $this->get_field_id('luckiesdesign_show_facebook'); ?>" <?php checked($luckiesdesign_show_facebook); ?> />
            <label for="<?php echo $this->get_field_id('luckiesdesign_show_facebook'); ?>"><?php _e('Facebook Link', 'luckiesDesign'); ?>: </label>
            <input class="widefat" id="<?php echo $this->get_field_id('facebook_link'); ?>" name="<?php echo $this->get_field_name('facebook_link'); ?>" type="text" role="textbox" value="<?php echo esc_attr($facebook_link); ?>" />
        </p>
        <p>
            <input role="checkbox" type="checkbox" name="<?php echo $this->get_field_name('luckiesdesign_show_twitter'); ?>" id="<?php echo $this->get_field_id('luckiesdesign_show_twitter'); ?>" <?php checked($luckiesdesign_show_twitter); ?> />
            <label for="<?php echo $this->get_field_id('luckiesdesign_show_twitter'); ?>"><?php _e('Twitter Link', 'luckiesDesign'); ?>: </label>
            <input class="widefat" id="<?php echo $this->get_field_id('twitter_link'); ?>" name="<?php echo $this->get_field_name('twitter_link'); ?>" type="text" role="textbox" value="<?php echo esc_attr($twitter_link); ?>" />
        </p>
        <p>
            <input role="checkbox" type="checkbox" name="<?php echo $this->get_field_name('luckiesdesign_show_google'); ?>" id="<?php echo $this->get_field_id('luckiesdesign_show_google'); ?>" <?php checked($luckiesdesign_show_google); ?> />
            <label for="<?php echo $this->get_field_id('luckiesdesign_show_google'); ?>"><?php _e('Google+ Link', 'luckiesDesign'); ?>: </label>
            <input class="widefat" id="<?php echo $this->get_field_id('google_link'); ?>" name="<?php echo $this->get_field_name('google_link'); ?>" type="text" role="textbox" value="<?php echo esc_attr($google_link); ?>" />
        </p>
        <p>
            <input role="checkbox" type="checkbox" name="<?php echo $this->get_field_name('luckiesdesign_show_pintrest'); ?>" id="<?php echo $this->get_field_id('luckiesdesign_show_pintrest'); ?>" <?php checked($luckiesdesign_show_pintrest); ?> />
            <label for="<?php echo $this->get_field_id('luckiesdesign_show_pintrest'); ?>"><?php _e('Pintrest Link', 'luckiesDesign'); ?>: </label>
            <input class="widefat" id="<?php echo $this->get_field_id('pintrest_link'); ?>" name="<?php echo $this->get_field_name('pintrest_link'); ?>" type="text" role="textbox" value="<?php echo esc_attr($pintrest_link); ?>" />
        </p>
        <p>
            <input role="checkbox" type="checkbox" name="<?php echo $this->get_field_name('luckiesdesign_show_tumbler'); ?>" id="<?php echo $this->get_field_id('luckiesdesign_show_tumbler'); ?>" <?php checked($luckiesdesign_show_tumbler); ?> />
            <label for="<?php echo $this->get_field_id('luckiesdesign_show_tumbler'); ?>"><?php _e('Tumbler Link', 'luckiesDesign'); ?>: </label>
            <input class="widefat" id="<?php echo $this->get_field_id('tumbler_link'); ?>" name="<?php echo $this->get_field_name('tumbler_link'); ?>" type="text" role="textbox" value="<?php echo esc_attr($tumbler_link); ?>" />
        </p>
        <p>
            <input role="checkbox" type="checkbox" name="<?php echo $this->get_field_name('luckiesdesign_show_vimeo'); ?>" id="<?php echo $this->get_field_id('luckiesdesign_show_vimeo'); ?>" <?php checked($luckiesdesign_show_vimeo); ?> />
            <label for="<?php echo $this->get_field_id('luckiesdesign_show_vimeo'); ?>"><?php _e('Vimeo Link', 'luckiesDesign'); ?>: </label>
            <input class="widefat" id="<?php echo $this->get_field_id('vimeo_link'); ?>" name="<?php echo $this->get_field_name('vimeo_link'); ?>" type="text" role="textbox" value="<?php echo esc_attr($vimeo_link); ?>" />
        </p>
        <p>
            <input role="checkbox" type="checkbox" name="<?php echo $this->get_field_name('luckiesdesign_show_youtube'); ?>" id="<?php echo $this->get_field_id('luckiesdesign_show_youtube'); ?>" <?php checked($luckiesdesign_show_youtube); ?> />
            <label for="<?php echo $this->get_field_id('luckiesdesign_show_youtube'); ?>"><?php _e('Youtube Link', 'luckiesDesign'); ?>: </label>
            <input class="widefat" id="<?php echo $this->get_field_id('youtube_link'); ?>" name="<?php echo $this->get_field_name('youtube_link'); ?>" type="text" role="textbox" value="<?php echo esc_attr($youtube_link); ?>" />
        </p>
        <p>
            <input role="checkbox" type="checkbox" name="<?php echo $this->get_field_name('luckiesdesign_show_instagram'); ?>" id="<?php echo $this->get_field_id('luckiesdesign_show_instagram'); ?>" <?php checked($luckiesdesign_show_instagram); ?> />
            <label for="<?php echo $this->get_field_id('luckiesdesign_show_instagram'); ?>"><?php _e('Instagram Link', 'luckiesDesign'); ?>: </label>
            <input class="widefat" id="<?php echo $this->get_field_id('instagram_link'); ?>" name="<?php echo $this->get_field_name('instagram_link'); ?>" type="text" role="textbox" value="<?php echo esc_attr($instagram_link); ?>" />
        </p>
        <p>
            <input role="checkbox" type="checkbox" name="<?php echo $this->get_field_name('luckiesdesign_show_rss'); ?>" id="<?php echo $this->get_field_id('luckiesdesign_show_rss'); ?>" <?php checked($luckiesdesign_show_rss); ?> />
            <label for="<?php echo $this->get_field_id('luckiesdesign_show_rss'); ?>"><?php _e('RSS Feed Link', 'luckiesDesign'); ?>: </label>
            <input class="widefat" id="<?php echo $this->get_field_id('rss_link'); ?>" name="<?php echo $this->get_field_name('rss_link'); ?>" type="text" role="textbox" value="<?php echo esc_attr($rss_link); ?>" />
        </p>
        <p>
            <input class="link_target" id="<?php echo $this->get_field_id('luckiesdesign_link_target'); ?>" name="<?php echo $this->get_field_name('luckiesdesign_link_target'); ?>" role="checkbox" role="checkbox" role="checkbox" role="checkbox" type="checkbox" <?php checked($luckiesdesign_link_target); ?> />
            <label for="<?php echo $this->get_field_id('luckiesdesign_link_target'); ?>"><?php _e('Open Social Links in New Tab/Window', 'luckiesDesign'); ?></label>
        </p><?php
    }

}

/**
 * Custom Widget for Footer Menu
 */
class luckiesdesign_nav_menu_widget extends WP_Widget {

    function __construct() {
        $widget_ops = array('description' => __('Add a custom menu to your footer.'));
        parent::__construct('nav_menu', __('luckiesDesign: Footer Menu'), $widget_ops);
    }

    function widget($args, $instance) {
        $title = empty($instance['title']) ? '' : apply_filters('widget_title', $instance['title']);
        // Get menu
        $nav_menu = !empty($instance['nav_menu']) ? wp_get_nav_menu_object($instance['nav_menu']) : false;

        if (!$nav_menu)
            return;

        echo $args['before_widget'];
        ?><h3 class="menu-title"><?php echo $title; ?></h3><?php
        wp_nav_menu(array('container' => 'nav', 'container_class' => 'footer-menu-container', 'menu_class' => 'bullet_list', 'menu' => $nav_menu, 'menu_id' => 'luckiesdesign-nav-menu'));
        ?><?php
        echo $args['after_widget'];
    }

    function update($new_instance, $old_instance) {
        $instance = $old_instance;
        $title = isset($instance['title']) ? esc_attr(( $instance['title'])) : '';
        $instance['nav_menu'] = (int) $new_instance['nav_menu'];
        return $instance;
    }

    function form($instance) {
        $title = isset($instance['title']) ? esc_attr(( $instance['title'])) : '';
        $nav_menu = isset($instance['nav_menu']) ? $instance['nav_menu'] : '';

        // Get menus
        $menus = wp_get_nav_menus(array('orderby' => 'name'));

        // If no menus exists, direct the user to go and create some.
        if (!$menus) {
            echo '<p>' . sprintf(__('No menus have been created yet. <a href="%s">Create some</a>.'), admin_url('nav-menus.php')) . '</p>';
            return;
        }
        ?>
        <p><label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Title', 'luckiesDesign'); ?>: </label><input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" role="textbox" value="<?php echo esc_attr($title); ?>" /></p>
        <p>
            <label for="<?php echo $this->get_field_id('nav_menu'); ?>"><?php _e('Select Menu:'); ?></label>
            <select id="<?php echo $this->get_field_id('nav_menu'); ?>" name="<?php echo $this->get_field_name('nav_menu'); ?>">
                <?php
                foreach ($menus as $menu) {
                    echo '<option value="' . $menu->term_id . '"'
                    . selected($nav_menu, $menu->term_id, false)
                    . '>' . $menu->name . '</option>';
                }
                ?>
            </select>
        </p>
        <?php
    }

}

/**
 * Custom Widget for Footer Menu
 */
class luckiesdesign_archive_widget extends WP_Widget {

    function __construct() {
        $widget_ops = array('description' => __('Add a custom archive dropdown.'));
        parent::__construct('post_archive', __('luckiesDesign: Archive'), $widget_ops);
    }

    function widget($args, $instance) {
        extract($args, EXTR_SKIP);
        $title = empty($instance['title']) ? '' : apply_filters('widget_title', $instance['title']);

        echo $args['before_widget'];
        ?>
        <div class="width100 toggle_win">
            <h1><a href="javascript:void();" class="icon_archive" id="archive"><?php echo $title; ?><span class="fr icon_arrow_up_down"></span></a></h1>
            <div class="width100" id="archive_win">
                <ul class="bullet_list">
                    <?php wp_get_archives(); ?>
                </ul>
            </div>
        </div><!-- end of width100 toggle_win-->
        <div class="clear20"></div>
        <?php
        echo $args['after_widget'];
    }

    function update($new_instance, $old_instance) {
        $instance = $old_instance;
        $instance['title'] = strip_tags($new_instance['title']);
        return $instance;
    }

    function form($instance) {
        $instance = wp_parse_args((array) $instance, $defaults);
        $title = isset($instance['title']) ? esc_attr(( $instance['title'])) : '';
        ?>
        <p><label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Title', 'luckiesDesign'); ?>: </label><input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" role="textbox" value="<?php echo esc_attr($title); ?>" /></p>

        <?php
    }

}

/**
 * Custom Widget for Footer Menu
 */
class luckiesdesign_category_dropdown_widget extends WP_Widget {

    function __construct() {
        $widget_ops = array('description' => __('Add a custom archive dropdown.'));
        parent::__construct('category_archive', __('luckiesDesign: Category Dropdown'), $widget_ops);
    }

    function widget($args, $instance) {
        extract($args, EXTR_SKIP);
        $title = empty($instance['title']) ? '' : apply_filters('widget_title', $instance['title']);

        echo $args['before_widget'];
        ?>
        <div class="width100 toggle_win">
            <h1><a href="javascript:void();" class="icon_categories" id="category"><?php echo $title; ?><span class="fr icon_arrow_up_down"></span></a></h1>
					<div class="width100" id="category_win">
						<ul class="bullet_list">
                     <?php wp_list_categories(array('title_li'=> __(''))); ?> 
                </ul>
            </div>
        </div><!-- end of width100 toggle_win-->
        <div class="clear20"></div>
        <?php
        echo $args['after_widget'];
    }

    function update($new_instance, $old_instance) {
        $instance = $old_instance;
        $instance['title'] = strip_tags($new_instance['title']);
        return $instance;
    }

    function form($instance) {
        $instance = wp_parse_args((array) $instance, $defaults);
        $title = isset($instance['title']) ? esc_attr(( $instance['title'])) : '';
        ?>
        <p><label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Title', 'luckiesDesign'); ?>: </label><input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" role="textbox" value="<?php echo esc_attr($title); ?>" /></p>

        <?php
    }

}

/**
 * Registers all luckiesDesign Custom Widgets
 */
function luckiesdesign_register_widgets() {
    register_widget('luckiesdesign_subscribe_widget');
    register_widget('luckiesdesign_nav_menu_widget');
    register_widget('luckiesdesign_subscribe_widget_footer');
    register_widget('luckiesdesign_archive_widget');
    register_widget('luckiesdesign_category_dropdown_widget');
}

add_action('widgets_init', 'luckiesdesign_register_widgets');
