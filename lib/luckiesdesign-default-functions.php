<?php
/** 
 * luckiesDesign default functions
 *
 * @package luckiesDesign
 */

/**
 * Checks whether the post meta div needs to be displayed or not
 *
 * @uses $luckiesdesign_post_comments Post Comments DB array
 * @uses $post Post Data
 * @param string $position Specify the position of the post meta (u/l)
 */
function luckiesdesign_has_postmeta( $position = 'u' ) {
    global $post, $luckiesdesign_post_comments;
    $can_edit = ( get_edit_post_link() ) ? 1 : 0;
    $flag = 0;
    // Show Author?
    if ( $luckiesdesign_post_comments['post_author_'.$position] ) {
        $flag++;
    }
    // Show Date?
    elseif ( $luckiesdesign_post_comments['post_date_'.$position] )  {
        $flag++;
    }
     // Show Category?
    elseif ( get_the_category_list() && $luckiesdesign_post_comments['post_category_'.$position] ) {
        $flag++;
    }
    // Show Tags?
    elseif ( get_the_tag_list() && $luckiesdesign_post_comments['post_tags_'.$position] ) {
        $flag++;
    } 
    // Checked if logged in and post meta top
    else if ( $can_edit && $position == 'u' ) {
        $flag++;
    } 
    elseif ( ( has_action( 'luckiesdesign_hook_begin_post_meta_top' ) || ( has_action( 'luckiesdesign_hook_end_post_meta_top' ) && $can_edit ) ) && $position == 'u' ) {
        $flag++;
    }
    elseif ( ( has_action( 'luckiesdesign_hook_begin_post_meta_bottom' ) || has_action( 'luckiesdesign_hook_end_post_meta_bottom' ) ) && $position == 'l' ) {
        $flag++;
    }
    else {
        // Show Custom Taxonomies?
        $args = array( '_builtin' => false );
        $taxonomies = get_taxonomies( $args, 'names' );
        foreach ( $taxonomies as $taxonomy ) {
            if ( get_the_terms( $post->ID, $taxonomy ) && isset( $luckiesdesign_post_comments['post_'.$taxonomy.'_'.$position] ) && $luckiesdesign_post_comments['post_'.$taxonomy.'_'.$position] ) {
                $flag++;
            }
        }
    }
    
    return $flag;
}

/**
 * Default post meta
 *
 * @uses $luckiesdesign_post_comments Post Comments DB array
 * @uses $post Post Data
 * @param string $placement Specify the position of the post meta (top/bottom)
 */
function luckiesdesign_default_post_meta( $placement = 'top' ) { 
    
    if ( 'post' == get_post_type() && !luckiesdesign_is_bbPress() ) {
        global $post, $luckiesdesign_post_comments;
        $position = ( 'bottom' == $placement ) ? 'l' : 'u'; // l = Lower/Bottom , u = Upper/Top

        if ( luckiesdesign_has_postmeta( $position ) ) {
            if ( $position == 'l' ) { echo '<footer class="post-footer">'; } ?>
            <div class="clearfix post-meta post-meta-<?php echo $placement; ?>"><?php   

                if( 'bottom' == $placement )
                    luckiesdesign_hook_begin_post_meta_bottom();
                else
                    luckiesdesign_hook_begin_post_meta_top();

                // Author Link
                if ( $luckiesdesign_post_comments['post_author_'.$position] || $luckiesdesign_post_comments['post_date_'.$position] ) { ?>
                    <p class="post-publish alignleft"><?php
                        if ( $luckiesdesign_post_comments['post_author_'.$position] ) {
                            printf( __( 'Posted by <span class="author vcard">%s</span>', 'luckiesDesign' ), ( !$luckiesdesign_post_comments['author_link_'.$position] ? get_the_author() . ( $luckiesdesign_post_comments['author_count_'.$position] ? '(' . get_the_author_posts() . ')' : '' ) : sprintf( __( '<a class="fn" href="%1$s" title="%2$s">%3$s</a>', 'luckiesDesign' ), get_author_posts_url( get_the_author_meta( 'ID' ), get_the_author_meta( 'user_nicename' ) ), esc_attr( sprintf( __( 'Posts by %s', 'luckiesDesign' ), get_the_author() ) ), get_the_author() ) . ( $luckiesdesign_post_comments['author_count_'.$position] ? '(' . get_the_author_posts() . ')' : '' ) ) );
                        }
                        echo ( $luckiesdesign_post_comments['post_author_'.$position] && $luckiesdesign_post_comments['post_date_'.$position] ) ? ' ' : '';
                        if ( $luckiesdesign_post_comments['post_date_'.$position] ) {
                            printf( __( 'on <time class="published" datetime="%s">%s</time>', 'luckiesDesign' ), get_the_date('c'), get_the_time( $luckiesdesign_post_comments['post_date_format_'.$position] ) );
                        } ?>
                    </p><?php
                }

                // Post Categories
                echo ( get_the_category_list() && $luckiesdesign_post_comments['post_category_'.$position] ) ? '<p class="post-category alignleft">&nbsp;' . __( 'in', 'luckiesDesign' ) . ' <span>' . get_the_category_list( ', ' ) . '</span></p>' : '';

                // Post Tags
                echo ( get_the_tag_list() && $luckiesdesign_post_comments['post_tags_'.$position] ) ? '<p class="post-tags alignleft">' . get_the_tag_list( __( 'Tagged', 'luckiesDesign' ) . ': <span>', ', ', '</span>' ) . '</p>' : '';

                // Post Custom Taxonomies
                $args = array( '_builtin' => false );
                $taxonomies = get_taxonomies( $args, 'objects' );
                foreach ( $taxonomies as $key => $taxonomy ) {
                    ( get_the_terms( $post->ID, $key ) && isset( $luckiesdesign_post_comments['post_'.$key.'_'.$position] ) && $luckiesdesign_post_comments['post_'.$key.'_'.$position] ) ? the_terms( $post->ID, $key, '<p class="post-custom-tax post-' . $key . '">' . $taxonomy->labels->singular_name . ': ', ', ', '</p>' ) : '';
                }

                if ( 'bottom' == $placement )
                    luckiesdesign_hook_end_post_meta_bottom();
                else
                    luckiesdesign_hook_end_post_meta_top(); ?>

            </div><!-- .post-meta --><?php
            if ( $position == 'l' ) { echo '</footer>'; }
        }
    } elseif ( !luckiesdesign_is_bbPress() ) {
        if ( get_edit_post_link() && ( 'top' == $placement ) ) { ?>
            <div class="post-meta post-meta-top"><?php luckiesdesign_hook_end_post_meta_top(); ?></div><?php
        }
    }
 }
add_action('luckiesdesign_hook_post_meta_top','luckiesdesign_default_post_meta'); // Post Meta Top
add_action('luckiesdesign_hook_post_meta_bottom','luckiesdesign_default_post_meta'); // Post Meta Bottom

/**
 * Default Navigation Menu
 */
function luckiesdesign_default_nav_menu() {
     echo '<nav id="luckiesdesign-primary-menu" role="navigation" class="span5 luckiesdesign-grid' . apply_filters( 'luckiesdesign_mobile_nav_support', ' luckiesdesign-mobile-nav' ) . '">';
        /* Call wp_nav_menu() for Wordpress Navigaton with fallback wp_list_pages() if menu not set in admin panel */
        if ( function_exists( 'wp_nav_menu' ) && has_nav_menu( 'primary' ) ) {
            wp_nav_menu( array( 'container' => '', 'menu_class' => 'luckiesdesign-nav-menu', 'theme_location' => 'primary', 'depth' => apply_filters( 'luckiesdesign_nav_menu_depth', 4 ) ) );
            if (is_user_logged_in()) { ?>
                                        <li><a href="<?php echo get_permalink(get_option('woocommerce_myaccount_page_id')); ?>" title="<?php _e('My Account', 'woothemes'); ?>"><?php _e('My Account', 'woothemes'); ?></a></li>
                                            <?php } else {
                                                ?>
                                                <li><a class="icon_register" href="<?php echo get_permalink(get_option('woocommerce_myaccount_page_id')); ?>" title="<?php _e('Login / Register', 'woothemes'); ?>">Register</a></li>
                                                <li><a class="icon_login_pg" href="<?php echo get_permalink(get_option('woocommerce_myaccount_page_id')); ?>" title="<?php _e('Login / Register', 'woothemes'); ?>">Login</a></li>
                                            <?php } 
        } else {
            echo '<ul class="menu" id="luckiesdesign-nav-menu">';
                wp_list_pages( array( 'title_li' => '', 'sort_column' => 'menu_order', 'number' => '5', 'depth' => apply_filters( 'luckiesdesign_nav_menu_depth', 4 ) ) );
            echo '</ul>';
        }
    echo '</nav>';
}
add_action('luckiesdesign_hook_before_logo','luckiesdesign_default_nav_menu'); // Adds default nav menu after #header

/**
 * 'Edit' link for post/page
 */
function luckiesdesign_edit_link() {
    // Call Edit Link
    edit_post_link( __( '[ edit ]', 'luckiesDesign' ), '<p class="luckiesdesign-edit-link alignleft">', '&nbsp;</p>');
}
add_action('luckiesdesign_hook_begin_post_meta_top', 'luckiesdesign_edit_link');

/**
 * Adds breadcrumb support to the theme.
 */
function luckiesdesign_breadcrumb_support( $text ) { 
   // Breadcrumb Support
    if ( function_exists( 'bcn_display' ) ) {
        echo '<div class="breadcrumb">';
            bcn_display();
        echo '</div>';
    }
}
add_action( 'get_breadcrumb', 'luckiesdesign_breadcrumb_support' );

/**
 * Adds Site Description
 */
function luckiesdesign_blog_description(){
    if ( get_bloginfo( 'description' ) ) { ?>
        <h2 class="tagline"><?php bloginfo( 'description' ); ?></h2><?php
    }
}
add_action( 'luckiesdesign_hook_after_logo', 'luckiesdesign_blog_description' );

/**
 * Adds pagination to single
 */
function luckiesdesign_default_single_pagination() {
    if ( is_single() && ( get_adjacent_post( '', '', true ) || get_adjacent_post( '', '', false ) ) ){ ?>
        <div class="luckiesdesign-navigation clearfix">
            <?php if ( get_adjacent_post( '', '', true ) ) { ?><div class="alignleft"><?php previous_post_link( '%link', __( '&larr; %title', 'luckiesDesign' ) ); ?></div><?php } ?>
            <?php if ( get_adjacent_post( '', '', false ) ) { ?><div class="alignright"><?php next_post_link( '%link', __( '%title &rarr;', 'luckiesDesign' ) ); ?></div><?php } ?>
        </div><!-- .luckiesdesign-navigation --><?php
    }
}
add_action( 'luckiesdesign_hook_single_pagination', 'luckiesdesign_default_single_pagination' );

/**
 * Adds pagination to archives
 */
function luckiesdesign_default_blog_pagination() { 
    
    /* Page-Navi Plugin Support with WordPress Default Pagination */

            global $post_query, $luckiesdesign_post_comments;
                if ( ( $post_query->max_num_pages > 1 ) ) { ?>
                    <nav class="wp-pagenavi"><?php 
                        echo paginate_links( array(
                                'base' => str_replace( 999999999, '%#%', esc_url( get_pagenum_link( 999999999 ) ) ),
                                'format' => '?paged=%#%',
                                'current' => max( 1, get_query_var('paged') ),
                                'total' => $post_query->max_num_pages,
                                'prev_text' => esc_attr( $luckiesdesign_post_comments['prev_text'] ),
                                'next_text' => esc_attr( $luckiesdesign_post_comments['next_text'] ),
                                'end_size' => $luckiesdesign_post_comments['end_size'],
                                'mid_size' => $luckiesdesign_post_comments['mid_size']
                            ) ); ?>
                    </nav><?php
                }
}
add_action( 'luckiesdesign_hook_blog_pagination', 'luckiesdesign_default_blog_pagination' );

/**
 * Adds pagination to archives
 */
function luckiesdesign_default_archive_pagination() { 
    
    /* Page-Navi Plugin Support with WordPress Default Pagination */

            global $wp_query, $luckiesdesign_post_comments;
            if ( isset( $luckiesdesign_post_comments['pagination_show'] ) && $luckiesdesign_post_comments['pagination_show'] ) {
                if ( ( $wp_query->max_num_pages > 1 ) ) { ?>
                    <nav class="wp-pagenavi"><?php 
                        echo paginate_links( array(
                                'base' => str_replace( 999999999, '%#%', esc_url( get_pagenum_link( 999999999 ) ) ),
                                'format' => '?paged=%#%',
                                'current' => max( 1, get_query_var('paged') ),
                                'total' => $wp_query->max_num_pages,
                                'prev_text' => esc_attr( $luckiesdesign_post_comments['prev_text'] ),
                                'next_text' => esc_attr( $luckiesdesign_post_comments['next_text'] ),
                                'end_size' => $luckiesdesign_post_comments['end_size'],
                                'mid_size' => $luckiesdesign_post_comments['mid_size']
                            ) ); ?>
                    </nav><?php
                }
            } elseif ( function_exists( 'wp_pagenavi' ) ) {
                wp_pagenavi();
            } elseif ( get_next_posts_link() || get_previous_posts_link() ) { ?>
                <nav class="luckiesdesign-navigation clearfix">
                    <?php if ( get_next_posts_link() ) { ?><div class="alignleft"><?php next_posts_link( __( '&larr; Older Entries', 'luckiesDesign' ) ); ?></div><?php } ?>
                    <?php if ( get_previous_posts_link() ) { ?><div class="alignright"><?php previous_posts_link( __( 'Newer Entries &rarr;', 'luckiesDesign' ) ); ?></div><?php } ?>
                </nav><!-- .luckiesdesign-navigation --><?php
            }
}
add_action( 'luckiesdesign_hook_archive_pagination', 'luckiesdesign_default_archive_pagination' );

/**
 * Displays the sidebar.
 */
function luckiesdesign_default_sidebar() {
    get_sidebar();
}
add_action( 'luckiesdesign_hook_sidebar', 'luckiesdesign_default_sidebar' );

/**
 * Displays the comments and comment form.
 */
function luckiesdesign_default_comments() {
    if ( is_singular() ) {
        comments_template( '', true );
    }
}
add_action( 'luckiesdesign_hook_comments', 'luckiesdesign_default_comments' );

/**
 * Outputs the comment count linked to the comments of the particular post/page
 */
function luckiesdesign_default_comment_count() {
    global $luckiesdesign_post_comments;
    // Comment Count
    add_filter( 'get_comments_number', 'luckiesdesign_only_comment_count', 11, 2 );
    if ( ( ( get_comments_number() || @comments_open() ) && !is_attachment() && !luckiesdesign_is_bbPress() ) || ( is_attachment() && $luckiesdesign_post_comments['attachment_comments'] ) ) { // If post meta is set to top then only display the comment count. ?>
        <p class="alignright luckiesdesign-post-comment-count"><span class="luckiesdesign-curly-bracket">{</span><?php comments_popup_link( _x( '<span>0</span> Comments', 'comments number', 'luckiesDesign' ), _x( '<span>1</span> Comment', 'comments number', 'luckiesDesign' ), _x( '<span>%</span> Comments', 'comments number', 'luckiesDesign' ), 'luckiesdesign-post-comment luckiesdesign-common-link' ); ?><span class="luckiesdesign-curly-bracket">}</span></p><?php
    }
    remove_filter( 'get_comments_number', 'luckiesdesign_only_comment_count', 11, 2 );
}
add_action( 'luckiesdesign_hook_end_post_title', 'luckiesdesign_default_comment_count' );

/**
 * Get the sidebar ID for current page.
 */
function luckiesdesign_get_sidebar_id() {
    global $luckiesdesign_general;
    $sidebar_id = "sidebar-widgets";
    
    if ( function_exists('bp_current_component') && bp_current_component() ) {
        
        if ( $luckiesdesign_general['buddypress_sidebar'] === "buddypress-sidebar" ) {
            $sidebar_id = "buddypress-sidebar-widgets";
        } else if ( $luckiesdesign_general['buddypress_sidebar'] === "no-sidebar" ) {
            $sidebar_id = 0;
        }
        
    } else if ( function_exists('is_bbpress') && is_bbpress() ) {
        
        if ( $luckiesdesign_general['bbpress_sidebar'] === "bbpress-sidebar" ) {
            $sidebar_id = "bbpress-sidebar-widgets";
        } else if ( $luckiesdesign_general['bbpress_sidebar'] === "no-sidebar" ) {
            $sidebar_id = 0;
        }
        
    }
    
    return $sidebar_id;
}

/**
 * Adds custom css through theme options
 */
function luckiesdesign_custom_css(){
    global $luckiesdesign_general;
    echo ( $luckiesdesign_general['custom_styles'] ) ? '<style>' . $luckiesdesign_general['custom_styles'] . '</style>' . "\r\n" : '';
}
add_action( 'luckiesdesign_head', 'luckiesdesign_custom_css' );
