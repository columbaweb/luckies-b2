<?php
/**
 * luckiesDesign sidebars
 *
 * @package luckiesDesign
 */

/**
 * Registers sidebars
 */
function luckiesdesign_widgets_init() {
    global $luckiesdesign_general;

    // Header Widget
    register_sidebar( array(
        'name' => __( 'Header Social Icons', 'luckiesDesign' ),
        'id' => 'header_widgetized_area',
        'before_widget' => '<div id="%1$s" class="widget header-widget %2$s">',
        'after_widget' => '</div>',
        'before_title' => '<h3 class="widgettitle">',
        'after_title' => '</h3>',
    ) );

    // Tweeter Widget
    register_sidebar( array(
        'name' => __( 'Home Page Tweete..', 'luckiesDesign' ),
        'id' => 'tweeter-sidebar',
        'before_widget' => '<div id="%1$s" class="widget header-widget %2$s">',
        'after_widget' => '</div>',
        'before_title' => '<h3 class="widgettitle">',
        'after_title' => '</h3>',
    ) );
    
    // Tweeter Widget
    register_sidebar( array(
        'name' => __( 'Home Page Latest/Popular Post', 'luckiesDesign' ),
        'id' => 'home-page-post-tabs',
        'before_widget' => '<div id="%1$s" class="widget header-widget %2$s">',
        'after_widget' => '</div>',
        'before_title' => '<h3 class="widgettitle">',
        'after_title' => '</h3>',
    ) );
    
    // Sidebar Widget
    register_sidebar( array(
        'name' => __( 'Shop Pages Sidebar Widgets', 'luckiesDesign' ),
        'id' => 'shop-sidebar',
        'before_widget' => '<div id="%1$s" class="widget sidebar-widget %2$s">',
        'after_widget' => '</div>',
        'before_title' => '<h4 class="widgettitle">',
        'after_title' => '</h4>',
    ) );

    // Sidebar Widget
    register_sidebar( array(
        'name' => __( 'Single Post Page Sidebar Widgets', 'luckiesDesign' ),
        'id' => 'single-post-sidebar',
        'before_widget' => '<div id="%1$s" class="widget sidebar-widget %2$s">',
        'after_widget' => '</div>',
        'before_title' => '<h4 class="widgettitle">',
        'after_title' => '</h4>',
    ) );
    
    
    // Sidebar Widget
    register_sidebar( array(
        'name' => __( 'Footer Column 1', 'luckiesDesign' ),
        'id' => 'footer-column1',
        'before_widget' => '<div id="%1$s" class="widget sidebar-widget %2$s">',
        'after_widget' => '</div>',
        'before_title' => '<h3 class="widgettitle">',
        'after_title' => '</h3>',
    ) );
    
    // Sidebar Widget
    register_sidebar( array(
        'name' => __( 'Footer Column 2', 'luckiesDesign' ),
        'id' => 'footer-column2',
        'before_widget' => '<div id="%1$s" class="widget sidebar-widget %2$s">',
        'after_widget' => '</div>',
        'before_title' => '<h3 class="widgettitle">',
        'after_title' => '</h3>',
    ) );
    
    // Sidebar Widget
    register_sidebar( array(
        'name' => __( 'Footer Column 3', 'luckiesDesign' ),
        'id' => 'footer-column3',
        'before_widget' => '<div id="%1$s" class="widget sidebar-widget %2$s">',
        'after_widget' => '</div>',
        'before_title' => '<h3 class="widgettitle">',
        'after_title' => '</h3>',
    ) );
    
}
add_action( 'widgets_init', 'luckiesdesign_widgets_init' );