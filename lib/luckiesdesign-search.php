<?php
/**
 * Over-riding WordPress default search mechanism
 *
 * Support for Google Custom Search ( Bonus !!! )
 * 
 * @package luckiesDesign
 */

/**
 * luckiesDesign Custom Search Form
 *
 * @param string $form
 * @return string
 */
function luckiesdesign_custom_search_form( $form ) {
    global $luckiesdesign_general, $is_chrome;
    $search_class = 'txt_search';
    if ( preg_match( '/customSearchControl.draw\(\'cse\'(.*)\)\;/i', @$luckiesdesign_general["search_code"] ) ) {
        $search_class .= ' luckiesdesign-google-search';
        $placeholder = NULL;
    } else {
        $placeholder = 'placeholder="' . apply_filters( 'luckiesdesign_search_placeholder', __( 'SEARCH ENTIRE STORE HERE', 'luckiesDesign' ) ) . '" ';
    }
    $chrome_voice_search = ( $is_chrome ) ? ' x-webkit-speech="x-webkit-speech" speech="speech" onwebkitspeechchange="this.form.submit();"' : '';
    $form = '<form role="search" class="searchform" action="' . home_url( '/' ) . '">
                <div><label class="hidden">' . __( 'Search for:', 'luckiesDesign' ) . '</label>
                    <input type="search" required="required" ' . $placeholder . 'value="' . esc_attr( apply_filters( 'the_search_query', get_search_query() ) ).'" name="s" class="' . $search_class . '" title="' . apply_filters( 'luckiesdesign_search_placeholder', __( 'SEARCH ENTIRE STORE HERE', 'luckiesDesign' ) ). '"' . $chrome_voice_search . ' />
                    <input type="submit" class="btn_search" value="' . esc_attr( __( ' ', 'luckiesDesign' ) ) . '" title="Search" />
                </div>
             </form>';
    return $form;
}
add_filter( 'get_search_form', 'luckiesdesign_custom_search_form' );


/* Customizing URLs, when using Google Custom Search */
if( is_search() ) {
    $result_url = get_site_url( '', '?s=' );
        header( 'Location:' . $result_url . $s );
    exit;
}