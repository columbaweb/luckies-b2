<?php
/**
 * luckiesDesign support for Facebook Open Graph Protocol
 *
 * @package luckiesDesign
 */

/**
 * Facebook Open Graph Protocol
 */
class luckiesdesign_ogp {
    
    var $data;

    /**
     * Constructor
     *
     * @return void
     **/
    function luckiesdesign_ogp() {
        add_action( 'wp_head', array( $this, 'luckiesdesign_ogp_add_head' ) );
    }

    /**
     * Outputs Open Graph meta tags
     **/
    function luckiesdesign_ogp_add_head() {
        $this->data = $this->luckiesdesign_ogp_set_data();
        echo $this->luckiesdesign_ogp_get_headers( $this->data );
    }

    /**
     * Sets Open Graph meta tags
     *
     * @return array
     **/
    function luckiesdesign_ogp_set_data() {
        global $post, $luckiesdesign_general;
        $data = array();
        
        if ( !empty( $luckiesdesign_general['fb_app_id'] ) )
            $data['fb:app_id'] = $luckiesdesign_general['fb_app_id'];

        if ( !empty( $luckiesdesign_general['fb_admins'] ) )
            $data['fb:admins'] = $luckiesdesign_general['fb_admins'];

        $data['og:site_name'] = get_bloginfo('name');

        if ( is_singular () && !is_front_page() ) {
            $append = '';
            $post_content = ( isset( $post->post_excerpt ) && trim( $post->post_excerpt ) ) ? $post->post_excerpt : $post->post_content;
            if( strlen( wp_html_excerpt( $post_content, 130 ) ) >= 130 )
                $append = '...';
            
            $data['og:title'] = esc_attr( $post->post_title );
            $data['og:type'] = 'article';
            $data['og:image'] = $this->luckiesdesign_ogp_image_url();
            $data['og:url'] = get_permalink();
            $data['og:description'] = esc_attr( wp_html_excerpt( $post_content, 130 ).$append );
        } else {
            $data['og:title'] = get_bloginfo('name');
            $data['og:type'] = 'website';
            $data['og:image'] = $this->luckiesdesign_ogp_image_url();
            $data['og:url'] = home_url( $_SERVER['REQUEST_URI'] );
            $data['og:description'] = get_bloginfo('description');
        }
        return $data;
    }

    /**
     * Returns Formatted Open Graph meta tags
     *
     * @return array
     **/
    function luckiesdesign_ogp_get_headers($data) {
        if ( !count( $data ) ) {
            return;
        }
        $out = array();
        $out[] = "\n<!-- BEGIN: Open Graph Protocol : http://opengraphprotocol.org/ for more info -->";
        foreach ($data as $property => $content) {
            if ($content != '') {
                $out[] = "<meta property=\"{$property}\" content=\"" . apply_filters( 'luckiesdesign_ogp_content_' . $property, $content ) . "\" />";
            } else {
                $out[] = "<!--{$property} value was blank-->";
            }
        }
        $out[] = "<!-- End: Open Graph Protocol -->\n";
        return implode("\n", $out);
    }

    /**
     * Returns Open Graph image meta tag value
     *
     * @return string
     **/
    function luckiesdesign_ogp_image_url() {
        global $post, $luckiesdesign_general;
        $image = '';
        if ( is_singular() && !is_front_page() ) {
            if (has_post_thumbnail($post->ID)) {
                $thumbnail = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'post-thumbnail' );
                if ( !empty ( $thumbnail ) ) {
                    $image = $thumbnail[0];
                }
            } else {                
                $image = apply_filters( 'luckiesdesign_default_ogp_image_path', '' );                
                if(empty($image)){
                    $image = $luckiesdesign_general['logo_upload'];
                }
            }
        } else {
            $image = $luckiesdesign_general['logo_upload'];
        }        
        return $image;
    }
}

global $luckiesdesign_general;
if( !empty( $luckiesdesign_general['fb_app_id'] ) || !empty( $luckiesdesign_general['fb_admins'] ) ) {
// Facebook Open Graph Protocol
    $luckiesdesign_ogp = new luckiesdesign_ogp();
}
?>