<?php
/**
 * Custom Post Types
 */

/** 
 * Registers FAQ's
 */
function create_frequently_asked_questions() {
   /* Custom Posts */
    register_post_type( 'faqs', array(
        'labels' => array(
        'name' => _x('FAQ\'s', 'post type general name', 'luckiesDesign'),
        'singular_name' => _x('FAQ\'s', 'post type singular name', 'luckiesDesign'),
        'add_new' => _x('Add New FAQ', 'custom post', 'luckiesDesign'),
        'add_new_item' => __('Add New FAQ', 'luckiesDesign'),
        'edit_item' => __('Edit FAQ', 'luckiesDesign'),
        'new_item' => __('New FAQ', 'luckiesDesign'),
        'view_item' => __('View FAQ', 'luckiesDesign'),
        'search_items' => __('Search FAQ', 'luckiesDesign'),
        'not_found' => __('No FAQ\'s found.', 'luckiesDesign'),
        'not_found_in_trash' => __('No FAQ\'s found in Trash.', 'luckiesDesign'),
        'parent_item_colon' => array( null, __('FAQ\'s:', 'luckiesDesign') ),
        'all_items' => __( 'All FAQ\'s', 'luckiesDesign' ) ),
        'description' => __( 'FAQ\'s', 'luckiesDesign' ),
        'publicly_queryable' => null, 
        'exclude_from_search' => null,
        'capability_type' => 'post', 
        'capabilities' => array(), 
        'map_meta_cap' => null,
        '_builtin' => false, 
        '_edit_link' => 'post.php?post=%d', 
        'hierarchical' => false,
        'public' => true, 
        'rewrite' => true,
        'has_archive' => true, 
        'query_var' => true,
        'supports' => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt', 'comments' ),
        'register_meta_box_cb' => null,
        'taxonomies' => array(), 
        'show_ui' => null, 
        'menu_position' => null, 
        'menu_icon' => null,
        'permalink_epmask' => EP_PERMALINK, 
        'can_export' => true,
        'show_in_nav_menus' => null, 
        'show_in_menu' => null, 
        'show_in_admin_bar' => null )
    );
}

add_action( 'init', 'create_frequently_asked_questions' );

/** 
 * Registers T&C
 */
function create_terms_condition() {
   /* Custom Posts */
    register_post_type( 'terms-conditions', array(
        'labels' => array(
        'name' => _x('Terms & Conditions', 'post type general name', 'luckiesDesign'),
        'singular_name' => _x('Terms & Conditions', 'post type singular name', 'luckiesDesign'),
        'add_new' => _x('Add New Terms & Conditions', 'custom post', 'luckiesDesign'),
        'add_new_item' => __('Add New Terms & Conditions', 'luckiesDesign'),
        'edit_item' => __('Edit Terms & Conditions', 'luckiesDesign'),
        'new_item' => __('New Terms & Conditions', 'luckiesDesign'),
        'view_item' => __('View Terms & Conditions', 'luckiesDesign'),
        'search_items' => __('Search Terms & Conditions', 'luckiesDesign'),
        'not_found' => __('No Terms & Conditions found.', 'luckiesDesign'),
        'not_found_in_trash' => __('No Terms & Conditions found in Trash.', 'luckiesDesign'),
        'parent_item_colon' => array( null, __('Terms & Conditions:', 'luckiesDesign') ),
        'all_items' => __( 'All Terms & Conditions', 'luckiesDesign' ) ),
        'description' => __( 'Terms & Conditions', 'luckiesDesign' ),
        'publicly_queryable' => null, 
        'exclude_from_search' => null,
        'capability_type' => 'post', 
        'capabilities' => array(), 
        'map_meta_cap' => null,
        '_builtin' => false, 
        '_edit_link' => 'post.php?post=%d', 
        'hierarchical' => false,
        'public' => true, 
        'rewrite' => true,
        'has_archive' => true, 
        'query_var' => true,
        'supports' => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt', 'comments' ),
        'register_meta_box_cb' => null,
        'taxonomies' => array(), 
        'show_ui' => null, 
        'menu_position' => null, 
        'menu_icon' => null,
        'permalink_epmask' => EP_PERMALINK, 
        'can_export' => true,
        'show_in_nav_menus' => null, 
        'show_in_menu' => null, 
        'show_in_admin_bar' => null )
    );
}

add_action( 'init', 'create_terms_condition' );
