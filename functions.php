<?php

/**
 * luckiesDesign functions and definitions
 */
define('LUCKIESDESIGN_VERSION', '1.0');

/* Define Directory Constants */
define('LUCKIESDESIGN_ADMIN', get_template_directory() . '/admin');
define('LUCKIESDESIGN_CSS', get_template_directory() . '/css');
define('LUCKIESDESIGN_JS', get_template_directory() . '/js');
define('LUCKIESDESIGN_IMG', get_template_directory() . '/images');

/* Define Directory URL Constants */
define('LUCKIESDESIGN_TEMPLATE_URL', get_template_directory_uri());
define('LUCKIESDESIGN_CSS_FOLDER_URL', get_template_directory_uri() . '/css');
define('LUCKIESDESIGN_JS_FOLDER_URL', get_template_directory_uri() . '/js');
define('LUCKIESDESIGN_IMG_FOLDER_URL', get_template_directory_uri() . '/images');

$luckiesdesign_general = get_option('luckiesdesign_general'); // luckiesDesign General Options
$luckiesdesign_post_comments = get_option('luckiesdesign_post_comments'); // luckiesDesign Post & Comments Options
$luckiesdesign_version = get_option('luckiesdesign_version'); // luckiesDesign Version

/* Check if default values are present in the database else force defaults - Since luckiesDesign v2.1 */
$luckiesdesign_general['pagination_show'] = isset($luckiesdesign_general['pagination_show']) ? $luckiesdesign_general['pagination_show'] : 0;
$luckiesdesign_post_comments['prev_text'] = isset($luckiesdesign_post_comments['prev_text']) ? $luckiesdesign_post_comments['prev_text'] : __('&laquo; Previous', 'luckiesDesign');
$luckiesdesign_post_comments['next_text'] = isset($luckiesdesign_post_comments['next_text']) ? $luckiesdesign_post_comments['next_text'] : __('Next &raquo;', 'luckiesDesign');
$luckiesdesign_post_comments['end_size'] = isset($luckiesdesign_post_comments['end_size']) ? $luckiesdesign_post_comments['end_size'] : 1;
$luckiesdesign_post_comments['mid_size'] = isset($luckiesdesign_post_comments['mid_size']) ? $luckiesdesign_post_comments['mid_size'] : 2;
$luckiesdesign_post_comments['attachment_comments'] = isset($luckiesdesign_post_comments['attachment_comments']) ? $luckiesdesign_post_comments['attachment_comments'] : 0;

/* Includes PHP files located in 'lib' folder */
foreach (glob(get_template_directory() . "/lib/*.php") as $lib_filename) {
    require_once( $lib_filename );
}

/* Includes luckiesDesign Theme Options */
require_once( get_template_directory() . "/admin/luckiesdesign-theme-options.php" );


/*
 * Enqueue Custom Javascripts and CSS files 
 */
add_action('wp_enqueue_scripts', 'luckiesdesign_custom_scripts');

function luckiesdesign_custom_scripts() {
    
//    scripts
    wp_register_script('bootstrap-min', LUCKIESDESIGN_JS_FOLDER_URL . '/bootstrap.min.js');
    wp_enqueue_script('bootstrap-min');

    wp_register_script('modernizr', LUCKIESDESIGN_JS_FOLDER_URL . '/modernizr.js');
    wp_enqueue_script('modernizr');

    wp_register_script('jquery.easing', LUCKIESDESIGN_JS_FOLDER_URL . '/jquery.easing.js');
    wp_enqueue_script('jquery.easing');

    wp_register_script('jquery.mousewheel', LUCKIESDESIGN_JS_FOLDER_URL . '/jquery.mousewheel.js');
    wp_enqueue_script('jquery.mousewheel');

    wp_register_script('jquery.flexslider-min', LUCKIESDESIGN_JS_FOLDER_URL . '/jquery.flexslider-min.js');
    wp_enqueue_script('jquery.flexslider-min');

    wp_register_script('accordian', LUCKIESDESIGN_JS_FOLDER_URL . '/accordian.js');
    wp_enqueue_script('accordian');

    wp_register_script('email-a-friend', LUCKIESDESIGN_JS_FOLDER_URL . '/email-a-friend.js');
    wp_enqueue_script('email-a-friend');

//    styles
    wp_register_style('flexslider', LUCKIESDESIGN_CSS_FOLDER_URL . '/flexslider.css');
    wp_enqueue_style('flexslider');

}

add_filter('woocommerce_enqueue_styles', '__return_false');

//// Hook in
//add_filter('woocommerce_checkout_fields', 'custom_override_checkout_fields');
//
//// Our hooked in function - $fields is passed via the filter!
//function custom_override_checkout_fields($fields) {
//    $fields['billing']['billing_first_name'] = array(
//        'label' => __('Full Name', 'woocommerce'),
//        'required' => true,
//        'class' => array('txt_checkout'),
//        'label_class' => array('text_right'),
//        'clear' => true,
//        'placeholder' => _x('', 'placeholder')
//    );
//    $fields['billing']['billing_country'] = array(
//        'type' => 'select',
//        'required' => true,
//        'class' => array('sel_checkout'),
//        'label_class' => array('text_right'),
//        'clear' => true,
//        'placeholder' => _x('', 'placeholder')
//    );
//    $fields['billing']['billing_postcode'] = array(
//        'label' => __('PostCode', 'woocommerce'),
//        'required' => true,
//        'class' => array('txt_postcode'),
//        'label_class' => array('text_right'),
//        'clear' => true,
//        'placeholder' => _x('', 'placeholder')
//    );
//    $fields['billing']['billing_company'] = array(
//        'label' => __('Company Name', 'woocommerce'),
//        'required' => true,
//        'class' => array('txt_checkout'),
//        'label_class' => array('text_right'),
//        'clear' => true,
//        'placeholder' => _x('', 'placeholder')
//    );
//    $fields['billing']['billing_address_1'] = array(
//        'label' => __('House Name/Number', 'woocommerce'),
//        'required' => true,
//        'class' => array('txt_checkout'),
//        'label_class' => array('text_right'),
//        'clear' => true,
//        'placeholder' => _x('', 'placeholder')
//    );
//    $fields['billing']['billing_address_2'] = array(
//       'label' => __('Street', 'woocommerce'),
//        'required' => true,
//        'class' => array('txt_checkout'),
//        'label_class' => array('text_right'),
//        'clear' => true,
//        'placeholder' => _x('', 'placeholder')
//    );
//    $fields['billing']['billing_city'] = array(
//       'label' => __('Town/City', 'woocommerce'),
//        'required' => true,
//        'class' => array('txt_checkout'),
//        'label_class' => array('text_right'),
//        'clear' => true,
//        'placeholder' => _x('', 'placeholder')
//    );
//    $fields['billing']['billing_state'] = array(
//       'label' => __('State', 'woocommerce'),
//        'required' => true,
//        'class' => array('txt_checkout'),
//        'label_class' => array('text_right'),
//        'clear' => true,
//        'placeholder' => _x('', 'placeholder')
//    );
//    $fields['billing']['billing_phone'] = array(
//       'label' => __('Contact Number', 'woocommerce'),
//        'required' => true,
//        'class' => array('txt_checkout'),
//        'label_class' => array('text_right'),
//        'clear' => true,
//        'placeholder' => _x('', 'placeholder')
//    );
//    return $fields;
//}

function show_all_products(){
// Display 24 products per page. Goes in functions.php
add_filter( 'loop_shop_per_page', create_function( '$cols', 'return 24;' ), 20 );
}

// Ajax response //
function my_special_action() {
    
	print_r($_POST);
	exit;
    $mail_friend_name = $_POST['mail_friend_name'];
    $mail_friend_mail = $_POST['mail_friend_mail'];
    $mail_your_name = $_POST['mail_your_name'];
    $mail_your_mail = $_POST['mail_your_mail'];
    $mail_your_msg = $_POST['mail_your_msg'];
    
    if(isset($mail_friend_mail) && !empty($mail_friend_mail) && isset($mail_friend_name) && !empty($mail_friend_name) && isset($mail_your_mail) && !empty($mail_your_mail) && isset($mail_your_name) && !empty($mail_your_name) && isset($mail_your_msg) && !empty($mail_your_msg) ){
    $headers = 'From: '.$mail_your_name . $mail_your_mail .'"\r\n"';
    wp_mail( $mail_friend_mail, 'Message From '.$mail_your_name, $mail_your_msg, $headers);
    
    echo 'Your Message has been sent to: <b>'. $mail_friend_name. '</b>';

        die(1);
    }  else {
        echo 'There is some error in sending mail at the moment ! <br/> Please try after sometime. Thanks';
        die(1);
    }
    }

add_action('wp_ajax_my_special_action', 'my_special_action');
add_action('wp_ajax_nopriv_my_special_action', 'my_special_action');



/* Development Purposr Only  */

function db($test) {
    echo '<pre>';
    print_r($test);
    echo '</pre>';
}


/* new footer widget areas */
if ( function_exists('register_sidebar') )
   register_sidebar(array('name' => 'Footer 1','before_widget' => '<div id="%1$s" class="box %2$s">','after_widget' => '</div>',));
   register_sidebar(array('name' => 'Footer 2','before_widget' => '<div id="%1$s" class="widget-box %2$s">','after_widget' => '</div>',));
   register_sidebar(array('name' => 'Footer 3','before_widget' => '<div id="%1$s" class="box %2$s">','after_widget' => '</div>',));
   register_sidebar(array('name' => 'Footer 4','before_widget' => '<div id="%1$s" class="widget-box %2$s">','after_widget' => '</div>',));
   register_sidebar(array('name' => 'Footer 5','before_widget' => '<div id="%1$s" class="box %2$s">','after_widget' => '</div>',));
   register_sidebar(array('name' => 'Footer 6','before_widget' => '<div id="%1$s" class="widget-box %2$s">','after_widget' => '</div>',));
