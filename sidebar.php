<?php
/**
 * The template for displaying Sidebar
 */
    
    $sidebar_id = luckiesdesign_get_sidebar_id();
    $class_name = "";
    if ( $sidebar_id === "buddypress-sidebar-widgets" )
        $class_name = " luckiesdesign-buddypress-sidebar";
    else if ( $sidebar_id === "bbpress-sidebar-widgets" )
        $class_name = " luckiesdesign-bbpress-sidebar"; ?>

    <aside id="sidebar" class="luckiesdesign-grid-4<?php echo $class_name; ?>" role="complementary">
        <?php luckiesdesign_hook_begin_sidebar(); ?>

            <?php   // Default Widgets ( Fallback )
                    if ( !($sidebar_id && dynamic_sidebar( $sidebar_id )) ) { ?>
                        <div class="widget sidebar-widget"><h3 class="widgettitle"><?php _e( 'Search', 'luckiesDesign' ); ?></h3><?php get_search_form(); ?></div>
                        <div class="widget sidebar-widget"><h3 class="widgettitle"><?php _e( 'Archives', 'luckiesDesign' ); ?></h3><ul><?php wp_get_archives( array( 'type' => 'monthly' ) ); ?></ul></div>
                        <div class="widget sidebar-widget"><h3 class="widgettitle"><?php _e( 'Meta', 'luckiesDesign' ); ?></h3><ul><?php wp_register(); ?><li><?php wp_loginout(); ?></li><?php wp_meta(); ?></ul></div><?php
                    } ?>

        <?php luckiesdesign_hook_end_sidebar(); ?>
    </aside><!-- #sidebar -->