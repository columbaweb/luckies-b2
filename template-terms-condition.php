<?php
/**
 * Template Name: Terms-Condition Template
 */
get_header();
?>
<div class="row-fluid faq">
    <!--breadcrumb-->
    <?php get_breadcrumb(); ?>
    <div class="span12 about_grey_box">
        <div class="headeing_border">
            <h2><?php the_title(); ?></h2>
        </div>

        <p class="text_center">
            <?php
            if (have_posts()): while (have_posts()): the_post();
                    the_content();
                endwhile;
            endif;
            ?>
        </p>

        <?php
        $terms_post_array = array();
        $terms_condition_args = array('post_type' => 'terms-conditions', 'posts_per_page' => -1);
        $terms_condition_query = new WP_Query($terms_condition_args);

        if ($terms_condition_query->have_posts()) {
            while ($terms_condition_query->have_posts()) {
                $terms_condition_query->the_post();
                $terms_post_array[] = get_the_ID();
            }
        }
        $count_terms = wp_count_posts('terms-conditions');
        if ($count_terms % 2 == 0) {
            $half_terms = $count_terms / 2;
        } else {
            $half_terms = ($count_terms + 1) / 2;
        }
        wp_reset_postdata();
        wp_reset_query();
        ?>

        <div class="span6 faq_left terms-condition">
            <?php
            $count = 0;
            foreach ($terms_post_array as $single_condition) {
                if ($count < $half_terms) {
                    $single_condition_object = get_post($single_condition);
                    ?>
                    <div class="accordion faq-post terms-condition-post">
                        <h2 class="faq-question-title terms-condition-title"><?php echo $single_condition_object->post_title; ?></h2>
                        <div class="pane faq-content terms-condition-content">
                            <h2 class="faq-answer terms-condition-answer"><?php echo $single_condition_object->post_content; ?></h2>
                        </div>
                    </div>
                    <?php
                    array_shift($terms_post_array);
                    $count++;
                }
            }
            ?>
        </div>
        <div class="span6 fr faq_right">
            <?php
            foreach ($terms_post_array as $single_condition) {
                $single_condition_object = get_post($single_condition);
                ?>
                <div class="accordion faq-post">
                    <h2 class="faq-question-title"><?php echo $single_condition_object->post_title; ?></h2>
                    <div class="pane faq-content">
                        <h2 class="faq-answer"><?php echo $single_condition_object->post_content; ?></h2>
                    </div>
                </div>
                <?php
                array_shift($terms_post_array);
            }
            ?>
        </div>

    </div>
</div>

<?php get_footer(); ?>