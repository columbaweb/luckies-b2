<?php
/**
 * Template Name: WOO Template
 */
get_header();
?>
<div class="row-fluid about_us_page">
    <!--breadcrumb-->
    <?php get_breadcrumb(); ?>
            <?php
            if (have_posts()): while (have_posts()): the_post();
                    the_content();
                endwhile;
            endif;
            ?>
</div>

<?php get_footer(); ?>




