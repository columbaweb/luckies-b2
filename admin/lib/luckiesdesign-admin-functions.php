<?php
/**
 * luckiesDesign Admin Functions
 */

global $luckiesdesign_general, $luckiesdesign_custom_theme_options, $luckiesdesign_post_comments, $luckiesdesign_hooks, $luckiesdesign_version;

/**
 * Data validation for luckiesDesign General Options
 * 
 * @uses $luckiesdesign_general array
 * @return Array
 */
function luckiesdesign_general_validate($input) {
    global $luckiesdesign_general;
    require_once( ABSPATH . '/wp-admin/includes/class-wp-filesystem-base.php' );
    require_once( ABSPATH . '/wp-admin/includes/class-wp-filesystem-direct.php' );
    @$file_object = new WP_Filesystem_Direct;
    $default = luckiesdesign_theme_setup_values();

    if (isset($_POST['luckiesdesign_submit'])) {
        add_filter('intermediate_image_sizes_advanced', 'luckiesdesign_create_favicon');
        if ('image' == $input['logo_use'] && !empty($_FILES) && isset($_FILES['html-upload-logo']) && $_FILES['html-upload-logo']['size']) {
            if (substr($_FILES['html-upload-logo']['type'], 0, 5) == 'image') {
                $id = media_handle_upload('html-upload-logo', 0);
                if (is_wp_error($id)) {
                    if (!empty($id->errors['upload_error']))
                        $logo_errors = $id->errors['upload_error'];
                    if (!empty($logo_errors)) {
                        foreach ($logo_errors as $logo_error) {
                            add_settings_error('html-upload-logo', 'html-upload-logo', $logo_error, 'error');
                        }
                    }
                } else {
                    $img_src = wp_get_attachment_image_src($id, 'full', true);
                    $input['logo_upload'] = $img_src[0];
                    $input['logo_id'] = $id;
                    $input['logo_width'] = $img_src[1];
                    $input['logo_height'] = $img_src[2];
                    add_settings_error('html-upload-logo', 'html-upload-logo', __('Logo & Favicon Settings Updated', 'luckiesDesign'), 'updated');
                }
            } else {
                add_settings_error('html-upload-logo', 'html-upload-logo', __('Please upload a valid image file.', 'luckiesDesign'), 'error');
            }
        }
        if ('image' == $input['favicon_use'] && !empty($_FILES) && isset($_FILES['html-upload-fav']) && $_FILES['html-upload-fav']['size']) {
// Upload File button was clicked
            if (substr($_FILES['html-upload-fav']['type'], 0, 5) == 'image') {
                $id = media_handle_upload('html-upload-fav', 0);
                if (is_wp_error($id)) {
                    if (!empty($id->errors['upload_error']))
                        $fav_errors = $id->errors['upload_error'];
                    if (!empty($fav_errors)) {
                        foreach ($fav_errors as $fav_error) {
                            add_settings_error('html-upload-fav', 'html-upload-fav', $fav_error, 'error');
                        }
                    }
                } else {
                    $img_src = wp_get_attachment_image_src($id, 'favicon', true);
                    $input['favicon_upload'] = $img_src[0];
                    $input['favicon_id'] = $id;
                    add_settings_error('html-upload-fav', 'html-upload-fav', __('Logo & Favicon Settings Updated', 'luckiesDesign'), 'updated');
                }
            } else {
                add_settings_error('html-upload-fav', 'html-upload-fav', __('Please upload a valid image file.', 'luckiesDesign'), 'error');
            }
        } elseif ('logo' == $input['favicon_use']) {
            if (LUCKIESDESIGN_IMG_FOLDER_URL . '/luckiesdesign-logo.jpg' == $input['logo_upload']) {
                $input['favicon_upload'] = LUCKIESDESIGN_IMG_FOLDER_URL . '/favicon.ico';
                $input['favicon_id'] = 0;
            } else {
                $img_src = wp_get_attachment_image_src($input['logo_id'], 'favicon', true);
                $input['favicon_upload'] = $img_src[0];
                $input['favicon_id'] = $input['logo_id'];
            }
        }
        remove_filter('intermediate_image_sizes_advanced', 'luckiesdesign_create_favicon');

        if ('image' != $input['logo_use']) {
            $input['login_head'] = $luckiesdesign_general['login_head'];
        }

        if (!empty($input['feedburner_url'])) {
            $result = wp_remote_get($input['feedburner_url']);
            if (is_wp_error($result) || $result["response"]["code"] != 200) {
                $input['feedburner_url'] = $luckiesdesign_general['feedburner_url'];
                add_settings_error('feedburner_url', 'invalid_feedburner_url', __('The FeedBurner URL is not a valid url. The changes made have been reverted.', 'luckiesDesign'));
            } elseif ($input['feedburner_url'] != $luckiesdesign_general['feedburner_url']) {
                add_settings_error('feedburner_url', 'valid_feedburner_url', __('The FeedBurner Settings have been updated.', 'luckiesDesign'), 'updated');
            }
        }

        if (trim($input['fb_app_id']) != $luckiesdesign_general['fb_app_id']) {
            $input['fb_app_id'] = trim($input['fb_app_id']);
            add_settings_error('fb_app_id', 'valid_fb_app_id', __('The Facebook App ID has been updated.', 'luckiesDesign'), 'updated');
        }

        if (trim($input['fb_admins']) != $luckiesdesign_general['fb_admins']) {
            $input['fb_admins'] = trim($input['fb_admins']);
            add_settings_error('fb_admins', 'valid_fb_admins', __('The Facebook Admin ID(s) has been updated.', 'luckiesDesign'), 'updated');
        }

        if (!empty($input['search_code'])) {
            if (!preg_match('/customSearchControl.draw\(\'cse\'(.*)\)\;/i', $input['search_code']) && !preg_match('/\<gcse:(searchresults-only|searchresults|search).*\>\<\/gcse:(searchresults-only|searchresults|search)\>/i', $input['search_code'])) {
                $input['search_code'] = $luckiesdesign_general['search_code'];
                add_settings_error('search_code', 'invalid_search_code', __('Google Search Code Error : While generating the code the layout option should either be "full-width" or "compact". The changes made have been reverted.', 'luckiesDesign'));
            } elseif ($input['search_code'] != $luckiesdesign_general['search_code']) {
                add_settings_error('search_code', 'valid_search_code', __('Google Custom Search Integration has been updated.', 'luckiesDesign'), 'updated');
            }
        }
        
    } elseif (isset($_POST['luckiesdesign_logo_favicon_reset'])) {
        $options = maybe_unserialize($luckiesdesign_general);
        unset($input);

        foreach ($options as $option => $value)
            $input[$option] = $value;

        $input['logo_use'] = $default[0]['logo_use'];
        $input['logo_upload'] = $default[0]['logo_upload'];
        $input['logo_id'] = $default[0]['logo_id'];
        $input['logo_width'] = $default[0]['logo_width'];
        $input['logo_height'] = $default[0]['logo_height'];
        $input['login_head'] = $default[0]['login_head'];
        $input['favicon_use'] = $default[0]['favicon_use'];
        $input['favicon_upload'] = $default[0]['favicon_upload'];
        $input['favicon_id'] = $default[0]['favicon_id'];
        add_settings_error('logo_favicon_settings', 'logo_favicon_reset', __('The Logo Settings have been restored to Default.', 'luckiesDesign'), 'updated');
    } elseif (isset($_POST['luckiesdesign_fb_ogp_reset'])) {
        $options = maybe_unserialize($luckiesdesign_general);
        unset($input);

        foreach ($options as $option => $value)
            $input[$option] = $value;

        $input['fb_app_id'] = $default[0]['fb_app_id'];
        $input['fb_admins'] = $default[0]['fb_admins'];
        add_settings_error('facebook_ogp', 'reset_facebook_ogp', __('The Facebook Open Graph Settings have been restored to Default.', 'luckiesDesign'), 'updated');
    } elseif (isset($_POST['luckiesdesign_feed_reset'])) {
        $options = maybe_unserialize($luckiesdesign_general);
        unset($input);

        foreach ($options as $option => $value)
            $input[$option] = $value;

        $input['feedburner_url'] = $default[0]['feedburner_url'];
        add_settings_error('feedburner_url', 'reset_feeburner_url', __('The Feedburner Settings have been restored to Default.', 'luckiesDesign'), 'updated');
    } elseif (isset($_POST['luckiesdesign_google_reset'])) {
        $options = maybe_unserialize($luckiesdesign_general);
        unset($input);

        foreach ($options as $option => $value)
            $input[$option] = $value;

        $input['search_code'] = $default[0]['search_code'];
        $input['search_layout'] = $default[0]['search_layout'];
        add_settings_error('search_code', 'reset_search_code', __('The Google Custom Search Integration has been restored to Default.', 'luckiesDesign'), 'updated');
    } elseif (isset($_POST['luckiesdesign_sidebar_reset'])) {
        $options = maybe_unserialize($luckiesdesign_general);
        unset($input);

        foreach ($options as $option => $value)
            $input[$option] = $value;

        $input['footer_sidebar'] = $default[0]['footer_sidebar'];
        $input['buddypress_sidebar'] = $default[0]['buddypress_sidebar'];
        $input['bbpress_sidebar'] = $default[0]['bbpress_sidebar'];
        add_settings_error('sidebar', 'reset_sidebar', __('The Sidebar Settings have been restored to Default.', 'luckiesDesign'), 'updated');
    } elseif (isset($_POST['luckiesdesign_custom_styles_reset'])) {
        $options = maybe_unserialize($luckiesdesign_general);
        unset($input);

        foreach ($options as $option => $value)
            $input[$option] = $value;

        $input['custom_styles'] = $default[0]['custom_styles'];
        add_settings_error('custom_styles', 'reset_custom_styles', __('Custom Styles has been restored to Default.', 'luckiesDesign'), 'updated');
    } elseif (isset($_POST['luckiesdesign_export'])) {
        luckiesdesign_export();
        die();
    } elseif (isset($_POST['luckiesdesign_import'])) {
        $general = luckiesdesign_import($_FILES['luckiesdesign_import']);
        if ($general && $general != 'ext') {
            unset($input);
            $input = maybe_unserialize($general);
            add_settings_error('luckiesdesign_import', 'import', __('luckiesDesign Options have been imported successfully', 'luckiesDesign'), 'updated');
        } elseif ($general == 'ext') {
            add_settings_error('luckiesdesign_import', 'no_import', __('Not a valid LD file', 'luckiesDesign'));
        } else {
            add_settings_error('luckiesdesign_import', 'no_import', __('The file is corrupt. There was an error while importing. Please Try Again', 'luckiesDesign'));
        }
    } elseif (isset($_POST['luckiesdesign_reset'])) {
        $input = $default[0];
        add_settings_error('luckiesdesign_general', 'reset_general_options', __('All the luckiesDesign General Settings have been restored to default.', 'luckiesDesign'), 'updated');
    }
    return $input; // Return validated input.
}

/**
 * Data validation for luckiesDesign Post & Comments Options
 * 
 * @uses $luckiesdesign_post_comments array
 * @param array $input all post & comments options inputs.
 * @return Array
 */
function luckiesdesign_post_comments_validate($input) {
    global $luckiesdesign_post_comments;
    $default = luckiesdesign_theme_setup_values();

    if (isset($_POST['luckiesdesign_submit'])) {
        $input['notices'] = $luckiesdesign_post_comments['notices'];
        if ($input['summary_show']) {
            $updated = 0;
            if (trim($input['read_text']) != $luckiesdesign_post_comments['read_text']) {
                $input['read_text'] = trim($input['read_text']);
                $updated++;
            }
            if (!preg_match('/^[0-9]{1,3}$/i', $input['word_limit'])) {
                $input['word_limit'] = $luckiesdesign_post_comments['word_limit'];
                add_settings_error('word_limit', 'invalid_word_limit', __('The Word Limit provided is invalid. Please provide a proper value.', 'luckiesDesign'));
            } elseif (trim($input['word_limit']) != $luckiesdesign_post_comments['word_limit']) {
                $updated++;
            }
            if ($updated) {
                add_settings_error('post_summary_settings', 'post_summary_settings', __('The Post Summary Settings have been updated.', 'luckiesDesign'), 'updated');
            }
            if ($input['thumbnail_show']) {
                $updated = 0;
                if (!preg_match('/^[0-9]{1,3}$/i', $input['thumbnail_width'])) {
                    $input['thumbnail_width'] = get_option('thumbnail_size_w');
                    add_settings_error('thumbnail_width', 'invalid_thumbnail_width', __('The Thumbnail Width provided is invalid. Please provide a proper value.', 'luckiesDesign'));
                } elseif (get_option('thumbnail_size_w') != $input['thumbnail_width']) {
                    $input['notices'] = '1';
                    update_option('thumbnail_size_w', $input['thumbnail_width']);
                    $updated++;
                }

                if (!preg_match('/^[0-9]{1,3}$/i', $input['thumbnail_height'])) {
                    $input['thumbnail_height'] = get_option('thumbnail_size_h');
                    add_settings_error('thumbnail_height', 'invalid_thumbnail_height', __('The Thumbnail Height provided is invalid. Please provide a proper value.', 'luckiesDesign'));
                } elseif (get_option('thumbnail_size_h') != $input['thumbnail_height']) {
                    $input['notices'] = '1';
                    update_option('thumbnail_size_h', $input['thumbnail_height']);
                    $updated++;
                }

                if ($input['thumbnail_crop'] != get_option('thumbnail_crop')) {
                    $input['notices'] = '1';
                    update_option('thumbnail_crop', $input['thumbnail_crop']);
                    $updated++;
                }
                if ($updated) {
                    add_settings_error('post_thumbnail_settings', 'post_thumbnail_settings', __('The Post Thumbnail Settings have been updated', 'luckiesDesign'), 'updated');
                }
            } else {
                $input['thumbnail_position'] = $luckiesdesign_post_comments['thumbnail_position'];
                $input['thumbnail_frame'] = $luckiesdesign_post_comments['thumbnail_frame'];
            }
        } else {
            $input['thumbnail_show'] = $luckiesdesign_post_comments['thumbnail_show'];
            $input['word_limit'] = $luckiesdesign_post_comments['word_limit'];
            $input['read_text'] = $luckiesdesign_post_comments['read_text'];
            $input['thumbnail_position'] = $luckiesdesign_post_comments['thumbnail_position'];
            $input['thumbnail_frame'] = $luckiesdesign_post_comments['thumbnail_frame'];
        }

        if (!in_array($input['post_date_format_u'], array($luckiesdesign_post_comments['post_date_format_u'], 'F j, Y', 'Y/m/d', 'm/d/Y', 'd/m/Y'))) {
            $input['post_date_format_u'] = str_replace('<', '', $input['post_date_format_u']);
            $input['post_date_format_l'] = str_replace('<', '', $input['post_date_format_l']);
            $input['post_date_custom_format_u'] = str_replace('<', '', $input['post_date_custom_format_u']);
            $input['post_date_custom_format_l'] = str_replace('<', '', $input['post_date_custom_format_l']);
        }

        if (!$input['post_date_u']) {
            $input['post_date_format_u'] = $luckiesdesign_post_comments['post_date_format_u'];
            $input['post_date_custom_format_u'] = $luckiesdesign_post_comments['post_date_custom_format_u'];
        }

        if (!$input['post_date_l']) {
            $input['post_date_format_l'] = $luckiesdesign_post_comments['post_date_format_l'];
            $input['post_date_custom_format_l'] = $luckiesdesign_post_comments['post_date_custom_format_l'];
        }

        if (!$input['post_author_u']) {
            $input['author_count_u'] = $luckiesdesign_post_comments['author_count_u'];
            $input['author_link_u'] = $luckiesdesign_post_comments['author_link_u'];
        }

        if (!$input['post_author_l']) {
            $input['author_count_l'] = $luckiesdesign_post_comments['author_count_l'];
            $input['author_link_l'] = $luckiesdesign_post_comments['author_link_l'];
        }

        if ($input['pagination_show']) {
            $updated = 0;
            if (trim($input['prev_text']) != $luckiesdesign_post_comments['prev_text']) {
                $input['prev_text'] = trim($input['prev_text']);
                $updated++;
            }
            if (trim($input['next_text']) != $luckiesdesign_post_comments['next_text']) {
                $input['next_text'] = trim($input['next_text']);
                $updated++;
            }
            if (!preg_match('/^[0-9]{1,3}$/i', $input['end_size'])) {
                $input['end_size'] = $luckiesdesign_post_comments['end_size'];
                add_settings_error('end_size', 'invalid_end_size', __('The End Size provided is invalid. Please provide a proper value.', 'luckiesDesign'));
            }
            if (!preg_match('/^[0-9]{1,3}$/i', $input['mid_size'])) {
                $input['mid_size'] = $luckiesdesign_post_comments['mid_size'];
                add_settings_error('mid_size', 'invalid_mid_size', __('The Mid Size provided is invalid. Please provide a proper value.', 'luckiesDesign'));
            }
            if ($updated) {
                add_settings_error('pagination_settings', 'pagination_settings', __('The Pagination Settings have been updated.', 'luckiesDesign'), 'updated');
            }
        } else {
            $input['prev_text'] = $luckiesdesign_post_comments['prev_text'];
            $input['next_text'] = $luckiesdesign_post_comments['next_text'];
            $input['end_size'] = $luckiesdesign_post_comments['end_size'];
            $input['mid_size'] = $luckiesdesign_post_comments['mid_size'];
        }

        if (!$input['gravatar_show']) {
            $input['gravatar_size'] = $luckiesdesign_post_comments['gravatar_size'];
        }
    } elseif (isset($_POST['luckiesdesign_summary_reset'])) {
        $options = maybe_unserialize($luckiesdesign_post_comments);
        unset($input);

        foreach ($options as $option => $value)
            $input[$option] = $value;

        $input['notices'] = $luckiesdesign_post_comments['notices'];
        $input['summary_show'] = $default[1]['summary_show'];
        $input['word_limit'] = $default[1]['word_limit'];
        $input['read_text'] = $default[1]['read_text'];
        add_settings_error('summary', 'reset_summary', __('The Post Summary Settings have been restored to default.', 'luckiesDesign'), 'updated');
    } elseif (isset($_POST['luckiesdesign_thumbnail_reset'])) {
        $options = maybe_unserialize($luckiesdesign_post_comments);
        unset($input);

        foreach ($options as $option => $value)
            $input[$option] = $value;

        $input['notices'] = $luckiesdesign_post_comments['notices'];
        $input['thumbnail_show'] = $default[1]['thumbnail_show'];
        $input['thumbnail_position'] = $default[1]['thumbnail_position'];
        $input['thumbnail_frame'] = $default[1]['thumbnail_frame'];
        add_settings_error('thumbnail', 'reset_thumbnail', __('The Post Thumbnail Settings have been restored to default.', 'luckiesDesign'), 'updated');
    } elseif (isset($_POST['luckiesdesign_meta_reset'])) {
        $options = maybe_unserialize($luckiesdesign_post_comments);
        unset($input);

        foreach ($options as $option => $value)
            $input[$option] = $value;

        $input['notices'] = $luckiesdesign_post_comments['notices'];
        $input['post_date_u'] = $default[1]['post_date_u'];
        $input['post_date_format_u'] = $default[1]['post_date_format_u'];
        $input['post_date_custom_format_u'] = $default[1]['post_date_custom_format_u'];
        $input['post_author_u'] = $default[1]['post_author_u'];
        $input['author_count_u'] = $default[1]['author_count_u'];
        $input['author_link_u'] = $default[1]['author_link_u'];
        $input['post_category_u'] = $default[1]['post_category_u'];
        $input['post_tags_u'] = $default[1]['post_tags_u'];
        $input['post_date_l'] = $default[1]['post_date_l'];
        $input['post_date_format_l'] = $default[1]['post_date_format_l'];
        $input['post_date_custom_format_l'] = $default[1]['post_date_custom_format_l'];
        $input['post_author_l'] = $default[1]['post_author_l'];
        $input['author_count_l'] = $default[1]['author_count_l'];
        $input['author_link_l'] = $default[1]['author_link_l'];
        $input['post_category_l'] = $default[1]['post_category_l'];
        $input['post_tags_l'] = $default[1]['post_tags_l'];
        $args = array('_builtin' => false);
        $taxonomies = get_taxonomies($args, 'names');

        if (!empty($taxonomies)) {
            foreach ($taxonomies as $taxonomy) {
                $input['post_' . $taxonomy . '_u'] = '0';
                $input['post_' . $taxonomy . '_l'] = '0';
            }
        }
        add_settings_error('post_meta', 'reset_post_meta', __('The Post Meta Settings have been restored to default.', 'luckiesDesign'), 'updated');
    } elseif (isset($_POST['luckiesdesign_pagination_reset'])) {
        $options = maybe_unserialize($luckiesdesign_post_comments);
        unset($input);

        foreach ($options as $option => $value)
            $input[$option] = $value;

        $input['notices'] = $luckiesdesign_post_comments['notices'];
        $input['pagination_show'] = $default[1]['pagination_show'];
        $input['prev_text'] = $default[1]['prev_text'];
        $input['next_text'] = $default[1]['next_text'];
        $input['end_size'] = $default[1]['end_size'];
        $input['mid_size'] = $default[1]['mid_size'];
        add_settings_error('pagination', 'reset_pagination', __('The Pagination Settings have been restored to default.', 'luckiesDesign'), 'updated');
    } elseif (isset($_POST['luckiesdesign_comment_reset'])) {
        $options = maybe_unserialize($luckiesdesign_post_comments);
        unset($input);

        foreach ($options as $option => $value)
            $input[$option] = $value;

        $input['notices'] = $luckiesdesign_post_comments['notices'];
        $input['compact_form'] = $default[1]['compact_form'];
        $input['hide_labels'] = $default[1]['hide_labels'];
        $input['comment_textarea'] = $default[1]['comment_textarea'];
        $input['comment_separate'] = $default[1]['comment_separate'];
        add_settings_error('comment', 'reset_comment', __('The Comment Form Settings have been restored to default.', 'luckiesDesign'), 'updated');
    } elseif (isset($_POST['luckiesdesign_gravatar_reset'])) {
        $options = maybe_unserialize($luckiesdesign_post_comments);
        unset($input);

        foreach ($options as $option => $value)
            $input[$option] = $value;

        $input['notices'] = $luckiesdesign_post_comments['notices'];
        $input['gravatar_show'] = $default[1]['gravatar_show'];
        $input['gravatar_size'] = $default[1]['gravatar_size'];
        add_settings_error('gravatar', 'reset_gravatar', __('The Gravatar Settings have been restored to default.', 'luckiesDesign'), 'updated');
    } elseif (isset($_POST['luckiesdesign_reset'])) {
        $input = $default[1];
        $input['notices'] = $luckiesdesign_post_comments['notices'];
        $args = array('_builtin' => false);
        $taxonomies = get_taxonomies($args, 'names');
        if (!empty($taxonomies)) {
            foreach ($taxonomies as $taxonomy) {
                $input['post_' . $taxonomy . '_u'] = '0';
                $input['post_' . $taxonomy . '_l'] = '0';
            }
        }
        add_settings_error('luckiesdesign_post_comments', 'reset_post_comments_options', __('All the luckiesDesign Post & Comments Settings have been restored to default.', 'luckiesDesign'), 'updated');
    }
    return $input; // return validated input
}

/**
 * Setup Default Values for luckiesDesign
 *
 * This function sets up default values for 'luckiesDesign' and creates
 * 2 options in the WordPress options table: 'luckiesdesign_general' &
 * 'luckiesdesign_post_comments', where the values for the 'General' and
 * 'Post & Comments' tabs are stored respectively
 *
 * @return array.
 */
function luckiesdesign_theme_setup_values() {
    global $luckiesdesign_general, $luckiesdesign_post_comments, $luckiesdesign_version;

    $default_general = array(
        'logo_use' => 'image',
        'logo_upload' => LUCKIESDESIGN_IMG_FOLDER_URL . '/logo.jpg',
        'logo_id' => 0,
        'logo_width' => 224,
        'logo_height' => 51,
        'login_head' => '0',
        'favicon_use' => 'image',
        'favicon_upload' => LUCKIESDESIGN_IMG_FOLDER_URL . '/favicon.ico',
        'favicon_id' => 0,
        'fb_app_id' => '',
        'fb_admins' => '',
        'feedburner_url' => '',
        'footer_sidebar' => '1',
        'buddypress_sidebar' => 'default-sidebar',
        'bbpress_sidebar' => 'default-sidebar',
        'custom_styles' => '',
        'search_code' => '',
        'search_layout' => '1',
    );

    $default_post_comments = array(
        'notices' => isset($luckiesdesign_post_comments['notices']) ? $luckiesdesign_post_comments['notices'] : 0,
        'summary_show' => '1',
        'word_limit' => 55,
        'read_text' => __('Read More&hellip;', 'luckiesDesign'),
        'thumbnail_show' => '1',
        'thumbnail_position' => 'Right',
        'thumbnail_width' => get_option('thumbnail_size_w'),
        'thumbnail_height' => get_option('thumbnail_size_h'),
        'thumbnail_crop' => get_option('thumbnail_crop'),
        'thumbnail_frame' => '0',
        'post_date_u' => '1',
        'post_date_format_u' => 'F j, Y',
        'post_date_custom_format_u' => 'F j, Y',
        'post_author_u' => '1',
        'author_count_u' => '0',
        'author_link_u' => '1',
        'post_category_u' => '1',
        'post_tags_u' => '0',
        'post_date_l' => '0',
        'post_date_format_l' => 'F j, Y',
        'post_date_custom_format_l' => 'F j, Y',
        'post_author_l' => '0',
        'author_count_l' => '0',
        'author_link_l' => '1',
        'post_category_l' => '0',
        'post_tags_l' => '0',
        'pagination_show' => '1',
        'prev_text' => '&laquo; Previous',
        'next_text' => 'Next &raquo;',
        'end_size' => '1',
        'mid_size' => '2',
        'compact_form' => '1',
        'hide_labels' => '1',
        'comment_textarea' => '0',
        'comment_separate' => '1',
        'attachment_comments' => '0',
        'gravatar_show' => '1',
        'gravatar_size' => '64',
    );

    $args = array('_builtin' => false);
    $taxonomies = get_taxonomies($args, 'names');
    if (!empty($taxonomies)) {
        foreach ($taxonomies as $taxonomy) {
            $default_post_comments['post_' . $taxonomy . '_u'] = '0';
            $default_post_comments['post_' . $taxonomy . '_l'] = '0';
        }
    }

    if (!get_option('luckiesdesign_general')) {
        update_option('luckiesdesign_general', $default_general);
        $blog_users = get_users();

        foreach ($blog_users as $blog_user) {
            $blog_user_id = $blog_user->ID;
            if (!get_user_meta($blog_user_id, 'screen_layout_appearance_page_luckiesdesign_general'))
                update_user_meta($blog_user_id, 'screen_layout_appearance_page_luckiesdesign_general', 1, NULL);
        }
    }
    if (!get_option('luckiesdesign_post_comments')) {
        update_option('luckiesdesign_post_comments', $default_post_comments);
        $blog_users = get_users();

        foreach ($blog_users as $blog_user) {
            $blog_user_id = $blog_user->ID;
            if (!get_user_meta($blog_user_id, 'screen_layout_appearance_page_luckiesdesign_post_comments'))
                update_user_meta($blog_user_id, 'screen_layout_appearance_page_luckiesdesign_post_comments', 1, NULL);
        }
    }

    $luckiesdesign_version = luckiesdesign_export_version();
    if (!get_option('luckiesdesign_version') || ( get_option('luckiesdesign_version') != $luckiesdesign_version )) {
        update_option('luckiesdesign_version', $luckiesdesign_version);
        $updated_general = wp_parse_args($luckiesdesign_general, $default_general);
        $updated_post_comments = wp_parse_args($luckiesdesign_post_comments, $default_post_comments);
        update_option('luckiesdesign_general', $updated_general);
        update_option('luckiesdesign_post_comments', $updated_post_comments);
    }

    return array($default_general, $default_post_comments);
}

// Redirect to luckiesDesign on theme activation //
function luckiesdesign_theme_activation($themename, $theme = false) {
    global $luckiesdesign_general;
    $update = 0;
    if (isset($luckiesdesign_general['logo_show']) && $luckiesdesign_general['logo_show']) {
        $update++;
        $luckiesdesign_general['logo_use'] = 'image';
        unset($luckiesdesign_general['logo_show']);
    } elseif (isset($luckiesdesign_general['logo_show'])) {
        $update++;
        $luckiesdesign_general['logo_use'] = 'site_title';
        unset($luckiesdesign_general['logo_show']);
    }
    if (isset($luckiesdesign_general['use_logo']) && ( $luckiesdesign_general['logo_use'] == 'use_logo_url' )) {
        $update++;
        $luckiesdesign_general['logo_upload'] = $luckiesdesign_general['logo_url'];
        $id = luckiesdesign_get_attachment_id_from_src($luckiesdesign_general['logo_upload'], true);
        $img_dimensions = luckiesdesign_get_image_dimensions($luckiesdesign_general['logo_upload'], true, '', $id);
        $luckiesdesign_general['logo_id'] = $id;
        $luckiesdesign_general['logo_width'] = $img_dimensions['width'];
        $luckiesdesign_general['logo_height'] = $img_dimensions['height'];
        unset($luckiesdesign_general['use_logo']);
        unset($luckiesdesign_general['logo_url']);
    } elseif (isset($luckiesdesign_general['use_logo']) && ( $luckiesdesign_general['use_logo'] == 'use_logo_upload' )) {
        $update++;
        $id = luckiesdesign_get_attachment_id_from_src($luckiesdesign_general['logo_upload'], true);
        $img_dimensions = luckiesdesign_get_image_dimensions($luckiesdesign_general['logo_upload'], true, '', $id);
        $luckiesdesign_general['logo_id'] = $id;
        $luckiesdesign_general['logo_width'] = $img_dimensions['width'];
        $luckiesdesign_general['logo_height'] = $img_dimensions['height'];
        unset($luckiesdesign_general['use_logo']);
    }
    if (isset($luckiesdesign_general['favicon_show']) && $luckiesdesign_general['favicon_show']) {
        $update++;
        $luckiesdesign_general['favicon_use'] = 'image';
        unset($luckiesdesign_general['favicon_show']);
    } elseif (isset($luckiesdesign_general['favicon_show'])) {
        $update++;
        $luckiesdesign_general['favicon_use'] = 'disable';
        unset($luckiesdesign_general['favicon_show']);
    }
    if (isset($luckiesdesign_general['use_favicon']) && ( $luckiesdesign_general['use_favicon'] == 'use_favicon_url' )) {
        $update++;
        $luckiesdesign_general['favicon_upload'] = $luckiesdesign_general['favicon_url'];
        $id = luckiesdesign_get_attachment_id_from_src($luckiesdesign_general['favicon_upload'], true);
        $img_dimensions = luckiesdesign_get_image_dimensions($luckiesdesign_general['favicon_upload'], true, '', $id);
        $luckiesdesign_general['favicon_id'] = $id;
        unset($luckiesdesign_general['use_favicon']);
    } elseif (isset($luckiesdesign_general['use_favicon']) && ( $luckiesdesign_general['use_favicon'] == 'use_favicon_upload' )) {
        $update++;
        $luckiesdesign_general['favicon_id'] = luckiesdesign_get_attachment_id_from_src($luckiesdesign_general['favicon_upload'], true);
        unset($luckiesdesign_general['use_favicon']);
        unset($luckiesdesign_general['favicon_url']);
    }
    if ($update) {
        update_option('luckiesdesign_general', $luckiesdesign_general);
    }
}

add_action('after_switch_theme', 'luckiesdesign_theme_activation', '', 2);

/**
 * Feedburner Redirection Code
 *
 * @uses string $feed
 * @uses array $luckiesdesign_general
 */
function luckiesdesign_feed_redirect() {
    global $feed, $luckiesdesign_general, $withcomments;
    if (is_feed() && $feed != 'comments-rss2' && ( $withcomments != 1 ) && !is_singular() && !is_archive() && !empty($luckiesdesign_general['feedburner_url'])) {
        if (function_exists('status_header')) {
            status_header(302);
        }
        header('Location: ' . trim($luckiesdesign_general['feedburner_url']));
        header('HTTP/1.1 302 Temporary Redirect');
        exit();
    }
}

/**
 * Used to check the feed type ( default or comment feed )
 *
 * @uses $luckiesdesign_general array
 */
function luckiesdesign_check_url() {
    global $luckiesdesign_general;
    switch (basename($_SERVER['PHP_SELF'])) {
        case 'wp-rss.php' :
        case 'wp-rss2.php' :
        case 'wp-atom.php' :
        case 'wp-rdf.php' : if (trim($luckiesdesign_general['feedburner_url']) != '') {
                if (function_exists('status_header')) {
                    status_header(302);
                }
                header('Location: ' . trim($luckiesdesign_general['feedburner_url']));
                header('HTTP/1.1 302 Temporary Redirect');
                exit();
            }
            break;

        case 'wp-commentsrss2.php': break;
    }
}

/* Condition to redirect WordPress feeds to feed burner */
if (isset($_SERVER['HTTP_USER_AGENT']) && !preg_match('/feedburner|feedvalidator/i', $_SERVER['HTTP_USER_AGENT'])) {
    add_action('template_redirect', 'luckiesdesign_feed_redirect');
    add_action('init', 'luckiesdesign_check_url');
}

/* condition to check Admin Login Logo option */
if (isset($luckiesdesign_general['logo_use']) && isset($luckiesdesign_general['login_head']) && ( 'image' == $luckiesdesign_general['logo_use'] ) && $luckiesdesign_general['login_head']) {
    add_action('login_head', 'luckiesdesign_custom_login_logo');
    add_filter('login_headerurl', 'luckiesdesign_login_site_url');
}

/**
 * Dislays custom logo on Login Page
 *
 * @uses $luckiesdesign_general array
 */
function luckiesdesign_custom_login_logo() {
    global $luckiesdesign_general;
    $custom_logo = $luckiesdesign_general['logo_upload'];
    if (isset($luckiesdesign_general['logo_width']) && !empty($luckiesdesign_general['logo_width']) && isset($luckiesdesign_general['logo_height']) && !empty($luckiesdesign_general['logo_height'])) {
        $luckiesdesign_logo_width = $luckiesdesign_general['logo_width'];
        $luckiesdesign_logo_height = $luckiesdesign_general['logo_height'];
    } else {
        $dimensions = luckiesdesign_get_image_dimensions($custom_logo, true);
        if (isset($dimensions['width']) && isset($dimensions['height'])) {
            $luckiesdesign_logo_width = $dimensions['width'];
            $luckiesdesign_logo_height = $dimensions['height'];
        } else {
            $luckiesdesign_logo_width = $luckiesdesign_logo_height = 0;
        }
    }
    $luckiesdesign_wp_loginbox_width = 312;
    if ($luckiesdesign_logo_width > $luckiesdesign_wp_loginbox_width) {
        $ratio = $luckiesdesign_logo_height / $luckiesdesign_logo_width;
        $luckiesdesign_logo_height = ceil($ratio * $luckiesdesign_wp_loginbox_width);
        $luckiesdesign_logo_width = $luckiesdesign_wp_loginbox_width;
        $luckiesdesign_background_size = 'contain';
    } else {
        $luckiesdesign_background_size = 'auto';
    }

    echo '<style type="text/css">
        .login h1 { margin-left: 8px; }
        .login h1 a { background: url(' . $custom_logo . ') no-repeat 50% 0;
                background-size: ' . $luckiesdesign_background_size . ';';
    if ($luckiesdesign_logo_width && $luckiesdesign_logo_height) {
        echo 'height: ' . $luckiesdesign_logo_height . 'px;
              width: ' . $luckiesdesign_logo_width . 'px; margin: 0 auto 15px; padding: 0; }';
    }
    echo '</style>';
}

/**
 * Returns Home URL, to be used by custom logo
 * 
 * @return string
 */
function luckiesdesign_login_site_url() {
    return home_url('/');
}


/**
 * Display feeds from a specified Feed URL
 *
 * @param string $feed_url The Feed URL.
 */
function luckiesdesign_get_feeds($feed_url = 'http://www.luckiesdesign.net/feed/') {

// Get RSS Feed(s)
    require_once( ABSPATH . WPINC . '/feed.php' );
    $maxitems = 0;
// Get a SimplePie feed object from the specified feed source.
    $rss = fetch_feed($feed_url);
    if (!is_wp_error($rss)) { // Checks that the object is created correctly
// Figure out how many total items there are, but limit it to 5.
        $maxitems = $rss->get_item_quantity(5);

// Build an array of all the items, starting with element 0 (first element).
        $rss_items = $rss->get_items(0, $maxitems);
    }
    ?>
    <ul><?php
    if ($maxitems == 0) {
        echo '<li>' . __('No items', 'luckiesDesign') . '.</li>';
    } else {
// Loop through each feed item and display each item as a hyperlink.
        foreach ($rss_items as $item) {
            ?>
                <li>
                    <a href='<?php echo $item->get_permalink(); ?>' title='<?php echo __('Posted ', 'luckiesDesign') . $item->get_date('j F Y | g:i a'); ?>'><?php echo $item->get_title(); ?></a>
                </li><?php
        }
    }
    ?>
    </ul><?php
}

/**
 * Adds luckiesDesign Contextual help
 *
 * @return string
 */
function luckiesdesign_theme_options_help() {

    $general_help = '<p>';
    $general_help .= __('luckiesDesign is the most easy to use WordPress Theme. You will find many state of the art options and widgets with luckiesDesign.', 'luckiesDesign');
    $general_help .= '</p><p>';
    $general_help .= __('By using luckiesDesign, users can specify settings for basic functions (like date format, excerpt word count etc.) directly from theme options. ', 'luckiesDesign');
    $general_help .= __('luckiesDesign provides theme options to manage some basic settings for your theme. ', 'luckiesDesign');
    $general_help .= __('Below are the options provided for your convenience.', 'luckiesDesign');
    $general_help .= '</p><p>';
    $general_help .= __('<strong>Logo Settings:</strong> Theme\'s logo can be managed from this setting.', 'luckiesDesign');
    $general_help .= '</p><p>';
    $general_help .= __('<strong>Favicon Settings:</strong> Theme\'s favicon can be managed from this setting.', 'luckiesDesign');
    $general_help .= '</p><p>';
    $general_help .= __('<strong>Facebook Open Graph Settings:</strong> This setting will provide an option to specify Faceboook App ID/Admin ID(s), required for Open Graph.', 'luckiesDesign');
    $general_help .= '</p><p>';
    $general_help .= __('<strong>FeedBurner Settings:</strong> FeedBurner URL can be specified from this setting to redirect your feeds.', 'luckiesDesign');
    $general_help .= '</p><p>';
    $general_help .= __('<strong>Google Custom Search Integration:</strong> This option would enable you to harness the power of Google Search instead of the default WordPress search by specifying the Google Custom Search Code.  You also have the option of rendering the Google Search Page without the sidebar.', 'luckiesDesign');
    $general_help .= '</p><p>';
    $general_help .= __('<strong>Sidebar Settings:</strong> Enable / Disable the Footer Sidebar from here.', 'luckiesDesign');
    $general_help .= '</p><p>';
    $general_help .= __('<strong>Custom Styles:</strong> You can specify your own CSS styles in this option to override the default Style.', 'luckiesDesign');
    $general_help .= '</p><p>';
    $general_help .= __('<strong>Backup luckiesDesign Options:</strong> Export or import all settings that you have configured in luckiesDesign.', 'luckiesDesign');
    $general_help .= '</p>';
    $general_help .= '<p>' . __('Remember to click "<strong>Save All Changes</strong>" to save any changes you have made to the theme options.', 'luckiesDesign') . '</p>';

    $post_comment_help = '<p>';
    $post_comment_help .= __('luckiesDesign is the most easy to use WordPress Theme Framework. You will find many state of the art options and widgets with luckiesDesign.', 'luckiesDesign');
    $post_comment_help .= '</p><p>';
    $post_comment_help .= __('luckiesDesign framework is used worldwide and keeping this in mind we have made it localization ready. ', 'luckiesDesign');
    $post_comment_help .= __('Developers can use luckiesDesign as a basic and stripped to bones theme framework for developing their own creative and wonderful WordPress Themes.', 'luckiesDesign');
    $post_comment_help .= '</p><p>';
    $post_comment_help .= __('By using luckiesDesign, developers and users can specify settings for basic functions (like date format, excerpt word count etc.) directly from theme options. ', 'luckiesDesign');
    $post_comment_help .= __('luckiesDesign provides theme options to manage some basic settings for your theme. ', 'luckiesDesign');
    $post_comment_help .= __('Below are the options provided for your convenience.', 'luckiesDesign');
    $post_comment_help .= '</p><p>';
    $post_comment_help .= __('<strong>Post Summaries Settings:</strong> Specify the different excerpt parameters like word count etc.', 'luckiesDesign');
    $post_comment_help .= '</p><p>';
    $post_comment_help .= __('<strong>Post Thumbnail Settings:</strong> Specify the post thumbnail options like position, size etc.', 'luckiesDesign');
    $post_comment_help .= '<br />';
    $post_comment_help .= __('<small><strong><em>NOTE:</em></strong> If you are using this option to change height or width of the thumbnail, then please use \'Regenerate Thumbnails\' plugin, to apply the new dimension settings to your thumbnails.</small>', 'luckiesDesign');
    $post_comment_help .= '</p><p>';
    $post_comment_help .= __('<strong>Post Meta Settings:</strong> You can specify the post meta options like post date format, display or hide author name and their positions in relation with the content.', 'luckiesDesign');
    $post_comment_help .= '</p><p>';
    $post_comment_help .= __('<strong>Pagination Settings:</strong> Enable this setting to use default WordPress pagination.', 'luckiesDesign');
    $post_comment_help .= '</p><p>';
    $post_comment_help .= __('<strong>Comment Form Settings:</strong> You can specify the comment form settings from this option.', 'luckiesDesign');
    $post_comment_help .= '</p><p>';
    $post_comment_help .= __('<strong>Gravtar Settings:</strong> Specify the general Gravtar support from this option.', 'luckiesDesign');
    $post_comment_help .= '</p>';
    $post_comment_help .= '<p>' . __('Remember to click "<strong>Save All Changes</strong>" to save any changes you have made to the theme options.', 'luckiesDesign') . '</p>';

    
    $screen = get_current_screen();
    $screen->add_help_tab(array('title' => __('General', 'luckiesDesign'), 'id' => 'luckiesdesign-general-help', 'content' => $general_help));
    $screen->add_help_tab(array('title' => __('Post &amp; Comment', 'luckiesDesign'), 'id' => 'post-comments-help', 'content' => $post_comment_help));
    $screen->set_help_sidebar($sidebar);
}

add_action('load-appearance_page_luckiesdesign_general', 'luckiesdesign_theme_options_help');
add_action('load-appearance_page_luckiesdesign_post_comments', 'luckiesdesign_theme_options_help');

/**
 * Show luckiesDesign only to Admin Users ( Admin-Bar only !!! )
 */
function luckiesdesign_admin_bar_init() {
    // Is the user sufficiently leveled, or has the bar been disabled?
    if (!is_super_admin() || !is_admin_bar_showing()) {
        return;
    }
    // Good to go, let's do this!
    add_action('admin_bar_menu', 'luckiesdesign_admin_bar_links', 500);
}

add_action('admin_bar_init', 'luckiesdesign_admin_bar_init');

/**
 * Adds luckiesDesign links to Admin Bar
 *
 * @uses object $wp_admin_bar
 */
function luckiesdesign_admin_bar_links() {
    global $wp_admin_bar, $rt_panel_theme;

    // Links to add, in the form: 'Label' => 'URL'
    foreach ($rt_panel_theme->theme_pages as $key => $theme_page) {
        if (is_array($theme_page))
            $links[$theme_page['menu_title']] = array('url' => admin_url('themes.php?page=' . $theme_page['menu_slug']), 'slug' => $theme_page['menu_slug']);
    }

    //  Add parent link
    $wp_admin_bar->add_menu(array(
        'title' => 'luckiesDesign',
        'href' => admin_url('themes.php?page=luckiesdesign_general'),
        'id' => 'rt_links',
    ));

    // Add submenu links
    foreach ($links as $label => $menu) {
        $wp_admin_bar->add_menu(array(
            'title' => $label,
            'href' => $menu['url'],
            'parent' => 'rt_links',
            'id' => $menu['slug']
        ));
    }
}

/**
 * Creates luckiesDesign Options backup file
 * 
 * @uses $wpdb object
 */
function luckiesdesign_export() {
    global $wpdb;
    $sitename = sanitize_key(get_bloginfo('name'));

    if (!empty($sitename))
        $sitename .= '.';

    $filename = $sitename . 'luckiesdesign.' . date('Y-m-d') . '.luckiesdesign';

    $general = "WHERE option_name = 'luckiesdesign_general'";
    $post_comments = "WHERE option_name = 'luckiesdesign_post_comments'";
    $hooks = "WHERE option_name = 'luckiesdesign_hooks'";
    $args['luckiesdesign_general'] = $wpdb->get_var("SELECT option_value FROM {$wpdb->options} $general");
    $args['luckiesdesign_post_comments'] = $wpdb->get_var("SELECT option_value FROM {$wpdb->options} $post_comments");
    $args['luckiesdesign_hooks'] = $wpdb->get_var("SELECT option_value FROM {$wpdb->options} $hooks");

    header('Content-Description: File Transfer');
    header('Content-Disposition: attachment; filename=' . $filename);
    header('Content-Type: text/xml; charset=' . get_option('blog_charset'), true);
    ?>
    <luckiesdesign>
        <luckiesdesign_version><?php echo maybe_serialize(luckiesdesign_export_version()); ?></luckiesdesign_version>
        <luckiesdesign_general><?php echo $args['luckiesdesign_general']; ?></luckiesdesign_general>
        <luckiesdesign_post_comments><?php echo $args['luckiesdesign_post_comments']; ?></luckiesdesign_post_comments>
    </luckiesdesign>
    <?php
}

/**
 * Restores luckiesDesign Options
 *
 * @uses $luckiesdesign_general array
 * @uses $luckiesdesign_post_comments array
 * @uses $luckiesdesign_hooks array
 * @param string $file The
 * @return bool|array
 */
function luckiesdesign_import($file) {
    global $luckiesdesign_general, $luckiesdesign_post_comments;
    require_once( ABSPATH . '/wp-admin/includes/class-wp-filesystem-base.php' );
    require_once( ABSPATH . '/wp-admin/includes/class-wp-filesystem-direct.php' );
    require_once( ABSPATH . '/wp-admin/includes/file.php' );

    @$file_object = new WP_Filesystem_Direct;
    $overrides = array('test_form' => false, 'test_type' => false);
    $import_file = wp_handle_upload($file, $overrides);
    extract(wp_check_filetype($import_file['file'], array('luckiesdesign' => 'txt/luckiesdesign')));
    $data = wp_remote_get($import_file['url']);
    $file_object->delete($import_file['file']);
    if ($ext != 'luckiesdesign') {
        return 'ext';
    }
    if (is_wp_error($data)) {
        return false;
    } else {
        preg_match('/\<luckiesdesign_general\>(.*)<\/luckiesdesign_general\>/is', $data['body'], $general);
        preg_match('/\<luckiesdesign_post_comments\>(.*)<\/luckiesdesign_post_comments\>/is', $data['body'], $post_comments);
        if (!empty($post_comments[1])) {
            update_option('luckiesdesign_post_comments', maybe_unserialize($post_comments[1]));
        }
        return $general[1];
    }
}

/**
 * Adds Custom Logo to Admin Dashboard ;)
 */
function luckiesdesign_custom_admin_logo() {
    echo '<style type="text/css"> #header-logo { background: url("' . LUCKIESDESIGN_IMG_FOLDER_URL . '/luckiesdesign-icon.jpg") no-repeat scroll center center transparent !important; max-width: 16px; height: auto; } </style>';
}

add_action('admin_head', 'luckiesdesign_custom_admin_logo');


/**
 * Gets luckiesDesign and WordPress version
 */
function luckiesdesign_export_version() {
    global $wp_version;
    require_once( ABSPATH . '/wp-admin/includes/update.php' );
    /* Backward Compatability for version prior to WordPress 3.4 */
    $theme_info = function_exists('wp_get_theme') ? wp_get_theme() : get_theme(get_current_theme());
    if (is_child_theme()) {
        $theme_info = function_exists('wp_get_theme') ? wp_get_theme('luckiesdesign') : get_theme($theme_info['Parent Theme']);
    }
    $theme_version = array('wp' => $wp_version, 'luckiesDesign' => $theme_info['Version']);
    return $theme_version;
}

/**
 * Gets luckiesDesign and WordPress version (in text) for footer
 */
function luckiesdesign_version($update_footer) {
    global $luckiesdesign_version;
    $update_footer .= '<br /><br />' . __('luckiesDesign Version ', 'luckiesDesign') . $luckiesdesign_version['luckiesDesign'];
    return $update_footer;
}

add_filter('update_footer', 'luckiesdesign_version', 9999);

/**
 * Adds Styles dropdown to TinyMCE Editor
 */
function luckiesdesign_mce_editor_buttons($buttons) {
    array_unshift($buttons, 'styleselect');
    return $buttons;
}

add_filter('mce_buttons_2', 'luckiesdesign_mce_editor_buttons');

/**
 * Adds Non Semantic Helper classes/styles dropdown to TinyMCE Editor
 */
function luckiesdesign_mce_before_init($settings) {

    $style_formats = array(
        array(
            'title' => 'Clean',
            'block' => 'p',
            'classes' => 'clean',
            'wrapper' => false
        ),
        array(
            'title' => 'Alert',
            'block' => 'p',
            'classes' => 'alert',
            'wrapper' => false
        ),
        array(
            'title' => 'Info',
            'block' => 'p',
            'classes' => 'info',
            'wrapper' => false
        ),
        array(
            'title' => 'Success',
            'block' => 'p',
            'classes' => 'success',
            'wrapper' => false
        ),
        array(
            'title' => 'Warning',
            'block' => 'p',
            'classes' => 'warning',
            'wrapper' => false
        ),
        array(
            'title' => 'Error',
            'block' => 'p',
            'classes' => 'error',
            'wrapper' => false
        )
    );

    $settings['style_formats'] = json_encode($style_formats);
    return $settings;
}

add_filter('tiny_mce_before_init', 'luckiesdesign_mce_before_init');

/**
 * Adds favicon image to the list of generated images ( For Logo/Favicon Settings )
 */
function luckiesdesign_create_favicon($sizes) {
    $sizes['favicon'] = array('width' => 16, 'height' => 16, 'crop' => 1);
    return $sizes;
}


/** 
 * Default Values for the extended custom theme options
 */
function luckiesdesign_custom_theme_default_values() {
    $default_values = array(
                        'footer_text1'      => '',
                        'footer_text2'      => '',
                        'custom_textarea'  => '',
                        'header_image_link'  => '',
                        'footer_logo_id'  => '',
                        'header_image_id'  => '',
                        
                        'cookie_policy_link'  => '',
                        'cookie_policy_text'  => '',
                        'terms_condition_link'  => '',
                        'terms_condition_text'  => '',
                        'site_map_text'  => '',
                        'site_map_link'  => '',
                    );

    if ( !get_option( 'custom_theme_options' ) ) {
        update_option( 'custom_theme_options', $default_values );
        $blog_users = get_users();

        /* Set screen layout to 1 by default for all users */
        foreach ( $blog_users as $blog_user ) {
          $blog_user_id = $blog_user->ID;
          if ( !get_user_meta( $blog_user_id, 'screen_layout_appearance_page_custom_theme_options' ) )
          update_user_meta( $blog_user_id, 'screen_layout_appearance_page_custom_theme_options', 1, NULL );
        }
    }

    return $default_values;
}

/** 
 * Extended Custom Theme Options Validation Callback
 */
function luckiesdesign_custom_theme_options_validate( $input ) {
    if ( isset ( $_POST['luckiesdesign_submit'] ) ) {
       $input['footer_text1'] = trim( $input['footer_text1'] );
       $input['footer_text2'] = trim( $input['footer_text2'] );
       $input['header_image_link'] = trim( $input['header_image_link'] );
       $input['footer_logo_id'] = trim( $input['footer_logo_id'] );
       $input['header_image_id'] = trim( $input['header_image_id'] );
       
       $input['terms_condition_link'] = trim( $input['terms_condition_link'] );
       $input['terms_condition_text'] = trim( $input['terms_condition_text'] );
       $input['site_map_link'] = trim( $input['site_map_link'] );
       $input['site_map_text'] = trim( $input['site_map_text'] );
       $input['cookie_policy_link'] = trim( $input['cookie_policy_link'] );
       $input['cookie_policy_text'] = trim( $input['cookie_policy_text'] );
    } elseif ( isset ( $_POST['luckiesdesign_reset'] ) ) {
       $input = luckiesdesign_custom_theme_default_values();
       add_settings_error( 'custom_theme_options', 'reset_custom_theme_options', __( 'All Custom Theme Options have been restored to default.', 'luckiesDesign' ), 'updated' );
    }
    return $input;
}


