<?php
/**
 * luckiesDesign options metaboxes
 */

/**
 * Registers luckiesDesign General and Post & Comments options
 */
function luckiesdesign_admin_init_general() {
    register_setting('general_settings', 'luckiesdesign_general', 'luckiesdesign_general_validate');
    register_setting('post_comment_settings', 'luckiesdesign_post_comments', 'luckiesdesign_post_comments_validate');
    register_setting('custom_theme_options_settings', 'custom_theme_options', 'luckiesdesign_custom_theme_options_validate');
}

add_action('admin_init', 'luckiesdesign_admin_init_general');

/**
 * Logo Settings Metabox - General Tab
 */
function luckiesdesign_logo_option_metabox() {
    global $luckiesdesign_general;
    $luckiesdesign_general['logo_use'] = isset($luckiesdesign_general['logo_use']) ? $luckiesdesign_general['logo_use'] : 'site_title';
    $luckiesdesign_general['favicon_use'] = isset($luckiesdesign_general['favicon_use']) ? $luckiesdesign_general['favicon_use'] : 'disable';
    $logo_style = ( 'site_title' == $luckiesdesign_general['logo_use'] ) ? ' style="display: none"' : '';
    $favicon_style = ( in_array($luckiesdesign_general['favicon_use'], array('disable', 'logo')) ) ? ' style="display: none"' : '';
    ?>
    <table class="form-table">
        <tbody>
            <tr valign="top">
                <th scope="row"><label for="logo_use"><?php _e('For Logo', 'luckiesDesign'); ?></label></th>
                <td colspan="3">
                    <div class="alignleft">
                        <p style="margin-bottom: 10px;"><input type="radio" name="luckiesdesign_general[logo_use]" value="site_title" id="use_site_title" class="luckiesdesign_logo" <?php checked('site_title', $luckiesdesign_general['logo_use']); ?> />
                            <label for="use_site_title" style="margin-right: 30px;"><?php _e('Use Site Title', 'luckiesDesign'); ?></label>
                            <input type="radio" name="luckiesdesign_general[logo_use]" value="image" id="use_logo_image" class="luckiesdesign_logo" <?php checked('image', $luckiesdesign_general['logo_use']); ?> />
                            <label for="use_logo_image"><?php _e('Upload Logo', 'luckiesDesign'); ?></label></p>
                        <input type="file" name="html-upload-logo" id="html-upload-logo"<?php echo $logo_style; ?>>
                        <input type="hidden"  name="luckiesdesign_general[logo_upload]" id="logo_upload_url" value="<?php if (isset($luckiesdesign_general['logo_upload'])) echo $luckiesdesign_general['logo_upload']; ?>" />
                        <input type="hidden"  name="luckiesdesign_general[logo_id]" id="logo_id" value="<?php if (isset($luckiesdesign_general['logo_id'])) echo $luckiesdesign_general['logo_id']; ?>" />
                        <input type="hidden"  name="luckiesdesign_general[logo_width]" id="logo_width" value="<?php if (isset($luckiesdesign_general['logo_width'])) echo $luckiesdesign_general['logo_width']; ?>" />
                        <input type="hidden"  name="luckiesdesign_general[logo_height]" id="logo_height" value="<?php if (isset($luckiesdesign_general['logo_height'])) echo $luckiesdesign_general['logo_height']; ?>" />
                        <p class="login-head"<?php echo $logo_style; ?>>
                            <input type="hidden" name="luckiesdesign_general[login_head]" value="0" />
                            <input type="checkbox" name="luckiesdesign_general[login_head]" value="1" id="login_head" <?php checked($luckiesdesign_general['login_head']); ?> />
                            <span class="description"><label for="login_head"><?php printf(__('Check this box to display logo on <a href="%s" title="Wordpress Login">WordPress Login Screen</a>', 'luckiesDesign'), site_url('/wp-login.php')); ?></label></span>
                        </p>
                    </div>
                    <div class="image-preview alignright" id="logo_metabox"<?php echo $logo_style; ?>>
                        <img alt="Logo" src="<?php echo $luckiesdesign_general['logo_upload']; ?>" />
                    </div>
                </td>

            </tr>
            <tr valign="top">
                <th scope="row"><label for="favicon_use"><?php _e('For Favicon', 'luckiesDesign'); ?></label></th>
                <td rowspan="3">
                    <div class="alignleft">
                        <p style="margin-bottom: 10px;"><input type="radio" name="luckiesdesign_general[favicon_use]" value="disable" id="favicon_disable" class="luckiesdesign_favicon" <?php checked('disable', $luckiesdesign_general['favicon_use']); ?> />
                            <label for="favicon_disable" style="margin-right: 30px;"><?php _e('Disable', 'luckiesDesign'); ?></label>
                            <input type="radio" name="luckiesdesign_general[favicon_use]" value="logo" id="use_logo" class="luckiesdesign_favicon" <?php
                            disabled($luckiesdesign_general['logo_use'], 'site_title');
                            checked('logo', $luckiesdesign_general['favicon_use']);
                            ?> />
                            <label for="use_logo"  style="margin-right: 30px;"><?php _e('Resize Logo and use as Favicon', 'luckiesDesign'); ?></label>
                            <input type="radio" name="luckiesdesign_general[favicon_use]" value="image" id="use_favicon_image" class="luckiesdesign_favicon" <?php checked('image', $luckiesdesign_general['favicon_use']); ?> />
                            <label for="use_favicon_image"><?php _e('Upload Favicon', 'luckiesDesign'); ?></label></p>
                        <input type="file" name="html-upload-fav" id="html-upload-fav"<?php echo $favicon_style; ?>>
                        <input type="hidden"  name="luckiesdesign_general[favicon_upload]" id="favicon_upload_url" value="<?php if (isset($luckiesdesign_general['favicon_upload'])) echo $luckiesdesign_general['favicon_upload']; ?>" />
                        <input type="hidden"  name="luckiesdesign_general[favicon_id]" id="favicon_id" value="<?php if (isset($luckiesdesign_general['favicon_id'])) echo $luckiesdesign_general['favicon_id']; ?>" />
                    </div>
                    <div class="image-preview alignright" id="favicon_metabox"<?php echo ( 'disable' == $luckiesdesign_general['favicon_use'] ) ? ' style="display: none"' : ''; ?>>
                        <img alt="Favicon" src="<?php echo $luckiesdesign_general['favicon_upload']; ?>" />
                    </div>
                </td>
            </tr>
        </tbody>
    </table>
    <div class="luckiesdesign_submit">
        <?php submit_button('Save All Changes', 'primary', 'luckiesdesign_submit', false); ?>
        <?php submit_button('Reset Logo & Favicon Settings', 'secondary', 'luckiesdesign_logo_favicon_reset', false); ?>
        <div class="clear"></div>
    </div>
    <?php
}

/**
 * Facebook Open Graph Metabox - General Tab
 */
function luckiesdesign_facebook_ogp_metabox() {
    global $luckiesdesign_general;
    ?>
    <table class="form-table">
        <tbody>
            <tr valign="top">
                <th scope="row"><label for="fb_admins"><?php _e('Facebook Admin ID(s)', 'luckiesDesign'); ?></label></th>
                <td>
                    <input type="text" value="<?php echo esc_attr($luckiesdesign_general['fb_admins']); ?>" size="40" name="luckiesdesign_general[fb_admins]" id="fb_admins" />
                    <span class="description"><label for="fb_admins"><?php _e('Specify Facebook Admin ID(s) ( Comma separated )', 'luckiesDesign'); ?></label></span>
                </td>
            </tr>
            <tr valign="top">
                <th scope="row"><label for="fb_app_id"><?php _e('Facebook App ID', 'luckiesDesign'); ?></label></th>
                <td>
                    <input type="text" value="<?php echo esc_attr($luckiesdesign_general['fb_app_id']); ?>" size="40" name="luckiesdesign_general[fb_app_id]" id="fb_app_id" />
                    <span class="description"><label for="fb_app_id"><?php printf(__('<a href="%s" target="_blank" title="Find your App ID Here">Find your App ID Here</a>', 'luckiesDesign'), 'https://developers.facebook.com/apps/'); ?></label></span>
                </td>
            </tr>
            <tr valign="top">
                <td colspan="2">
                    <strong><?php _e('Note', 'luckiesDesign'); ?> : </strong><span class="description"><?php _e('Anyone would be sufficient.', 'luckiesDesign'); ?></span>
                </td>
            </tr>
        </tbody>
    </table>
    <div class="luckiesdesign_submit">
        <?php submit_button('Save All Changes', 'primary', 'luckiesdesign_submit', false); ?>
        <?php submit_button('Reset Facebook OGP Settings', 'secondary', 'luckiesdesign_fb_ogp_reset', false); ?>
        <div class="clear"></div>
    </div>
    <?php
}

/**
 * Feedburner Settings Metabox - General Tab
 */
function luckiesdesign_feed_option_metabox() {
    global $luckiesdesign_general;
    ?>
    <table class="form-table">
        <tbody>
            <tr valign="top">
                <th scope="row"><label for="feedburner_url"><?php _e('FeedBurner URL', 'luckiesDesign'); ?></label></th>
                <td>
                    <input type="text" placeholder="http://www.example.com" value="<?php echo esc_attr($luckiesdesign_general['feedburner_url']); ?>" size="40" name="luckiesdesign_general[feedburner_url]" id="feedburner_url" />
                    <span class="description"><label for="feedburner_url"><?php printf(__('Specify <a href="%s" target="_blank" title="FeedBurner">FeedBurner</a> URL to redirect feed', 'luckiesDesign'), 'http://www.feedburner.com/'); ?></label></label></span>
                </td>
            </tr>
        </tbody>
    </table>
    <div class="luckiesdesign_submit">
        <?php submit_button('Save All Changes', 'primary', 'luckiesdesign_submit', false); ?>
        <?php submit_button('Reset FeedBurner Settings', 'secondary', 'luckiesdesign_feed_reset', false); ?>
        <div class="clear"></div>
    </div>
    <?php
}

/**
 * Misc. Settings Metabox - General Tab
 */
function luckiesdesign_sidebar_options_metabox() {
    global $luckiesdesign_general;
    ?>
    <table class="form-table">
        <tbody>
            <tr valign="top">
                <th scope="row"><label for="footer_sidebar"><?php _e('Enable Footer Sidebar', 'luckiesDesign'); ?></label></th>
                <td>
                    <input type="hidden" name="luckiesdesign_general[footer_sidebar]" value="0" />
                    <input type="checkbox" value="1" size="40" name="luckiesdesign_general[footer_sidebar]" id="footer_sidebar" <?php checked($luckiesdesign_general['footer_sidebar']); ?> />
                    <span class="description"><label for="footer_sidebar"><?php _e('Check this to enable footer sidebar', 'luckiesDesign'); ?></label><br /></span>
                </td>
            </tr>
        </tbody>
    </table>
    <div class="luckiesdesign_submit">
        <?php submit_button('Save All Changes', 'primary', 'luckiesdesign_submit', false); ?>
        <?php submit_button('Reset Sidebar Settings', 'secondary', 'luckiesdesign_sidebar_reset', false); ?>
        <div class="clear"></div>
    </div>
    <?php
}

/**
 * Google Custom Search Integration Metabox - General Tab
 */
function luckiesdesign_google_search_metabox() {
    global $luckiesdesign_general;
    ?>
    <table class="form-table">
        <tbody>
            <tr valign="top">
                <th scope="row"><label for="search_code"><?php _e('Google Custom Search Element Code', 'luckiesDesign'); ?></label></th>
                <td>
                    <textarea cols="80" rows="5" name="luckiesdesign_general[search_code]" id="search_code"><?php echo esc_textarea($luckiesdesign_general['search_code']); ?></textarea><br />
                    <label for="search_code"><span class="description"><?php printf(__('The Google Search Code Obtained by Default. You can obtain the Google Custom Search Code <a href="%s" title="Google Custom Search">here</a><br />', 'luckiesDesign'), 'http://www.google.com/cse/'); ?></span>
                        <strong><?php _e('NOTE', 'luckiesDesign'); ?>: </strong><span class="description"><?php _e('The hosting option must be "Search Element" and layout should be either "full-width" or "compact".', 'luckiesDesign'); ?></span></label>
                </td>
            </tr>
            <tr valign="top">
                <th scope="row"><label for="search_layout"><?php _e('Hide Sidebar', 'luckiesDesign'); ?></label></th>
                <td>
                    <input type="hidden" name="luckiesdesign_general[search_layout]" value="0" />
                    <input type="checkbox" name="luckiesdesign_general[search_layout]" value="1" id="search_layout" <?php checked($luckiesdesign_general['search_layout']); ?> />
                    <span class="description"><label for="search_layout"><?php _e('Do not show sidebar on "Search Results" Page ( While using Google Custom Search )', 'luckiesDesign'); ?></label></span>
                </td>
            </tr>
        </tbody>
    </table>
    <div class="luckiesdesign_submit">
        <?php submit_button('Save All Changes', 'primary', 'luckiesdesign_submit', false); ?>
        <?php submit_button('Reset Google Custom Search Integration', 'secondary', 'luckiesdesign_google_reset', false); ?>
        <div class="clear"></div>
    </div>
    <?php
}

/**
 * Custom Styles Metabox - General Tab
 */
function luckiesdesign_custom_styles_metabox() {
    global $luckiesdesign_general;
    ?>
    <table class="form-table">
        <tbody>
            <tr valign="top">
                <th scope="row"><label for="custom_styles"><?php _e('Add your CSS here &rarr;', 'luckiesDesign'); ?></label></th>
                <td>
                    <textarea cols="80" rows="5" name="luckiesdesign_general[custom_styles]" id="custom_styles"><?php echo esc_textarea($luckiesdesign_general['custom_styles']); ?></textarea><br />
                    <span class="description"><label for="custom_styles"><?php _e('Add your extra CSS rules here. No need to use !important. Rules written above will be loaded last.', 'luckiesDesign'); ?></label></span>
                </td>
            </tr>
        </tbody>
    </table>
    <div class="luckiesdesign_submit">
        <?php submit_button('Save All Changes', 'primary', 'luckiesdesign_submit', false); ?>
        <?php submit_button('Reset Custom Styles', 'secondary', 'luckiesdesign_custom_styles_reset', false); ?>
        <div class="clear"></div>
    </div>
    <?php
}

/**
 * luckiesDesign Options Backup / Restore Metabox - General Tab
 */
function luckiesdesign_backup_metabox() {
    ?>
    <table class="form-table">
        <tbody>
            <tr valign="top">
                <th><label for="luckiesdesign_export"><?php _e('Export luckiesDesign Options', 'luckiesDesign'); ?></label></th>
                <td>
                    <?php submit_button('Export', 'secondary', 'luckiesdesign_export', false); ?>
                </td>
            </tr>
            <tr valign="top">
                <th><label for="luckiesdesign_import"><?php _e('Import luckiesDesign Options', 'luckiesDesign'); ?></label></th>
                <td>
                    <input type="file" id="luckiesdesign_import" name="luckiesdesign_import" />
                    <?php submit_button('Import', 'secondary', 'luckiesdesign_import', false); ?>
                </td>
            </tr>
        </tbody>
    </table>
    <?php
}

/**
 * Post Summary Settings Metabox - Post & Comments Tab
 */
function luckiesdesign_post_summaries_metabox() {
    global $luckiesdesign_post_comments;
    ?>
    <table class="form-table">
        <tbody>
            <tr valign="top">
                <th scope="row"><label for="summary_show"><?php _e('Enable Summary', 'luckiesDesign'); ?></label></th>
                <td>
                    <input type="hidden" name="luckiesdesign_post_comments[summary_show]" value="0" />
                    <input type="checkbox" name="luckiesdesign_post_comments[summary_show]" value="1" id="summary_show" <?php checked($luckiesdesign_post_comments['summary_show']); ?> />
                    <span class="description"><label for="summary_show"><?php _e('Check this to enable excerpts on Archive pages ( Pages with multiple posts on them )', 'luckiesDesign'); ?></label></span>
                </td>
            </tr>
            <tr valign="top">
                <th scope="row"><label for="word_limit"><?php _e('Word Limit', 'luckiesDesign'); ?></label></th>
                <td>
                    <input  maxlength="4" type="number" value="<?php echo $luckiesdesign_post_comments['word_limit']; ?>" size="4" name="luckiesdesign_post_comments[word_limit]" id="word_limit" />
                    <span class="description"><label for="word_limit"><?php _e('Post Content will be cut around Word Limit you will specify here.', 'luckiesDesign'); ?></label></span>
                </td>
            </tr>
            <tr valign="top">
                <th scope="row"><label for="read_text"><?php _e('Read More Text', 'luckiesDesign'); ?></label></th>
                <td>
                    <input type="text" value="<?php echo esc_attr($luckiesdesign_post_comments['read_text']); ?>" size="30" name="luckiesdesign_post_comments[read_text]" id="read_text" />
                    <span class="description"><label for="read_text"><?php _e('This will be added after each post summary. Text added here will be automatically converted into a hyperlink pointing to the respective post.', 'luckiesDesign'); ?></label></span>
                </td>
            </tr>
        </tbody>
    </table>
    <div class="luckiesdesign_submit">
        <?php submit_button('Save All Changes', 'primary', 'luckiesdesign_submit', false); ?>
        <?php submit_button('Reset Post Sumarry Settings', 'secondary', 'luckiesdesign_summary_reset', false); ?>
        <div class="clear"></div>
    </div><?php
}

/**
 * Post Thumbnail Settings Metabox - Post & Comments Tab
 */
function luckiesdesign_post_thumbnail_metabox() {
    global $luckiesdesign_post_comments;
    ?> 
    <br />
    <span class="description post-summary-hide"><strong><?php _e('Enable Summary must be checked on the Post Summary Settings to show these Options', 'luckiesDesign'); ?></strong></span>
    <table class="form-table">
        <tbody>
            <tr valign="top">
                <th scope="row"><label for="thumbnail_show"><?php _e('Enable Thumbnails', 'luckiesDesign'); ?></label></th>
                <td>
                    <input type="hidden" name="luckiesdesign_post_comments[thumbnail_show]" value="0" />
                    <input type="checkbox" name="luckiesdesign_post_comments[thumbnail_show]" value="1" id="thumbnail_show" <?php checked($luckiesdesign_post_comments['thumbnail_show']); ?> />
                    <span class="description"><label for="thumbnail_show"><?php _e('Check this to display thumbnails as part of Post Summaries on Archive pages', 'luckiesDesign'); ?></label></span>
                </td>
            </tr>
            <tr valign="top">
                <th scope="row"><label><?php _e('Thumbnail Alignment', 'luckiesDesign'); ?></label></th>
                <td>
                    <div class="alignleft"><input type="radio" name="luckiesdesign_post_comments[thumbnail_position]" value="None" id="None" <?php checked('None', $luckiesdesign_post_comments['thumbnail_position']); ?> /><label for="None"><?php _e('None', 'luckiesDesign'); ?></label></div>
                    <div class="alignleft"><input type="radio" name="luckiesdesign_post_comments[thumbnail_position]" value="Left" id="Left" <?php checked('Left', $luckiesdesign_post_comments['thumbnail_position']); ?> /><label for="Left"><?php _e('Left', 'luckiesDesign'); ?></label></div>
                    <div class="alignleft"><input type="radio" name="luckiesdesign_post_comments[thumbnail_position]" value="Right" id="Right" <?php checked('Right', $luckiesdesign_post_comments['thumbnail_position']); ?> /><label for="Right"><?php _e('Right', 'luckiesDesign'); ?></label></div>
                    <div class="alignleft"><input type="radio" name="luckiesdesign_post_comments[thumbnail_position]" value="Center" id="Center" <?php checked('Center', $luckiesdesign_post_comments['thumbnail_position']); ?> /><label for="Center"><?php _e('Center', 'luckiesDesign'); ?></label></div>
                </td>
            </tr>
            <tr valign="top">
                <th scope="row"><label for="thumbnail_width"><?php _e('Width', 'luckiesDesign'); ?></label></th>
                <td>
                    <input maxlength="3" type="number" value="<?php echo get_option('thumbnail_size_w'); ?>" size="3" name="luckiesdesign_post_comments[thumbnail_width]" id="thumbnail_width" /> px
                </td>
            </tr>
            <tr valign="top">
                <th scope="row"><label for="thumbnail_height"><?php _e('Height', 'luckiesDesign'); ?></label></th>
                <td>
                    <input maxlength="3" type="number" value="<?php echo get_option('thumbnail_size_h'); ?>" size="3" name="luckiesdesign_post_comments[thumbnail_height]" id="thumbnail_height" /> px
                </td>
            </tr>
            <tr valign="top">
                <th scope="row"><label for="thumbnail_crop"><?php _e('Crop Thumbnail', 'luckiesDesign'); ?></label></th>
                <td>
                    <input type="hidden" name="luckiesdesign_post_comments[thumbnail_crop]" value="0" />
                    <input type="checkbox" name="luckiesdesign_post_comments[thumbnail_crop]" value="1" id="thumbnail_crop" <?php checked(get_option('thumbnail_crop')); ?> />
                    <span class="description"><label for="thumbnail_crop"><?php _e('Crop thumbnail to exact dimensions (normally thumbnails are proportional)', 'luckiesDesign'); ?></label></span>
                </td>
            </tr>
            <tr valign="top">
                <th scope="row"><label for="thumbnail_frame"><?php _e('Add Frame (Border Effect around Image)', 'luckiesDesign'); ?></label></th>
                <td>
                    <input type="hidden" name="luckiesdesign_post_comments[thumbnail_frame]" value="0" />
                    <input type="checkbox" name="luckiesdesign_post_comments[thumbnail_frame]" value="1" id="thumbnail_frame" <?php echo checked($luckiesdesign_post_comments['thumbnail_frame']) ?> />
                    <span class="description"><label for="thumbnail_frame"><?php _e('Check this to display a light shadow border effect for the thumbnails', 'luckiesDesign'); ?></label></span>
                </td>
            </tr>
        </tbody>
    </table>
    <div class="luckiesdesign_submit">
        <?php submit_button('Save All Changes', 'primary', 'luckiesdesign_submit', false); ?>
        <?php submit_button('Reset Post Thumbnail Settings', 'secondary', 'luckiesdesign_thumbnail_reset', false); ?>
        <div class="clear"></div>
    </div><?php
}

/**
 * Post Meta Settings Metabox - Post & Comments Tab
 */
function luckiesdesign_post_meta_metabox() {
    global $luckiesdesign_post_comments;
    $date_format_u = ( $luckiesdesign_post_comments['post_date_format_u'] != 'F j, Y' && $luckiesdesign_post_comments['post_date_format_u'] != 'Y/m/d' && $luckiesdesign_post_comments['post_date_format_u'] != 'm/d/Y' && $luckiesdesign_post_comments['post_date_format_u'] != 'd/m/Y' ) ? true : false;
    $date_format_l = ( $luckiesdesign_post_comments['post_date_format_l'] != 'F j, Y' && $luckiesdesign_post_comments['post_date_format_l'] != 'Y/m/d' && $luckiesdesign_post_comments['post_date_format_l'] != 'm/d/Y' && $luckiesdesign_post_comments['post_date_format_l'] != 'd/m/Y' ) ? true : false;
    $args = array('_builtin' => false);
    $taxonomies = get_taxonomies($args, 'objects');
    ?><br />
    <span class="description"><strong><?php _e('This option will allow you to specify the post meta attributes and their position', 'luckiesDesign'); ?></strong></span><br /><br />
    <strong><?php _e('These Post Meta will be displayed above content', 'luckiesDesign'); ?></strong>
    <table class="form-table">
        <tbody>
            <tr valign="top">
                <th scope="row"><p><label for="post_date_u"><?php _e('Post Date', 'luckiesDesign'); ?></label></p></th>
    <td>
        <input type="hidden" name="luckiesdesign_post_comments[post_date_u]" value="0" />
        <input type="checkbox" name="luckiesdesign_post_comments[post_date_u]" value="1" id="post_date_u" <?php checked($luckiesdesign_post_comments['post_date_u']); ?> />
        <span class="description"><label for="post_date_u"><?php _e('Check this box to include Post Dates in meta', 'luckiesDesign'); ?></label></span>
        <div class="post-meta-common post_date_format_u">
            <strong><?php _e('Select a Date Format', 'luckiesDesign'); ?> :</strong><br />
            <input type="radio" name="luckiesdesign_post_comments[post_date_format_u]" id="full-date-u" value="F j, Y" <?php checked('F j, Y', $luckiesdesign_post_comments['post_date_format_u']); ?> /><label class="full-date-u" for="full-date-u" title="F j, Y"><?php echo date_i18n(__('F j, Y', 'luckiesDesign')); ?></label><br />
            <input type="radio" name="luckiesdesign_post_comments[post_date_format_u]" id="y-m-d-u" value="Y/m/d" <?php checked('Y/m/d', $luckiesdesign_post_comments['post_date_format_u']); ?> /><label class="y-m-d-u" for="y-m-d-u" title="Y/m/d"><?php echo date_i18n(__('Y/m/d', 'luckiesDesign')); ?></label><br />
            <input type="radio" name="luckiesdesign_post_comments[post_date_format_u]" id="m-d-y-u" value="m/d/Y" <?php checked('m/d/Y', $luckiesdesign_post_comments['post_date_format_u']); ?> /><label class="m-d-y-u" for="m-d-y-u" title="m/d/Y"><?php echo date_i18n(__('m/d/Y', 'luckiesDesign')); ?></label><br />
            <input type="radio" name="luckiesdesign_post_comments[post_date_format_u]" id="d-m-y-u" value="d/m/Y" <?php checked('d/m/Y', $luckiesdesign_post_comments['post_date_format_u']); ?> /><label class="d-m-y-u" for="d-m-y-u" title="d/m/Y"><?php echo date_i18n(__('d/m/Y', 'luckiesDesign')); ?></label><br />
            <input type="radio" name="luckiesdesign_post_comments[post_date_format_u]" id="post_date_custom_format_u" value="<?php echo esc_attr($luckiesdesign_post_comments['post_date_custom_format_u']); ?>" <?php checked($date_format_u); ?> /><label for="custom-date-u" title="<?php echo esc_attr($luckiesdesign_post_comments['post_date_custom_format_u']); ?>">Custom :<input id="custom-date-u" value="<?php echo esc_attr($luckiesdesign_post_comments['post_date_custom_format_u']); ?>" type="text" size="5" name="luckiesdesign_post_comments[post_date_custom_format_u]" /> <span><?php echo date_i18n($luckiesdesign_post_comments['post_date_custom_format_u']); ?></span><img class="ajax-loading" alt="loading.." src="<?php echo admin_url('/images/wpspin_light.gif'); ?>" /></label><br />
        </div>
    </td>
    </tr>
    <tr valign="top">
        <th scope="row"><p><label for="post_author_u"><?php _e('Post Author', 'luckiesDesign'); ?></label></p></th>
    <td>
        <input type="hidden" name="luckiesdesign_post_comments[post_author_u]" value="0" />
        <input type="checkbox" name="luckiesdesign_post_comments[post_author_u]" value="1" id="post_author_u" <?php checked($luckiesdesign_post_comments['post_author_u']); ?> />
        <span class="description"><label for="post_author_u"><?php _e('Check this box to include Author Name in meta', 'luckiesDesign'); ?></label></span>
        <div class="post-meta-common post_author_u-sub">
            <input type="hidden" name="luckiesdesign_post_comments[author_count_u]" value="0" />
            <input type="checkbox" name="luckiesdesign_post_comments[author_count_u]" value="1" id="author_count_u" <?php checked($luckiesdesign_post_comments['author_count_u']); ?> /><label for="author_count_u"><?php _e('Show Author Posts Count', 'luckiesDesign'); ?></label><br />
            <input type="hidden" name="luckiesdesign_post_comments[author_link_u]" value="0" />
            <input type="checkbox" name="luckiesdesign_post_comments[author_link_u]" value="1" id="author_link_u" <?php checked($luckiesdesign_post_comments['author_link_u']); ?> /><label for="author_link_u"><?php _e('Link to Author Archive page', 'luckiesDesign'); ?></label><br />
        </div>
    </td>
    </tr>
    <tr valign="top">
        <th scope="row"><p><label for="post_category_u"><?php _e('Post Categories', 'luckiesDesign'); ?></label></p></th>
    <td>
        <input type="hidden" name="luckiesdesign_post_comments[post_category_u]" value="0" />
        <input type="checkbox" name="luckiesdesign_post_comments[post_category_u]" value="1" id="post_category_u" <?php checked($luckiesdesign_post_comments['post_category_u']); ?> />
    </td>
    </tr>
    <tr valign="top">
        <th scope="row"><p><label for="post_tags_u"><?php _e('Post Tags', 'luckiesDesign'); ?></label></p></th>
    <td>
        <input type="hidden" name="luckiesdesign_post_comments[post_tags_u]" value="0" />
        <input type="checkbox" name="luckiesdesign_post_comments[post_tags_u]" value="1" id="post_tags_u" <?php checked($luckiesdesign_post_comments['post_tags_u']); ?> />
    </td>
    </tr><?php
    if (!empty($taxonomies)) {
        foreach ($taxonomies as $key => $taxonomy) {
            $luckiesdesign_post_comments['post_' . $key . '_u'] = ( isset($luckiesdesign_post_comments['post_' . $key . '_u']) ) ? $luckiesdesign_post_comments['post_' . $key . '_u'] : 0;
            ?>
            <tr valign="top">
                <th scope="row"><p><label for="<?php echo 'post_' . $key . '_u'; ?>"><?php printf(__('%s', 'luckiesDesign'), $taxonomy->labels->name); ?></label></p></th>
            <td>
                <input type="hidden" name="luckiesdesign_post_comments[<?php echo 'post_' . $key . '_u'; ?>]" value="0" />
                <input type="checkbox" name="luckiesdesign_post_comments[<?php echo 'post_' . $key . '_u'; ?>]" value="1" id="<?php echo 'post_' . $key . '_u'; ?>" <?php checked($luckiesdesign_post_comments['post_' . $key . '_u']); ?> />
            </td>
            </tr><?php
        }
    }
    ?>
    </tbody>
    </table>
    <br />
    <strong><?php _e('These Post Meta will be displayed below content', 'luckiesDesign'); ?></strong>
    <table class="form-table">
        <tbody>
            <tr valign="top">
                <th scope="row"><p><label for="post_date_l"><?php _e('Post Date', 'luckiesDesign'); ?></label></p></th>
    <td>
        <input type="hidden" name="luckiesdesign_post_comments[post_date_l]" value="0" />
        <input type="checkbox" name="luckiesdesign_post_comments[post_date_l]" value="1" id="post_date_l" <?php checked($luckiesdesign_post_comments['post_date_l']); ?> />
        <span class="description"><label for="post_date_l"><?php _e('Check this box to include Post Dates in meta', 'luckiesDesign'); ?></label></span>
        <div class="post-meta-common post_date_format_l">
            <strong><?php _e('Select a Date Format', 'luckiesDesign'); ?> :</strong><br />
            <input type="radio" name="luckiesdesign_post_comments[post_date_format_l]" id="full-date-l" value="F j, Y" <?php checked('F j, Y', $luckiesdesign_post_comments['post_date_format_l']); ?> /><label class="full-date-l" for="full-date-l" title="F j, Y"><?php echo date_i18n(__('F j, Y', 'luckiesDesign')); ?></label><br />
            <input type="radio" name="luckiesdesign_post_comments[post_date_format_l]" id="y-m-d-l" value="Y/m/d" <?php checked('Y/m/d', $luckiesdesign_post_comments['post_date_format_l']); ?> /><label class="y-m-d-l" for="y-m-d-l" title="Y/m/d"><?php echo date_i18n(__('Y/m/d', 'luckiesDesign')); ?></label><br />
            <input type="radio" name="luckiesdesign_post_comments[post_date_format_l]" id="m-d-y-l" value="m/d/Y" <?php checked('m/d/Y', $luckiesdesign_post_comments['post_date_format_l']); ?> /><label class="m-d-y-l" for="m-d-y-l" title="m/d/Y"><?php echo date_i18n(__('m/d/Y', 'luckiesDesign')); ?></label><br />
            <input type="radio" name="luckiesdesign_post_comments[post_date_format_l]" id="d-m-y-l" value="d/m/Y" <?php checked('d/m/Y', $luckiesdesign_post_comments['post_date_format_l']); ?> /><label class="d-m-y-l" for="d-m-y-l" title="d/m/Y"><?php echo date_i18n(__('d/m/Y', 'luckiesDesign')); ?></label><br />
            <input type="radio" name="luckiesdesign_post_comments[post_date_format_l]" id="post_date_custom_format_l" value="<?php echo esc_attr($luckiesdesign_post_comments['post_date_custom_format_l']); ?>" <?php checked($date_format_l); ?> /><label for="custom-date-l" title="<?php echo esc_attr($luckiesdesign_post_comments['post_date_custom_format_l']); ?>">Custom :<input id="custom-date-l" value="<?php echo esc_attr($luckiesdesign_post_comments['post_date_custom_format_l']); ?>" type="text" size="5" name="luckiesdesign_post_comments[post_date_custom_format_l]" /> <span><?php echo date_i18n($luckiesdesign_post_comments['post_date_custom_format_l']); ?></span><img class="ajax-loading" alt="loading.." src="<?php echo admin_url('/images/wpspin_light.gif'); ?>" /></label><br />
        </div>
    </td>
    </tr>
    <tr valign="top">
        <th scope="row"><p><label for="post_author_l"><?php _e('Post Author', 'luckiesDesign'); ?></label></p></th>
    <td>
        <input type="hidden" name="luckiesdesign_post_comments[post_author_l]" value="0" />
        <input type="checkbox" name="luckiesdesign_post_comments[post_author_l]" value="1" id="post_author_l" <?php checked($luckiesdesign_post_comments['post_author_l']); ?> />
        <span class="description"><label for="post_author_l"><?php _e('Check this box to include Author Name in meta', 'luckiesDesign'); ?></label></span>
        <div class="post-meta-common post_author_l-sub">
            <input type="hidden" name="luckiesdesign_post_comments[author_count_l]" value="0" />
            <input type="checkbox" name="luckiesdesign_post_comments[author_count_l]" value="1" id="author_count_l" <?php checked($luckiesdesign_post_comments['author_count_l']); ?> /><label for="author_count_l"><?php _e('Show Author Posts Count', 'luckiesDesign'); ?></label><br />
            <input type="hidden" name="luckiesdesign_post_comments[author_link_l]" value="0" />
            <input type="checkbox" name="luckiesdesign_post_comments[author_link_l]" value="1" id="author_link_l" <?php checked($luckiesdesign_post_comments['author_link_l']); ?> /><label for="author_link_l"><?php _e('Link to Author Archive page', 'luckiesDesign'); ?></label><br />
        </div>
    </td>
    </tr>
    <tr valign="top">
        <th scope="row"><p><label for="post_category_l"><?php _e('Post Categories', 'luckiesDesign'); ?></label></p></th>
    <td>
        <input type="hidden" name="luckiesdesign_post_comments[post_category_l]" value="0" />
        <input type="checkbox" name="luckiesdesign_post_comments[post_category_l]" value="1" id="post_category_l" <?php checked($luckiesdesign_post_comments['post_category_l']); ?> />
    </td>
    </tr>
    <tr valign="top">
        <th scope="row"><p><label for="post_tags_l"><?php _e('Post Tags', 'luckiesDesign'); ?></label></p></th>
    <td>
        <input type="hidden" name="luckiesdesign_post_comments[post_tags_l]" value="0" />
        <input type="checkbox" name="luckiesdesign_post_comments[post_tags_l]" value="1" id="post_tags_l" <?php checked($luckiesdesign_post_comments['post_tags_l']); ?> />
    </td>
    </tr><?php
    if (!empty($taxonomies)) {
        foreach ($taxonomies as $key => $taxonomy) {
            $luckiesdesign_post_comments['post_' . $key . '_l'] = ( isset($luckiesdesign_post_comments['post_' . $key . '_l']) ) ? $luckiesdesign_post_comments['post_' . $key . '_l'] : 0;
            ?>
            <tr valign="top">
                <th scope="row"><p><label for="<?php echo 'post_' . $key . '_l'; ?>"><?php printf(__('%s', 'luckiesDesign'), $taxonomy->labels->name); ?></label></p></th>
            <td>
                <input type="hidden" name="luckiesdesign_post_comments[<?php echo 'post_' . $key . '_l'; ?>]" value="0" />
                <input type="checkbox" name="luckiesdesign_post_comments[<?php echo 'post_' . $key . '_l'; ?>]" value="1" id="<?php echo 'post_' . $key . '_l'; ?>" <?php checked($luckiesdesign_post_comments['post_' . $key . '_l']); ?> />
            </td>
            </tr><?php
        }
    }
    ?>
    </tbody>
    </table>
    <div class="luckiesdesign_submit">
        <?php submit_button('Save All Changes', 'primary', 'luckiesdesign_submit', false); ?>
        <?php submit_button('Reset Post Meta Settings', 'secondary', 'luckiesdesign_meta_reset', false); ?>
        <div class="clear"></div>
    </div><?php
}

/**
 * Pagination Settings Metabox - Post & Comments Tab
 */
function luckiesdesign_pagination_metabox() {
    global $luckiesdesign_post_comments;
    ?>
    <table class="form-table">
        <tbody>
            <tr valign="top">
                <th scope="row"><label for="pagination_show"><?php _e('Enable Pagination', 'luckiesDesign'); ?></label></th>
                <td>
                    <input type="hidden" name="luckiesdesign_post_comments[pagination_show]" value="0" />
                    <input type="checkbox" name="luckiesdesign_post_comments[pagination_show]" value="1" id="pagination_show" <?php checked($luckiesdesign_post_comments['pagination_show']); ?> />
                    <span class="description"><label for="pagination_show"><?php _e('Check this to enable default WordPress Pagination on Archive pages ( Pages with multiple posts on them )', 'luckiesDesign'); ?></label></span>
                </td>
            </tr>
            <tr valign="top">
                <th scope="row"><label for="prev_text"><?php _e('Prev Text', 'luckiesDesign'); ?></label></th>
                <td>
                    <input type="text" value="<?php echo esc_attr($luckiesdesign_post_comments['prev_text']); ?>" size="30" name="luckiesdesign_post_comments[prev_text]" id="prev_text" />
                    <span class="description"><label for="prev_text"><?php _e('Text to display for Previous Page', 'luckiesDesign'); ?></label></span>
                </td>
            </tr>
            <tr valign="top">
                <th scope="row"><label for="next_text"><?php _e('Next Text', 'luckiesDesign'); ?></label></th>
                <td>
                    <input type="text" value="<?php echo esc_attr($luckiesdesign_post_comments['next_text']); ?>" size="30" name="luckiesdesign_post_comments[next_text]" id="next_text" />
                    <span class="description"><label for="next_text"><?php _e('Text to display for Next Page', 'luckiesDesign'); ?></label></span>
                </td>
            </tr>
            <tr valign="top">
                <th scope="row"><label for="end_size"><?php _e('End Size', 'luckiesDesign'); ?></label></th>
                <td>
                    <input  maxlength="4" type="number" value="<?php echo $luckiesdesign_post_comments['end_size']; ?>" size="4" name="luckiesdesign_post_comments[end_size]" id="end_size" />
                    <span class="description"><label for="end_size"><?php _e('How many numbers on either the start and the end list edges?', 'luckiesDesign'); ?></label></span>
                </td>
            </tr>
            <tr valign="top">
                <th scope="row"><label for="mid_size"><?php _e('Mid Size', 'luckiesDesign'); ?></label></th>
                <td>
                    <input  maxlength="4" type="number" value="<?php echo $luckiesdesign_post_comments['mid_size']; ?>" size="4" name="luckiesdesign_post_comments[mid_size]" id="mid_size" />
                    <span class="description"><label for="mid_size"><?php _e('How many numbers to either side of current page, but not including current page?', 'luckiesDesign'); ?></label></span>
                </td>
            </tr>
        </tbody>
    </table>
    <div class="luckiesdesign_submit">
        <?php submit_button('Save All Changes', 'primary', 'luckiesdesign_submit', false); ?>
        <?php submit_button('Reset Pagination Settings', 'secondary', 'luckiesdesign_pagination_reset', false); ?>
        <div class="clear"></div>
    </div><?php
}

/**
 * Comment Form Settings Metabox - Post & Comments Tab
 */
function luckiesdesign_comment_form_metabox() {
    global $luckiesdesign_post_comments;
    ?>
    <table class="form-table">
        <tbody>
            <tr valign="top">
                <th scope="row"><p><label for="compact_form"><?php _e('Enable Compact Form', 'luckiesDesign'); ?></label></p></th>
    <td>
        <input type="hidden" name="luckiesdesign_post_comments[compact_form]" value="0" />
        <input type="checkbox" name="luckiesdesign_post_comments[compact_form]" value="1" id="compact_form" <?php checked($luckiesdesign_post_comments['compact_form']); ?> />
        <span class="description"><label for="compact_form"><?php _e('Check this box to compact comment form. Name, URL & Email Fields will be on same line', 'luckiesDesign'); ?></label></span>
        <br />
        <input type="hidden" name="luckiesdesign_post_comments[hide_labels]" value="0" />
        <input type="checkbox" name="luckiesdesign_post_comments[hide_labels]" value="1" id="hide_labels" <?php checked($luckiesdesign_post_comments['hide_labels']); ?> />
        <span class="description"><label for="hide_labels"><?php _e('Hide Labels for Name, Email & URL. These will be shown inside fields as default text', 'luckiesDesign'); ?></label></span>
    </td>
    </tr>
    <tr valign="top" class="show-fields-comments">
        <th scope="row"><p><label for="comment_textarea"><?php _e('Extra Settings', 'luckiesDesign'); ?></label></p></th>
    <td>
        <input type="hidden" name="luckiesdesign_post_comments[comment_textarea]" value="0" />
        <input type="checkbox" name="luckiesdesign_post_comments[comment_textarea]" value="1" id="comment_textarea" <?php checked($luckiesdesign_post_comments['comment_textarea']); ?> />
        <span class="description"><label for="comment_textarea"><?php _e('Display Comment textarea above Name, Email, &amp; URL Fields', 'luckiesDesign'); ?></label></span>
        <br />
        <input type="hidden" name="luckiesdesign_post_comments[comment_separate]" value="0" />
        <input type="checkbox" name="luckiesdesign_post_comments[comment_separate]" value="1" id="comment_separate" <?php checked($luckiesdesign_post_comments['comment_separate']); ?> />
        <span class="description"><label for="comment_separate"><?php _e('Separate Comments from Trackbacks &amp; Pingbacks', 'luckiesDesign'); ?></label></span>
        <br />
        <input type="hidden" name="luckiesdesign_post_comments[attachment_comments]" value="0" />
        <input type="checkbox" name="luckiesdesign_post_comments[attachment_comments]" value="1" id="attachment_comments" <?php checked($luckiesdesign_post_comments['attachment_comments']); ?> />
        <span class="description"><label for="attachment_comments"><?php _e('Enable the comment form on Attachments', 'luckiesDesign'); ?></label></span>
    </td>
    </tr>
    </tbody>
    </table>
    <div class="luckiesdesign_submit">
        <?php submit_button('Save All Changes', 'primary', 'luckiesdesign_submit', false); ?>
        <?php submit_button('Reset Comment Form Settings', 'secondary', 'luckiesdesign_comment_reset', false); ?>
        <div class="clear"></div>
    </div><?php
}

/**
 * Gravatar Settings Metabox - Post & Comments Tab
 */
function luckiesdesign_gravatar_metabox() {
    global $luckiesdesign_post_comments;
    ?>
    <table class="form-table">
        <tbody>
            <tr valign="top">
                <th scope="row"><p><label for="gravatar_show"><?php _e('Enable Gravatar Support', 'luckiesDesign'); ?></label></p></th>
    <td>
        <input type="hidden" name="luckiesdesign_post_comments[gravatar_show]" value="0" />
        <input type="checkbox" name="luckiesdesign_post_comments[gravatar_show]" value="1" id="gravatar_show" <?php checked($luckiesdesign_post_comments['gravatar_show']); ?> />
    </td>
    </tr>
    <tr valign="top" class="gravatar-size">
        <th scope="row"><p><label for="gravatar_size"><?php _e('Gravatar Size', 'luckiesDesign'); ?></label></p></th>
    <td>
        <select name="luckiesdesign_post_comments[gravatar_size]" id="gravatar_size">
            <option value="32" <?php selected('32', $luckiesdesign_post_comments['gravatar_size']); ?>>32px X 32px</option>
            <option value="40" <?php selected('40', $luckiesdesign_post_comments['gravatar_size']); ?>>40px X 40px</option>
            <option value="48" <?php selected('48', $luckiesdesign_post_comments['gravatar_size']); ?>>48px X 48px</option>
            <option value="56" <?php selected('56', $luckiesdesign_post_comments['gravatar_size']); ?>>56px X 56px</option>
            <option value="64" <?php selected('64', $luckiesdesign_post_comments['gravatar_size']); ?>>64px X 64px</option>
            <option value="96" <?php selected('96', $luckiesdesign_post_comments['gravatar_size']); ?>>96px X 96px</option>
        </select>
    </td>
    </tr>
    </tbody>
    </table>
    <div class="luckiesdesign_submit">
        <?php submit_button('Save All Changes', 'primary', 'luckiesdesign_submit', false); ?>
        <?php submit_button('Reset Gravatar Settings', 'secondary', 'luckiesdesign_gravatar_reset', false); ?>
        <div class="clear"></div>
    </div><?php
}

/**
 * Extended Header Image Custom Theme Options Metabox Markup
 */
function luckiesdesign_home_image_category_metabox() {
    global $luckiesdesign_custom_theme_options;
    $luckiesdesign_custom_theme_options = ( get_option('custom_theme_options') ) ? get_option('custom_theme_options') : luckiesdesign_custom_theme_default_values();
    $product_categories = get_terms('product_cat');
    ?>
    <table class="form-table">
        <tbody>
            <tr valign="top">
                <th scope="row"><label for="home_image_category1"><?php _e('Home Image Category', 'luckiesDesign'); ?></label></th>
                <td>
                    <select name="custom_theme_options[home_image_category1]">
                        <?php
                        foreach ($product_categories as $home_image_category1) {
                            $selected = '';
                            if ($luckiesdesign_custom_theme_options['home_image_category1'] == $home_image_category1->slug)
                                $selected = 'selected="selected"';
                            echo '<option value="' . $home_image_category1->slug . '" ' . $selected . '>' . $home_image_category1->name . '</option>';
                        }
                        ?>
                    </select>
                </td>
            </tr>
            <tr valign="top">
                <th scope="row"><label for="home_image_category1"><?php _e('Home Image Category', 'luckiesDesign'); ?></label></th>
                <td>
                    <select name="custom_theme_options[home_image_category2]">
                        <?php
                        foreach ($product_categories as $home_image_category2) {
                            $selected = '';
                            if ($luckiesdesign_custom_theme_options['home_image_category2'] == $home_image_category2->slug)
                                $selected = 'selected="selected"';
                            echo '<option value="' . $home_image_category2->slug . '" ' . $selected . '>' . $home_image_category2->name . '</option>';
                        }
                        ?>
                    </select>
                </td>
            </tr>
            <tr valign="top">
                <th scope="row"><label for="home_image_category3"><?php _e('Home Image Category', 'luckiesDesign'); ?></label></th>
                <td>
                    <select name="custom_theme_options[home_image_category3]">
                        <?php
                        foreach ($product_categories as $home_image_category3) {
                            $selected = '';
                            if ($luckiesdesign_custom_theme_options['home_image_category3'] == $home_image_category3->slug)
                                $selected = 'selected="selected"';
                            echo '<option value="' . $home_image_category3->slug . '" ' . $selected . '>' . $home_image_category3->name . '</option>';
                        }
                        ?>
                    </select>
                </td>
            </tr>
        </tbody>
    </table>
    <?php
}

/**
 * Extended Header Image Custom Theme Options Metabox Markup
 */
function luckiesdesign_home_additional_info_metabox() {
    global $luckiesdesign_custom_theme_options;
    $luckiesdesign_custom_theme_options = ( get_option('custom_theme_options') ) ? get_option('custom_theme_options') : luckiesdesign_custom_theme_default_values();
    ?>
    <table class="form-table">
        <tbody>
            <tr valign="top">
                <th scope="row"><label for="student_discount"><?php _e('Discount Text', 'luckiesDesign'); ?></label></th>
                <td>
                    <input type="text" value="<?php echo $luckiesdesign_custom_theme_options['student_discount']; ?>" size="30" name="custom_theme_options[student_discount]" id="student_discount" />
                </td>
                <th scope="row"><label for="discount_amount"><?php _e('Discount Amount', 'luckiesDesign'); ?></label></th>
                <td>
                    <input type="text" value="<?php echo $luckiesdesign_custom_theme_options['discount_amount']; ?>" size="30" name="custom_theme_options[discount_amount]" id="discount_amount" />
                </td>
            </tr>
            <tr valign="top">
                <th scope="row"><label for="call_text"><?php _e('Contact Text', 'luckiesDesign'); ?></label></th>
                <td>
                    <input type="text" value="<?php echo $luckiesdesign_custom_theme_options['call_text']; ?>" size="30" name="custom_theme_options[call_text]" id="call_text" />
                </td>
                <th scope="row"><label for="contact_number"><?php _e('Contact Number', 'luckiesDesign'); ?></label></th>
                <td>
                    <input type="text" value="<?php echo $luckiesdesign_custom_theme_options['contact_number']; ?>" size="30" name="custom_theme_options[contact_number]" id="contact_number" />
                </td>
            </tr>
            <tr valign="top">
                <th scope="row"><label for="shipping_text"><?php _e('Shipping Text', 'luckiesDesign'); ?></label></th>
                <td>
                    <input type="text" value="<?php echo $luckiesdesign_custom_theme_options['shipping_text']; ?>" size="30" name="custom_theme_options[shipping_text]" id="shipping_text" />
                </td>
                <th scope="row"><label for="shipping_amount"><?php _e('Shipping Amount', 'luckiesDesign'); ?></label></th>
                <td>
                    <input type="text" value="<?php echo $luckiesdesign_custom_theme_options['shipping_amount']; ?>" size="30" name="custom_theme_options[shipping_amount]" id="shipping_amount" />
                </td>
            </tr>
        </tbody>
    </table>
    <?php
}

/**
 * Extended Header Image Custom Theme Options Metabox Markup
 */
function luckiesdesign_home_category_crousel_metabox() {
    global $luckiesdesign_custom_theme_options;
    $luckiesdesign_custom_theme_options = ( get_option('custom_theme_options') ) ? get_option('custom_theme_options') : luckiesdesign_custom_theme_default_values();
    $product_categories = get_terms('product_cat');
    $check = isset($luckiesdesign_custom_theme_options['new_arrival']) ? esc_attr($luckiesdesign_custom_theme_options['new_arrival']) : 'off';
    ?>
    <table class="form-table">
        <tbody>
            <tr valign="top">
                <th scope="row"><label for="home_category_crousel1"><?php _e('Crousel Category 1', 'luckiesDesign'); ?></label></th>
                <td>
                    <select name="custom_theme_options[home_category_crousel1]">
                        <?php
                        foreach ($product_categories as $home_category_crousel1) {
                            $selected = '';
                            if ($luckiesdesign_custom_theme_options['home_category_crousel1'] == $home_category_crousel1->slug)
                                $selected = 'selected="selected"';
                            echo '<option value="' . $home_category_crousel1->slug . '" ' . $selected . '>' . $home_category_crousel1->name . '</option>';
                        }
                        ?>
                    </select>
                </td>
                <th scope="row"><label for="number_of_product1"><?php _e('Number of Product to Show', 'luckiesDesign'); ?></label></th>
                <td>
                    <input type="text" value="<?php echo $luckiesdesign_custom_theme_options['number_of_product1']; ?>" size="20" name="custom_theme_options[number_of_product1]" id="number_of_product1" />
                </td>
            </tr>
            <tr valign="top">
                <th scope="row"><label for="home_category_crousel2"><?php _e('Crousel Category 2', 'luckiesDesign'); ?></label></th>
                <td>
                    <select name="custom_theme_options[home_category_crousel2]">
                        <?php
                        foreach ($product_categories as $home_category_crousel2) {
                            $selected = '';
                            if ($luckiesdesign_custom_theme_options['home_category_crousel2'] == $home_category_crousel2->slug)
                                $selected = 'selected="selected"';
                            echo '<option value="' . $home_category_crousel2->slug . '" ' . $selected . '>' . $home_category_crousel2->name . '</option>';
                        }
                        ?>
                    </select>
                </td>
                <th scope="row"><label for="number_of_product2"><?php _e('Number of Product to Show', 'luckiesDesign'); ?></label></th>
                <td>
                    <input type="text" value="<?php echo $luckiesdesign_custom_theme_options['number_of_product2']; ?>" size="20" name="custom_theme_options[number_of_product2]" id="number_of_product2" />
                </td>
            </tr>
            <tr valign="top">
                <th scope="row"><label for="home_category_crousel3"><?php _e('Crousel Category 3', 'luckiesDesign'); ?></label></th>
                <td>
                    <select name="custom_theme_options[home_category_crousel3]">
                        <?php
                        foreach ($product_categories as $home_category_crousel3) {
                            $selected = '';
                            if ($luckiesdesign_custom_theme_options['home_category_crousel3'] == $home_category_crousel3->slug)
                                $selected = 'selected="selected"';
                            echo '<option value="' . $home_category_crousel3->slug . '" ' . $selected . '>' . $home_category_crousel3->name . '</option>';
                        }
                        ?>
                    </select>
                </td>
                <th scope="row"><label for="number_of_product3"><?php _e('Number of Product to Show', 'luckiesDesign'); ?></label></th>
                <td>
                    <input type="text" value="<?php echo $luckiesdesign_custom_theme_options['number_of_product3']; ?>" size="20" name="custom_theme_options[number_of_product3]" id="number_of_product3" />
                </td>
            </tr>
            <tr>
                <th scope="row"><label for="new_arrival"><?php _e('Show New Arrival in Place of Second Category', 'luckiesDesign'); ?></label></th>
                <td>
                    <input type="checkbox" <?php checked($check, 'on'); ?> name="custom_theme_options[new_arrival]" id="new_arrival" />
                </td>
            </tr>
        </tbody>
    </table>
    <?php
}

/**
 * Extended Header Image Custom Theme Options Metabox Markup
 */
function luckiesdesign_footer_navigation_metabox() {
    global $luckiesdesign_custom_theme_options;
    $luckiesdesign_custom_theme_options = ( get_option('custom_theme_options') ) ? get_option('custom_theme_options') : luckiesdesign_custom_theme_default_values();
    ?>
    <table class="form-table">
        <tbody>
            <tr>
                <th scope="row"><label for="terms_condition_link"><?php _e('Terms and Condition', 'luckiesDesign'); ?></label></th>
                <td>
                    <input type="text" value="<?php echo $luckiesdesign_custom_theme_options['terms_condition_link']; ?>" size="20" name="custom_theme_options[terms_condition_link]" id="terms_condition_link" />
                </td>
            </tr>
                <th scope="row"><label for="terms_condition_text"><?php _e('Terms and Condition Text', 'luckiesDesign'); ?></label></th>
                <td>
                    <input type="text" value="<?php echo $luckiesdesign_custom_theme_options['terms_condition_text']; ?>" size="20" name="custom_theme_options[terms_condition_text]" id="terms_condition_text" />
                </td>
            </tr>
            <tr>
                <th scope="row"><label for="site_map_link"><?php _e('Sitemap Link', 'luckiesDesign'); ?></label></th>
                <td>
                    <input type="text" value="<?php echo $luckiesdesign_custom_theme_options['site_map_link']; ?>" size="20" name="custom_theme_options[site_map_link]" id="site_map_link" />
                </td>
            </tr>
                <th scope="row"><label for="site_map_text"><?php _e('Sitemap Text', 'luckiesDesign'); ?></label></th>
                <td>
                    <input type="text" value="<?php echo $luckiesdesign_custom_theme_options['site_map_text']; ?>" size="20" name="custom_theme_options[site_map_text]" id="site_map_text" />
                </td>
            </tr>
            <tr>
                <th scope="row"><label for="cookie_policy_link"><?php _e('Policy Link', 'luckiesDesign'); ?></label></th>
                <td>
                    <input type="text" value="<?php echo $luckiesdesign_custom_theme_options['cookie_policy_link']; ?>" size="20" name="custom_theme_options[cookie_policy_link]" id="cookie_policy_link" />
                </td>
            </tr>
                <th scope="row"><label for="cookie_policy_text"><?php _e('Policy Text', 'luckiesDesign'); ?></label></th>
                <td>
                    <input type="text" value="<?php echo $luckiesdesign_custom_theme_options['cookie_policy_text']; ?>" size="20" name="custom_theme_options[cookie_policy_text]" id="cookie_policy_text" />
                </td>
            </tr>
        </tbody>
    </table>
    <?php
}

function luckiesdesign_theme_options_page_scripts() {
    // WP Enqueue Media
    if (function_exists('wp_enqueue_media')) {
        wp_enqueue_media();
    }

    wp_enqueue_script('custom-theme-options', get_stylesheet_directory_uri() . '/admin/js/custom-theme-options.js', 'luckiesdesign-admin-scripts');
    wp_enqueue_script('image-upload-widget', get_stylesheet_directory_uri() . '/admin/js/image-upload-widget.js');
}

add_action('admin_print_scripts-appearance_page_theme_options', 'luckiesdesign_theme_options_page_scripts', 999);
add_action('admin_head', 'luckiesdesign_theme_options_page_scripts', 999);
