/**
 * Theme Options Scripts
 */

jQuery(document).ready(function() {

    /* WP Media Uploader */

    function media_upload_new(button_id) {
        var _custom_media = true,
                _orig_send_attachment = wp.media.editor.send.attachment;
        jQuery(button_id).click(function(e) {
            var self = jQuery(this);
            var button = jQuery(this);
            _custom_media = true;
            wp.media.editor.send.attachment = function(props, attachment) {
                if (_custom_media) {
                    self.next().val(attachment.id);
                } else {
                    return _orig_send_attachment.apply(this, [props, attachment]);
                }
                ;
            };
            wp.media.editor.open(button);
            return false;
        });
    }

    media_uploader_theme_options('.custom_image_uploader');

});

function media_uploader_theme_options(button_id) {
    var _luckiesdesign_media = true;

    jQuery(button_id).click(function() {

        var button = jQuery(this),
            textbox_id = jQuery(this).attr('data-id');
            _luckiesdesign_media = true;

        wp.media.editor.send.attachment = function(props, attachment) {

            if (_luckiesdesign_media && (attachment.type === 'image')) {
                jQuery('#' + textbox_id).val(attachment.id);
                button.next().show();
            } else {
                alert('Please select a valid image file');
                return false;
            }
        }

        wp.media.editor.open(button);
        return false;
    });

}