<?php
/**
 * luckiesDesign Theme Options
 *
 * @package luckiesDesign
 */

// Includes PHP files located in 'admin/php/' folder
foreach ( glob( get_template_directory() . "/admin/lib/*.php" ) as $lib_filename ) {
    require_once( $lib_filename );
}

/**
 * luckiesDesign Theme Class
 *
 * Used to generate the luckiesDesign admin Panel Options.
 */
class luckiesdesign_theme {

    var $theme_pages;

    /**
     * Constructor
     *
     * @return void
     **/
    function luckiesdesign_theme() {
        $this->theme_pages = apply_filters( 'luckiesdesign_add_theme_pages', array(
            'luckiesdesign_general' => array(
                            'menu_title' => __( 'General', 'luckiesDesign' ),
                            'menu_slug' => 'luckiesdesign_general'
                            ),
            'luckiesdesign_post_comments' => array(
                            'menu_title' => __( 'Post &amp; Comments', 'luckiesDesign' ),
                            'menu_slug' => 'luckiesdesign_post_comments'
                            ),
            'custom_theme_options' => array(
                            'menu_title' => __( 'Luckies Design Theme Options', 'luckiesDesign' ),
                            'menu_slug' => 'custom_theme_options'
                            ) )
        );

        // Register callback for admin menu  setup
        add_action( 'admin_menu', array( &$this, 'luckiesdesign_theme_option_page' ) );
    }

    /**
     * Extends the admin menu, to add luckiesDesign
     **/
    function luckiesdesign_theme_option_page(  ) {
        // Add options page, you can also add it to different sections or use your own one
        add_theme_page( 'luckiesDesign - ' . $this->theme_pages['luckiesdesign_general']['menu_title'], '<strong class="luckiesdesign">luckiesDesign</strong>', 'edit_theme_options', 'luckiesdesign_general', array( &$this, 'luckiesdesign_admin_options' ) );
        foreach( $this->theme_pages as $key => $theme_page ) {
            if ( is_array( $theme_page ) )
                add_theme_page( 'luckiesDesign - ' . $theme_page['menu_title'], '--- <em>' . $theme_page['menu_title'] . '</em>', 'edit_theme_options', $theme_page['menu_slug'], array( &$this, 'luckiesdesign_admin_options' ) );
        }

        $tab = isset( $_GET['page'] )  ? $_GET['page'] : "luckiesdesign_general";

        /* Register  callback gets call prior the own page gets rendered */
        add_action( 'load-appearance_page_' . $tab, array( &$this, 'luckiesdesign_on_load_page' ) );
        add_action( 'admin_print_styles-appearance_page_' . $tab, array( &$this, 'luckiesdesign_admin_page_styles' ) );
        add_action( 'admin_print_scripts-appearance_page_' . $tab, array( &$this, 'luckiesdesign_admin_page_scripts' ) );
    }

    /**
     * Includes scripts for theme options page
     **/
    function luckiesdesign_admin_page_scripts() {
        wp_enqueue_script( 'luckiesdesign-admin-scripts', get_template_directory_uri() . '/admin/js/luckiesdesign-admin.js' );
         wp_enqueue_script( 'luckiesdesign-fb-share', ('http://static.ak.fbcdn.net/connect.php/js/FB.Share'),'', '', true );
        wp_enqueue_script( 'luckiesdesign-twitter-share', ('http://platform.twitter.com/widgets.js'),'', '', true );
        wp_enqueue_script( 'thickbox' );
        
    }

    /**
     * Includes styles for theme options page
     **/
    function luckiesdesign_admin_page_styles() {
        wp_enqueue_style( 'luckiesdesign-admin-styles', get_template_directory_uri() . '/admin/css/luckiesdesign-admin.css' );
        wp_enqueue_style( 'thickbox'); //thickbox for logo and favicon upload option
    }
 
    /**
     * luckiesDesign Tabs
     * 
     * Dividing the page into Tabs ( General, Post & Comments )
     **/
    function luckiesdesign_admin_options() {
        global $pagenow;
        $tabs = array();

        /* Separate the options page into two tabs - General , Post & Comments */
        foreach( $this->theme_pages as $key=>$theme_page ) {
            if ( is_array( $theme_page ) )
            $tabs[$theme_page['menu_slug']] = $theme_page['menu_title'];
        }
        $links = array();

        // Check to see which tab we are on
        $current = isset( $_GET['page'] )  ? $_GET['page'] : "luckiesdesign_general";
        foreach ( $tabs as $tab => $name ) {
            if ( $tab == $current ) {
                $links[] = "<a class='nav-tab nav-tab-active' href='?page=$tab'>$name</a>";
            } else {
                $links[] = "<a class='nav-tab' href='?page=$tab'>$name</a>";
            }
        } ?>

        <div class="wrap luckiesdesign-admin">
            <?php screen_icon( 'luckiesdesign' ); ?>
            <h2 class="luckiesdesign-tab-wrapper"><?php foreach ( $links as $link ) echo $link; ?></h2><?php
            if ( $pagenow == 'themes.php' ) {
                foreach( $this->theme_pages as $key=>$theme_page ) {
                    if ( is_array( $theme_page ) ) {
                        switch ( $current ) {
                            case $theme_page['menu_slug'] :
                                if ( function_exists( $theme_page['menu_slug'].'_options_page' ) )
                                call_user_func( $theme_page['menu_slug'].'_options_page', 'appearance_page_' . $current );
                                break;
                        }
                    }
                }
            } ?>
        </div><!-- .wrap --><?php
    }

    /**
     * Applies WordPress metabox funtionality to luckiesDesign metaboxes
     **/
    function luckiesdesign_on_load_page() {
        /* Javascripts loaded to allow drag/drop, expand/collapse and hide/show of boxes. */
        wp_enqueue_script( 'common' );
        wp_enqueue_script( 'wp-lists' );
        wp_enqueue_script( 'postbox' );

        // Check to see which tab we are on
        $tab = isset( $_GET['page'] )  ? $_GET['page'] : "luckiesdesign_general";
        
        switch ( $tab ) {
            case 'luckiesdesign_general' :
                // All metaboxes registered during load page can be switched off/on at "Screen Options" automatically, nothing special to do therefore
                add_meta_box( 'logo_options', __( 'Logo & Favicon Settings', 'luckiesDesign'), 'luckiesdesign_logo_option_metabox', 'appearance_page_' . $tab, 'normal', 'core' );
                add_meta_box( 'fb_ogp_options', __( 'Facebook Open Graph Settings', 'luckiesDesign'), 'luckiesdesign_facebook_ogp_metabox', 'appearance_page_' . $tab, 'normal', 'core' );
                add_meta_box( 'feed_options', __( 'Feedburner Settings', 'luckiesDesign'), 'luckiesdesign_feed_option_metabox', 'appearance_page_' . $tab, 'normal', 'core' );
                add_meta_box( 'google_search', __( 'Google Custom Search Integration', 'luckiesDesign'), 'luckiesdesign_google_search_metabox', 'appearance_page_' . $tab, 'normal', 'core' );
                add_meta_box( 'sidebar_options', __( 'Sidebar Settings', 'luckiesDesign' ), 'luckiesdesign_sidebar_options_metabox', 'appearance_page_' . $tab, 'normal', 'core' );
                add_meta_box( 'custom_styles_options', __( 'Custom Styles', 'luckiesDesign' ), 'luckiesdesign_custom_styles_metabox', 'appearance_page_' . $tab, 'normal', 'core' );
                add_meta_box( 'backup_options', __( 'Backup / Restore Settings', 'luckiesDesign' ), 'luckiesdesign_backup_metabox', 'appearance_page_' . $tab, 'normal', 'core' );
                do_action( $tab .'_metaboxes' );
                break;
            case 'luckiesdesign_post_comments' :
                // All metaboxes registered during load page can be switched off/on at "Screen Options" automatically, nothing special to do therefore
                add_meta_box( 'post_summaries_options', __('Post Summary Settings', 'luckiesDesign'), 'luckiesdesign_post_summaries_metabox', 'appearance_page_' . $tab, 'normal', 'core' );
                add_meta_box( 'post_thumbnail_options', __('Post Thumbnail Settings', 'luckiesDesign'), 'luckiesdesign_post_thumbnail_metabox', 'appearance_page_' . $tab, 'normal', 'core' );
                add_meta_box( 'post_meta_options', __('Post Meta Settings', 'luckiesDesign'), 'luckiesdesign_post_meta_metabox', 'appearance_page_' . $tab, 'normal', 'core' );
                add_meta_box( 'pagination_options', __('Pagination Settings', 'luckiesDesign'), 'luckiesdesign_pagination_metabox', 'appearance_page_' . $tab, 'normal', 'core' );
                add_meta_box( 'comment_form_options', __('Comment Form Settings', 'luckiesDesign'), 'luckiesdesign_comment_form_metabox', 'appearance_page_' . $tab, 'normal', 'core' );
                add_meta_box( 'gravatar_options', __('Gravatar Settings', 'luckiesDesign'), 'luckiesdesign_gravatar_metabox', 'appearance_page_' . $tab, 'normal', 'core' );
                do_action( $tab .'_metaboxes' );
                break;
            case $tab :
                do_action( $tab .'_metaboxes' );
                break;
        }
    }
}

// ★ of the show: luckiesDesign ... we ♥ it ;)
$rt_panel_theme = new luckiesdesign_theme();


/** 
 * Extended Custom Theme Options Metaboxes ( Screen Options )
 */
function luckiesdesign_custom_theme_options_screen_options() {
    add_meta_box( 'home-page-category', __( 'Image Categories', 'luckiesDesign' ), 'luckiesdesign_home_image_category_metabox', 'appearance_page_custom_theme_options', 'normal', 'core' );
    add_meta_box( 'home-page-additional-info', __( 'Additional Info', 'luckiesDesign' ), 'luckiesdesign_home_additional_info_metabox', 'appearance_page_custom_theme_options', 'normal', 'core' );
    add_meta_box( 'home-page-category-crousel', __( 'Category Crousel', 'luckiesDesign' ), 'luckiesdesign_home_category_crousel_metabox', 'appearance_page_custom_theme_options', 'normal', 'core' );
    add_meta_box( 'footer-links', __( 'Footer Navigation', 'luckiesDesign' ), 'luckiesdesign_footer_navigation_metabox', 'appearance_page_custom_theme_options', 'normal', 'core' );
        
}
add_action( 'custom_theme_options_metaboxes', 'luckiesdesign_custom_theme_options_screen_options' );